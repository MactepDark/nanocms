<?php

use yii\db\Migration;

/**
 * Class m181025_110952_language
 */
class m181025_110952_language extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('language', [
            'id' => $this->primaryKey(),
            'code' => $this->string(4)->notNull()->unique(),
            'view' => $this->string(4)->notNull(),
            'default' => $this->smallInteger(1)->notNull()->defaultValue(\app\models\Language::IS_NOT),
            'source' => $this->smallInteger(1)->notNull()->defaultValue(\app\models\Language::IS_NOT)
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('language');
    }
}
