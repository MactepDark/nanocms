<?php

use yii\db\Migration;

/**
 * Class m181025_160047_user
 */
class m181025_160047_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->unique()->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'token' => $this->string(128),
            'role' => $this->smallInteger(1)->notNull(),
            'status' => $this->smallInteger(1)->notNull(),
            'created_at' => $this->integer()->notNull()
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
