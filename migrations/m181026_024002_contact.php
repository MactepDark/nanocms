<?php

use yii\db\Migration;

/**
 * Class m181026_024002_contact
 */
class m181026_024002_contact extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('contact', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->notNull(),
            'priority' => $this->integer()->notNull(),
            'value' => $this->string(),
            'primary' => $this->smallInteger(1)->notNull(),
            'selected' => $this->smallInteger(1)->notNull()
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('contact');
    }
}
