<?php

use yii\db\Migration;

/**
 * Class m200401_180958_product
 */
class m200401_180958_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'type' => $this->smallInteger(1)->notNull(),
            'status' => $this->smallInteger(1)->notNull(),
            'priority' => $this->integer()->notNull(),
            'auctionPrice' => $this->integer()->notNull(),
            'marketPrice' => $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('product');
    }
}
