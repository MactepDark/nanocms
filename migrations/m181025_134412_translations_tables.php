<?php

use yii\db\Migration;

/**
 * Class m181025_174412_translations_tables
 */
class m181025_134412_translations_tables extends Migration
{
    public function up() {
        $this->createTable('source_message', [
            'id' => $this->primaryKey(),
            'category' => $this->string(),
            'message' => $this->text(),
        ], DEFAULT_MYSQL_TABLE_OPTIONS);

        $this->createTable('message', [
            'id' => $this->integer()->notNull(),
            'language' => $this->string(16)->notNull(),
            'translation' => $this->text(),
        ], DEFAULT_MYSQL_TABLE_OPTIONS);

        $this->addPrimaryKey('pk_message_id_language', 'message', ['id', 'language']);
        $this->addForeignKey('fk_message_source_message', 'message', 'id', 'source_message', 'id', 'CASCADE', 'RESTRICT');
        $this->createIndex('idx_source_message_category', 'source_message', 'category');
        $this->createIndex('idx_message_language', 'message', 'language');
    }

    public function down()
    {
        $this->dropForeignKey('fk_message_source_message', 'message');
        $this->dropTable('message');
        $this->dropTable('source_message');
    }
}
