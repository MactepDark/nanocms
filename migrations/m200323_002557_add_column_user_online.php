<?php

use yii\db\Migration;

/**
 * Class m200323_002557_add_column_user_online
 */
class m200323_002557_add_column_user_online extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('user', 'online', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('user', 'online');
    }
}
