<?php

use yii\db\Migration;

/**
 * Class m181028_083837_form
 */
class m181028_083837_form extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('form', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->notNull(),
            'status' => $this->smallInteger(1)->notNull(),
            'content' => $this->text(),
            'ip' => $this->string(16)->notNull(),
            'ua' => $this->string()->notNull(),
            'geo' => $this->string(),
            'created_at' => $this->integer()->notNull()
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('form');
    }
}