<?php

use app\models\User;
use yii\db\Migration;

/**
 * Class m181025_160124_add_default_admin
 */
class m181025_160124_add_default_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $defaultAdmin = new User();
        $defaultAdmin->name = 'Mactep';
        $defaultAdmin->email = 'anangalumbra@gmail.com';
        $defaultAdmin->generateAuthKey();
        $defaultAdmin->setPassword('1638432768');
        $defaultAdmin->role = User::ROLE_ADMIN;
        $defaultAdmin->status = User::STATUS_ACTIVE;
        $defaultAdmin->created_at = time();
        if ($defaultAdmin->save()) {
            echo "Default admin account is created!\n";
        } else { die('Unable to created default admin!'); }
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->delete('user', ['name' => 'Mactep']);
    }
}
