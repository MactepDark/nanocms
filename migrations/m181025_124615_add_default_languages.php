<?php

use app\models\Language;
use yii\db\Migration;

/**
 * Class m181025_174615_add_default_languages
 */
class m181025_124615_add_default_languages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('language', ['code' => 'en', 'view' => 'EN']);
        $this->insert('language', ['code' => 'uk', 'view' => 'UA']);
        $this->insert('language', ['code' => 'ru', 'view' => 'RU' , 'default' => Language::IS_DEFAULT, 'source' => Language::IS_SOURCE]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->delete('language', ['IN', 'code', ['en', 'ru', 'uk']]);
    }
}
