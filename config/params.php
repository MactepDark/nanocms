<?php
define('PROJECT_ROOT', realpath(__DIR__.'/..'));
//define('DEFAULT_MYSQL_TABLE_OPTIONS', ($this->db->driverName === 'mysql') ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);
define('DEFAULT_MYSQL_TABLE_OPTIONS', 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
define('SITE_LANGUAGES_CACHE', 86400);
define('TRANSLATIONS_CACHE', 86400);
define('DB_SCHEMA_CACHE', 86400);
define('DB_QUERY_CACHE', 3600);
return [
    'sendNotifyFormToMail' => ['anangalumbra@gmail.com' => 'MactepDark']
];
