<?php
require __DIR__ . '/types.php';
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'nanoCMS',
    'name' => 'AvtoTrust',
    'language' => 'uk',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['content', 'media'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'W5DgCp6rsSKxP-DdHa67iS9i4SysxhLs',
            'baseUrl' => ''
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['admin/login']
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'consoleRunner' => [
            'class' => 'vova07\console\ConsoleRunner',
            'file' => PROJECT_ROOT.DIRECTORY_SEPARATOR.'yii'
        ],
        'db' => $db,

        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                'ajax' => 'site/ajax',
                'robots.txt' => 'site/robots',
                'privacy-policy' => 'site/privacy-policy',
                'admin' => 'admin/index',
                'login' => 'admin/login',
                'logout' => 'admin/logout',
                'confirm/<token:[\w]+>' => 'admin/registration'
            ]
        ],

        'assetManager' => [
            'forceCopy' => YII_ENV_DEV,
            'converter' => [
                'class' => 'yii\web\AssetConverter',
                'commands' => [
                    'scss' => ['css', 'scss -t compressed --sourcemap=none -E utf-8 {from} {to}']
                ],
                'forceConvert' => YII_ENV_DEV
            ],
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'],
                ],
                'yii\web\YiiAsset' => ['js' => ['yii.js'], 'jsOptions' => ['defer' => 'defer']],
                'yii\bootstrap\BootstrapAsset' => ['css' => ['css/bootstrap.min.css']],
                'yii\bootstrap\BootstrapPluginAsset' => ['js' => [YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js']],
                'yii\widgets\ActiveFormAsset' => false,
                'yii\validators\ValidationAsset' => false,
            ],
            'appendTimestamp' => YII_ENV_DEV
        ],
        'i18n' => [
            'translations' => [
                'admin' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'enableCaching' => YII_ENV_PROD,
                    'cachingDuration' => TRANSLATIONS_CACHE
                ],
                'app' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'enableCaching' => YII_ENV_PROD,
                    'cachingDuration' => TRANSLATIONS_CACHE
                ]
            ]
        ]

    ],
    'modules' => [
        'content' => [
            'class' => 'app\modules\content\BaseContent'
        ],
        'media' => [
            'class' => 'app\modules\media\Media'
        ]
    ],
    'params' => $params,
];
if (YII_DEBUG) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*']
    ];
}
if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*']
    ];
}

return $config;