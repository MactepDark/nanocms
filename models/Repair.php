<?php

namespace app\models;

use app\modules\content\models\Content;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "repair".
 *
 * @property int $id
 * @property int $priority
 * @property int $status
 * @property int $price
 */
class Repair extends ActiveRecord {

    public static function tableName() { return 'repair'; }

    public function rules() {
        return [
            [['priority', 'status'], 'required'],
            [['priority', 'status', 'price'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'priority' => 'Priority',
            'status' => 'Status',
            'price' => 'Price',
        ];
    }

    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;
    public static $statusIcons = [
        self::STATUS_DRAFT => '<span class="glyphicon glyphicon-file" style="color: silver;"></span>',
        self::STATUS_ACTIVE => '<span class="glyphicon glyphicon-ok" style="color: green;"></span>'
    ];

    const DEFAULT_NEW_REPAIR_NAME = 'Новый тип ремонта';

    public static function create() {
        $newRepair = new self;
        $newRepair->priority = self::find()->select(['MAX(priority)'])->scalar() + 1;
        $newRepair->status = self::STATUS_DRAFT;
        if ($newRepair->save()) {
            Content::set(CONTENT_REPAIR_NAME, $newRepair->id, 'ru', self::DEFAULT_NEW_REPAIR_NAME);
            return ['result' => true, 'id' => $newRepair->id];
        } else {
            return ['result' => false];
        }
    }

    public static function raisePriority($id) {
        if ($repair = self::findOne($id)) {
            if ($swapRepair = self::findOne(['priority' => $repair->priority - 1])) {
                $swapRepair->priority++; $repair->priority--;
                return $swapRepair->save() && $repair->save();
            }
        }
        return false;
    }

    public static function lowerPriority($id) {
        if ($repair = self::findOne($id)) {
            if ($swapRepair = self::findOne(['priority' => $repair->priority + 1])) {
                $swapRepair->priority--; $repair->priority++;
                return $swapRepair->save() && $repair->save();
            }
        }
        return false;
    }

    public static function remove($id) {
        if ($repair = self::findOne($id)) {
            foreach (self::find()->where(['>', 'priority', $repair->priority])->asArray()->all() as $shiftedRepair) {
                self::updateAll(['priority' => $shiftedRepair['priority'] - 1], ['id' => $shiftedRepair['id']]);
            }
            Content::remove(CONTENT_REPAIR_NAME, $id);
            Service::deleteAll(['repair_id' => $id]);
            return $repair->delete();
        } else { return false; }
    }
}
