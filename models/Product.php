<?php

namespace app\models;

use app\modules\content\BaseContent;
use app\modules\content\models\Content;
use app\modules\media\models\Image;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property int $type
 * @property int $status
 * @property int $priority
 * @property int $auctionPrice
 * @property int $marketPrice
 */
class Product extends ActiveRecord {

    public static function tableName() { return 'product'; }

    public function rules() {
        return [
            [['name', 'type', 'status', 'priority', 'auctionPrice', 'marketPrice'], 'required'],
            [['type', 'status', 'priority', 'auctionPrice', 'marketPrice'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'status' => 'Status',
            'priority' => 'Priority',
            'auctionPrice' => 'Auction Price',
            'marketPrice' => 'Market Price'
        ];
    }

    const TYPE_CAR = 1;

    const DEFAULT_NEW_PRODUCT_NAME = 'New product';

    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    public static $statusNames = [
        self::STATUS_DRAFT => 'Черновик',
        self::STATUS_ACTIVE => 'Активен'
    ];

    public static $productExpensive = [
        CONTENT_PRODUCT_AUCTION_FEE => 'Аукционный сбор',
        CONTENT_PRODUCT_DELIVERY_TO_PORT => 'Доставка в порт',
        CONTENT_PRODUCT_DELIVERY_FROM_US_TO_ODESSA => 'Доставка из порта США в г. Одесса',
        CONTENT_PRODUCT_OUR_SERVICES => 'Наши услуги',
        CONTENT_PRODUCT_UNLOADING_BROKERAGE_EXPERT => 'Выгрузка, брокерские услуги, экспертная оценка',
        CONTENT_PRODUCT_CUSTOM_DUTY_EXCISE_TAX_VAT => 'Таможенная пошлина, акцизный сбор и НДС',
        CONTENT_PRODUCT_CERTIFICATION => 'Сертификация',
        CONTENT_PRODUCT_REGISTRATION => 'Постановка на учет',
        CONTENT_PRODUCT_REPAIRS => 'Ремонт'
    ];

    public static function create() {
        $newProduct = new self;
        $newProduct->name = self::DEFAULT_NEW_PRODUCT_NAME;
        $newProduct->type = self::TYPE_CAR;
        $newProduct->status = self::STATUS_DRAFT;
        $newProduct->priority = self::find()->select(['MAX(priority)'])->where([
            'type' => self::TYPE_CAR
        ])->scalar() + 1;
        $newProduct->auctionPrice = 0;
        $newProduct->marketPrice = 0;
        return ['result' => $newProduct->save(), 'error' => $newProduct->errors, 'id' => $newProduct->id];
    }

    public static function saveProduct($productID, $data = [], $content = []) {
        if ($product = self::findOne($productID)) {
            foreach ($data as $keyPair) {
                switch ($field = $keyPair['name']) {
                    default: $product->$field = trim($keyPair['value']);
                        break;
                }
            }
            foreach ($content as $typeID => $value) {
                Content::set($typeID, $product->id, BaseContent::LANG_NOT_SET, trim($value));
            }
            return ['result' => $product->save(), 'error' => $product->errors];
        } else { return ['result' => false, 'error' => ['Unknown product']]; }
    }

    public static function raisePriority($productID) {
        if ($product = self::findOne($productID)) {
            if ($swapProduct = self::findOne(['type' => $product->type, 'priority' => $product->priority - 1])) {
                $product->priority--; $swapProduct->priority++;
                return $product->save() && $swapProduct->save();
            }
        }
        return false;
    }

    public static function lowerPriority($productID) {
        if ($product = self::findOne($productID)) {
            if ($swapProduct = self::findOne(['type' => $product->type, 'priority' => $product->priority + 1])) {
                $product->priority++; $swapProduct->priority--;
                return $product->save() && $swapProduct->save();
            }
        }
        return false;
    }

    public static function remove($productID) {
        if ($product = self::findOne($productID)) {
            Content::remove(CONTENT_PRODUCT_AUCTION_FEE, $product->id);
            Content::remove(CONTENT_PRODUCT_DELIVERY_TO_PORT, $product->id);
            Content::remove(CONTENT_PRODUCT_DELIVERY_FROM_US_TO_ODESSA, $product->id);
            Content::remove(CONTENT_PRODUCT_OUR_SERVICES, $product->id);
            Content::remove(CONTENT_PRODUCT_UNLOADING_BROKERAGE_EXPERT, $product->id);
            Content::remove(CONTENT_PRODUCT_CUSTOM_DUTY_EXCISE_TAX_VAT, $product->id);
            Content::remove(CONTENT_PRODUCT_CERTIFICATION, $product->id);
            Content::remove(CONTENT_PRODUCT_REGISTRATION, $product->id);
            Content::remove(CONTENT_PRODUCT_REPAIRS, $product->id);
            Image::removeImages(TYPE_PRODUCT_IMAGE, $product->id);
            foreach (self::find()->where([
                'AND', ['=', 'type', $product->type], ['>', 'priority', $product->priority]
            ])->asArray()->all() as $shiftedProduct) {
                self::updateAll(['priority' => $shiftedProduct['priority'] - 1], ['id' => $shiftedProduct['id']]);
            }
            return $product->delete();
        } else { return false; }
    }

}
