<?php

namespace app\models;

use app\modules\content\models\Content;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "language".
 *
 * @property int $id
 * @property string $code
 * @property string $view
 * @property integer $default
 * @property integer $source
 */
class Language extends ActiveRecord {

    public static function tableName() { return 'language'; }

    public function rules() {
        return [
            [['code', 'view', 'default', 'source'], 'required'],
            [['code', 'view'], 'string', 'max' => 4],
            [['default', 'source'], 'integer'],
            [['code'], 'unique'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'view' => 'View',
            'default' => 'Default',
            'source' => 'Source'
        ];
    }

    const IS_NOT = 0;
    const IS_DEFAULT = 1;
    const IS_SOURCE = 1;

    public static function setupLanguages() {
        if (!$defaultLanguage = Yii::$app->cache->get('site-language')) {
            $defaultLanguage = self::find()->select(['code'])->where(['default' => self::IS_DEFAULT])->scalar();
            Yii::$app->cache->set('site-language', $defaultLanguage, SITE_LANGUAGES_CACHE);
        }
        if (!$sourceLanguage = Yii::$app->cache->get('site-source-language')) {
            $sourceLanguage = self::find()->select(['code'])->where(['source' => self::IS_SOURCE])->scalar();
            Yii::$app->cache->set('site-source-language', $sourceLanguage, SITE_LANGUAGES_CACHE);
        }
        Yii::$app->language = $defaultLanguage;
        Yii::$app->sourceLanguage = $sourceLanguage;
        Yii::$app->urlManager->languages = self::getLanguages();
        return null;
    }

    public static function create() {
        $newLanguage = new self;
        $newLanguage->code = 'new';
        $newLanguage->view = 'NEW';
        $newLanguage->default = self::IS_NOT;
        $newLanguage->source = self::IS_NOT;
        Yii::$app->cache->delete('site-languages');
        return ['result' => $newLanguage->save(), 'error' => $newLanguage->errors, 'id' => $newLanguage->id];
    }

    public static function setDefault($langID) {
        if ($language = self::findOne($langID)) {
            self::updateAll(['default' => self::IS_NOT]);
            $language->default = self::IS_DEFAULT;
            return $language->save();
        } else { return false; }
    }

    public static function setSource($langID) {
        if ($language = self::findOne($langID)) {
            self::updateAll(['source' => self::IS_NOT]);
            $language->source = self::IS_SOURCE;
            return $language->save();
        } else { return false; }
    }

    public static function remove($langID) {
        if ($language = self::findOne($langID)) {
            Content::remove(CONTENT_LANGUAGE_NAME, $langID);
            Content::deleteAll(['language' => $language->code]);
            Message::deleteAll(['language' => $language->code]);
            Yii::$app->cache->flush();
            return $language->delete();
        } else { return false; }
    }

    public static function getView($langCode = false) {
        if ($language = self::findOne(['code' => $langCode === false ? Yii::$app->language : $langCode])) {
            return $language->view;
        } else { return 'N/A'; }
    }

    public static function getName($langCode = false) {
        if ($language = self::findOne(['code' => $langCode === false ? Yii::$app->language : $langCode])) {
            return Content::get(CONTENT_LANGUAGE_NAME, $language->id, $langCode);
        } else { return 'N/A'; }
    }

    public static function getLanguages() {
        if (!$languages = Yii::$app->cache->get('site-languages')) {
            foreach (self::find()->asArray()->all() as $language) {
                $languages[strtolower($language['view'])] = $language['code'];
            }
            Yii::$app->cache->set('site-languages', $languages, SITE_LANGUAGES_CACHE);
        }
        return $languages;
    }

    public static function getSiteLanguages() {
        return self::find()->asArray()->all();
    }
}
