<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property int $repair_id
 * @property int $cloth_id
 * @property int $price_min
 * @property int $price_max
 */
class Service extends ActiveRecord {

    public static function tableName() { return 'service'; }

    public function rules() {
        return [
            [['repair_id', 'cloth_id'], 'required'],
            [['repair_id', 'cloth_id', 'price_min', 'price_max'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'repair_id' => 'Repair ID',
            'cloth_id' => 'Cloth ID',
            'price_min' => 'Price Min',
            'price_max' => 'Price Max'
        ];
    }

    public static function getSelectedRepairs($clothID) {
        return self::find()->select(['id', 'repair_id', 'price_min', 'price_max'])->where(['cloth_id' => $clothID])->asArray()->all();
    }

    public static function getSelectedClothes($repairID) {
        return self::find()->select(['id', 'cloth_id', 'price_min', 'price_max'])->where(['repair_id' => $repairID])->asArray()->all();
    }

    public static function getAvailableRepairs($clothID) {
        return Repair::find()->select(['id'])->where([
            'NOT IN', 'id', ArrayHelper::map(self::getSelectedRepairs($clothID), 'repair_id', 'repair_id')
        ])->column();
    }

    public static function getAvailableClothes($repairID) {
        return Cloth::find()->select(['id'])->where([
            'NOT IN', 'id', ArrayHelper::map(self::getSelectedClothes($repairID), 'cloth_id', 'cloth_id')
        ])->column();
    }

    public static function addService($repairID, $clothID) {
        $newService = new self;
        $newService->repair_id = $repairID;
        $newService->cloth_id = $clothID;
        return $newService->save();
    }

}
