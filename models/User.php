<?php namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * Class User
 * @property $id integer
 * @property $name string
 * @property $email string
 * @property $auth_key string
 * @property $password_hash string
 * @property $token string
 * @property $role integer
 * @property $status integer
 * @property $online integer
 * @property $created_at integer
 */
class User extends ActiveRecord implements IdentityInterface {

    const MIN_PASSWORD_LENGTH = 6;
    const MAX_PASSWORD_LENGTH = 9;

    const STATUS_INACTIVE = 0;
    const STATUS_INVITED = 3;
    const STATUS_ACTIVE = 5;

    public static $statusIcons = [
        self::STATUS_INACTIVE => '<span class="glyphicon glyphicon-ban-circle" style="color: red;"></span>',
        self::STATUS_INVITED => '<span class="glyphicon glyphicon-envelope" style="color: blue;"></span>',
        self::STATUS_ACTIVE => '<span class="glyphicon glyphicon-ok" style="color: green;"></span>'
    ];

    public static function getStatusNames($statusID = false) {
        $statusNames = [
            self::STATUS_INACTIVE => Yii::t('admin', 'Заблокирован'),
            self::STATUS_INVITED => Yii::t('admin', 'Приглашён'),
            self::STATUS_ACTIVE => Yii::t('admin', 'Активный')
        ];
        return $statusID === false ? $statusNames : array_key_exists($statusID, $statusNames) ? $statusNames[$statusID] : false;
    }

    const ROLE_ADMIN = 1;

    public static function tableName() { return 'user'; }

    public static function getName($userID = false) {
        if ($user = self::findOne($userID === false ? Yii::$app->user->id : $userID)) {
            return $user->name;
        } else { return false; }
    }

    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email) {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getId() {
        return $this->getPrimaryKey();
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public static function sendInvite($email) {
        if ($email = filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if (!self::findOne(['email' => $email])) {
                list($username, $mailDomain) = explode('@', $email);
                $randomPassword = Yii::$app->security->generateRandomString(rand(self::MIN_PASSWORD_LENGTH, self::MAX_PASSWORD_LENGTH));
                $invitedUser = new self;
                $invitedUser->name = $username;
                $invitedUser->email = $email;
                $invitedUser->auth_key = 'empty';
                $invitedUser->password_hash = Yii::$app->security->generatePasswordHash($randomPassword);
                $invitedUser->token = md5(Yii::$app->security->generateRandomString(128));
                $invitedUser->role = self::ROLE_ADMIN;
                $invitedUser->status = self::STATUS_INVITED;
                $invitedUser->online = 0;
                $invitedUser->created_at = time();
                if ($invitedUser->save()) {
                    return Yii::$app->mailer->compose('user-invite', [
                        'username' => $username, 'password' => $randomPassword,
                        'link' => Url::toRoute(['admin/registration', 'token' => $invitedUser->token], true)
                    ])->setFrom(['no-reply@'.Yii::$app->request->hostName])->setSubject('Registration invite')->setTo([
                        $email => $username
                    ])->send();
                }
            }
        }
        return false;
    }

    public static function changePassword($userID, $password, $confirm) {
        if ($user = self::findOne($userID)) {
            if (strlen($password) >= self::MIN_PASSWORD_LENGTH) {
                if ($password === $confirm) {
                    $user->setPassword($password);
                    return ['result' => $user->save(), 'error' => $user->errors];
                } else { return ['result' => false, 'error' => Yii::t('admin', 'Пароли не совпадают')]; }
            } else { return ['result' => false, 'error' => Yii::t('admin', 'Минимальная длинна пароля {n} символов', ['n' => self::MIN_PASSWORD_LENGTH])]; }
        } else { return ['result' => false, 'error' => Yii::t('admin', 'Неизвестный пользователь')]; }
    }

    public static function remove($userID) {
        if ($user = self::findOne($userID)) {
            return $user->delete();
        } else { return false; }
    }

}
