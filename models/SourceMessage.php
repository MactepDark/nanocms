<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "source_message".
 *
 * @property int $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $messages
 */
class SourceMessage extends ActiveRecord {

    public static function tableName() { return 'source_message'; }

    public function rules() {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'message' => 'Message',
        ];
    }

    public function getMessages() {
        return $this->hasMany(Message::className(), ['id' => 'id']);
    }

    const SECTION_ADMIN = 'admin';
    const SECTION_SITE = 'app';

    public static function getSectionName($section) {
        $sections = [
            self::SECTION_ADMIN => Yii::t('admin', 'Админка'),
            self::SECTION_SITE => Yii::t('admin', 'Лэндинг')
        ];
        return array_key_exists($section, $sections) ? $sections[$section] : 'N/A';
    }

    public static function refreshTranslations() {
        Yii::$app->consoleRunner->run('message/extract @app/config/i18n.php');
        return true;
    }

    public static function getTranslations() {
        $result = [];
        foreach (self::find()->asArray()->all() as $source) {
            $result[$source['category']][$source['id']]['source'] = $source['message'];
            foreach (Language::find()->select(['code'])->column() as $langCode) {
                if (!$translation = Message::findOne(['id' => $source['id'], 'language' => $langCode])) {
                    $translation = new Message();
                    $translation->id = $source['id'];
                    $translation->language = $langCode;
                    $translation->save();
                }
                $result[$source['category']][$source['id']]['translations'][$langCode] = $translation->translation;
            }
//            foreach (Message::find()->where(['id' => $source['id']])->asArray()->all() as $translation) {
//                $result[$source['category']][$source['id']]['translations'][$translation['language']] = $translation['translation'];
//            }
        }
        return $result;
    }
}
