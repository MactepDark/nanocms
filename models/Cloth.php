<?php

namespace app\models;

use app\modules\content\models\Content;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cloth".
 *
 * @property int $id
 * @property int $priority
 * @property int $status
 * @property int $price
 */
class Cloth extends ActiveRecord {

    public static function tableName() { return 'cloth'; }

    public function rules() {
        return [
            [['priority', 'status'], 'required'],
            [['priority', 'status', 'price'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'priority' => 'Priority',
            'status' => 'Status',
            'price' => 'Price',
        ];
    }
    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;
    public static $statusIcons = [
        self::STATUS_DRAFT => '<span class="glyphicon glyphicon-file" style="color: silver;"></span>',
        self::STATUS_ACTIVE => '<span class="glyphicon glyphicon-ok" style="color: green;"></span>'
    ];

    const DEFAULT_NEW_CLOTH_NAME = 'Новая одежда';

    public static function create() {
        $newCloth = new self;
        $newCloth->priority = self::find()->select(['MAX(priority)'])->scalar() + 1;
        $newCloth->status = self::STATUS_DRAFT;
        if ($newCloth->save()) {
            Content::set(CONTENT_CLOTH_NAME, $newCloth->id, 'ru', self::DEFAULT_NEW_CLOTH_NAME);
            return ['result' => true, 'id' => $newCloth->id];
        } else {
            return ['result' => false];
        }
    }

    public static function raisePriority($id) {
        if ($cloth = self::findOne($id)) {
            if ($swapCloth = self::findOne(['priority' => $cloth->priority - 1])) {
                $swapCloth->priority++; $cloth->priority--;
                return $swapCloth->save() && $cloth->save();
            }
        }
        return false;
    }

    public static function lowerPriority($id) {
        if ($cloth = self::findOne($id)) {
            if ($swapCloth = self::findOne(['priority' => $cloth->priority + 1])) {
                $swapCloth->priority--; $cloth->priority++;
                return $swapCloth->save() && $cloth->save();
            }
        }
        return false;
    }

    public static function remove($id) {
        if ($cloth = self::findOne($id)) {
            foreach (self::find()->where(['>', 'priority', $cloth->priority])->asArray()->all() as $shiftedCloth) {
                self::updateAll(['priority' => $shiftedCloth['priority'] - 1], ['id' => $shiftedCloth['id']]);
            }
            Content::remove(CONTENT_CLOTH_NAME, $id);
            Service::deleteAll(['cloth_id' => $id]);
            return $cloth->delete();
        } else { return false; }
    }
}
