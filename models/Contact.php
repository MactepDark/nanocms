<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property int $type
 * @property int $priority
 * @property string $value
 * @property int $primary
 * @property int $selected
 */
class Contact extends ActiveRecord {

    public static function tableName() { return 'contact'; }

    public function rules() {
        return [
            [['type', 'priority', 'primary', 'selected'], 'required'],
            [['type', 'priority', 'primary', 'selected'], 'integer'],
            [['value'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'priority' => 'Priority',
            'value' => 'Value',
            'primary' => 'Primary',
            'selected' => 'Selected',
        ];
    }

    const SECONDARY = 0;
    const PRIMARY = 1;
    const NOT_SELECTED = 0;
    const SELECTED = 1;

    const TYPE_PHONE = 1;
    const TYPE_EMAIL = 2;
    const TYPE_SKYPE = 3;
    const TYPE_FACEBOOK = 4;
    const TYPE_TWITTER = 5;
    const TYPE_INSTAGRAM = 6;
    const TYPE_YOUTUBE = 7;
    const TYPE_GOOGLE_PLUS = 8;
    const TYPE_VK = 9;

    public static $socialTypes = [
        self::TYPE_FACEBOOK, self::TYPE_TWITTER, self::TYPE_INSTAGRAM,
        self::TYPE_YOUTUBE, self::TYPE_GOOGLE_PLUS, self::TYPE_VK
    ];

    public static function getTypeNames($typeID = false) {
        $types = [
            self::TYPE_PHONE => Yii::t('admin', 'Телефон'),
            self::TYPE_EMAIL => 'E-mail',
            self::TYPE_SKYPE => Yii::t('admin', 'Skype'),
            self::TYPE_FACEBOOK => Yii::t('admin', 'Facebook'),
            self::TYPE_TWITTER => Yii::t('admin', 'Twitter'),
            self::TYPE_INSTAGRAM => Yii::t('admin', 'Instagram'),
            self::TYPE_YOUTUBE => Yii::t('admin', 'Youtube'),
            self::TYPE_GOOGLE_PLUS => Yii::t('admin', 'Google+'),
            self::TYPE_VK => Yii::t('admin', 'Вконтакте')
        ];
        return array_key_exists($typeID, $types) ? $types[$typeID] : false;
    }

    public static function add($typeID) {
        $newContact = new self;
        $newContact->type = $typeID;
        $newContact->priority = self::find()->select(['MAX(priority)'])->where(['type' => $typeID])->scalar() + 1;
        $newContact->primary = $newContact->priority > 1 ? self::SECONDARY : self::PRIMARY;
        $newContact->selected = self::NOT_SELECTED;
        return ['result' => $newContact->save(), 'error' => $newContact->errors, 'id' => $newContact->id];
    }

    public static function selectPrimary($contactID) {
        if ($contact = self::findOne($contactID)) {
            self::updateAll(['primary' => self::SECONDARY], ['type' => $contact->type]);
            $contact->primary = self::PRIMARY;
            return $contact->save();
        }
        return false;
    }

    public static function raisePriority($contactID) {
        if ($contact = self::findOne($contactID)) {
            if ($swapContact = self::findOne(['type' => $contact->type, 'priority' => $contact->priority - 1])) {
                $swapContact->priority++; $contact->priority--;
                return $swapContact->save() && $contact->save();
            }
        }
        return false;
    }

    public static function lowerPriority($contactID) {
        if ($contact = self::findOne($contactID)) {
            if ($swapContact = self::findOne(['type' => $contact->type, 'priority' => $contact->priority + 1])) {
                $swapContact->priority--; $contact->priority++;
                return $swapContact->save() && $contact->save();
            }
        }
        return false;
    }

    public static function remove($contactID) {
        if ($contact = self::findOne($contactID)) {
            foreach (self::find()->where([
                'AND', ['=', 'type', $contact->type], ['>', 'priority', $contact->priority]
            ])->asArray()->all() as $shiftedContact) {
                self::updateAll(['priority' => $shiftedContact['priority'] - 1], ['id' => $shiftedContact['id']]);
            }
            return $contact->delete();
        } else { return false; }
    }

    public static function getContactsByType($typeWhere = []) {
        return self::find()->where($typeWhere)->orderBy(['type' => SORT_ASC, 'priority' => SORT_ASC])->asArray()->all();
    }

    public static function cleanNumber($dirtyNumber) {
        return str_replace([' ', '-', '(', ')'], '', $dirtyNumber);
    }

}
