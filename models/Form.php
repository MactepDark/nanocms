<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\Cookie;

/**
 * This is the model class for table "form".
 *
 * @property int $id
 * @property int $type
 * @property int $status
 * @property string $content
 * @property string $ip
 * @property string $ua
 * @property string $geo
 * @property int $created_at
 */
class Form extends ActiveRecord {

    public static function tableName() { return 'form'; }

    public function rules() {
        return [
            [['type', 'status', 'ip', 'ua', 'created_at'], 'required'],
            [['type', 'status', 'created_at'], 'integer'],
            [['content'], 'string'],
            [['ua', 'geo'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 16]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'status' => 'Status',
            'content' => 'Content',
            'ip' => 'Ip',
            'ua' => 'Ua',
            'geo' => 'GEO',
            'created_at' => 'Created At',
        ];
    }

    const GEO_API_URL = 'http://ip-api.com/json/';

    const LIMIT_5 = 5;
    const LIMIT_10 = 10;
    const LIMIT_25 = 25;
    const LIMIT_50 = 50;

    public static $pageLimits = [self::LIMIT_5, self::LIMIT_10, self::LIMIT_25, self::LIMIT_50];

    const TYPE_ALL = 0;
    const TYPE_DEFAULT = 1;
    const TYPE_QUICK = 2;

    const STATUS_ALL = -1;
    const STATUS_NEW = 0;
    const STATUS_VIEW = 1;

    public static $statusIcons = [
        self::STATUS_NEW => '<span class="glyphicon glyphicon-eye-open" style="color: blue;"></span>',
        self::STATUS_VIEW => '<span class="glyphicon glyphicon-eye-close" style="color: grey;"></span>',
    ];

    const FIELD_NAME = 'name';
    const FIELD_PHONE = 'phone';
    const FIELD_BUDGET = 'budget';
    const FIELD_MODEL = 'model';
    const FIELD_YEAR = 'year';
    const FIELD_PRODUCT = 'product';
    const FIELD_SERVICE = 'service';

    public static $packedData;

    public static function getTypeNames($typeID = false) {
        $types = [
            self::TYPE_DEFAULT => Yii::t('admin', 'Форма по-умолчанию'),
            self::TYPE_QUICK => Yii::t('admin', 'Короткая форма')
        ];
        return $typeID === false ? $types : (array_key_exists($typeID, $types) ? $types[$typeID] : false);
    }

    public static function getStatusNames($statusID = false) {
        $statusNames = [
            self::STATUS_NEW => Yii::t('admin', 'Новые'),
            self::STATUS_VIEW => Yii::t('admin', 'Просмотреные')
        ];
        return $statusID === false ? $statusNames : (array_key_exists($statusID, $statusNames) ? $statusNames[$statusID] : false);
    }

    public static function getFieldNames($field = false) {
        $fieldNames = [
            self::FIELD_NAME => Yii::t('admin', 'Имя'),
            self::FIELD_PHONE => Yii::t('admin', 'Телефон'),
            self::FIELD_BUDGET => Yii::t('admin', 'Бюджет'),
            self::FIELD_MODEL => Yii::t('admin', 'Марка / модель'),
            self::FIELD_YEAR => Yii::t('admin', 'Год выпуска'),
            self::FIELD_PRODUCT => Yii::t('admin', 'Автомобиль'),
            self::FIELD_SERVICE => Yii::t('admin', 'Услуга')
        ];
        return $field === false ? $fieldNames : array_key_exists($field, $fieldNames) ? $fieldNames[$field] : false;
    }

    public static function formSubmit($formType, $data) {
        $errors = self::validateForm($data);
        if (count($errors) === 0) {
            $newForm = new self;
            $newForm->type = $formType;
            $newForm->status = self::STATUS_NEW;
            $newForm->content = json_encode((object)self::$packedData);
            $newForm->ip = Yii::$app->request->userIP;
            $newForm->ua = Yii::$app->request->userAgent;
            $newForm->created_at = time();
            if ($newForm->save()) {
                self::sendNotify($newForm->id);
                return ['result' => true];
            } else { return ['result' => false, 'error' => $newForm->errors]; }
        } else { return ['result' => false, 'error' => $errors]; }
    }

    private static function validateForm($data) {
        $errors = [];
        foreach ($data as $keyPair) {
            switch ($field = $keyPair['name']) {
                case self::FIELD_NAME: if (strlen($keyPair['value']) === 0) {
                    $errors[self::FIELD_NAME] = Yii::t('admin', 'Имя не может быть пустым');
                } elseif (strlen($keyPair['value']) < 3) {
                    $errors[self::FIELD_NAME] = Yii::t('admin', 'Имя должно быть больше 3-х символов');
                } elseif (strlen($keyPair['value']) > 32) {
                    $errors[self::FIELD_NAME] = Yii::t('admin', 'Слишком длинное имя');
                } elseif (strlen($keyPair['value']) != strlen(strip_tags($keyPair['value']))) {
                    $errors[self::FIELD_NAME] = Yii::t('admin', 'Имя содержит запрещённые символы');
                } else { self::$packedData[self::FIELD_NAME] = trim(strip_tags($keyPair['value'])); }
                    break;

                case self::FIELD_PHONE: if (strlen($keyPair['value']) === 0) {
                    $errors[self::FIELD_PHONE] = Yii::t('admin', 'Номер телефона обязателен');
                } elseif (strpos($keyPair['value'], '_') !== false) {
                    $errors[self::FIELD_PHONE] = Yii::t('admin', 'Некорректный номер телефона');
                } else { self::$packedData[self::FIELD_PHONE] = trim(strip_tags($keyPair['value'])); }
                    break;

                case self::FIELD_BUDGET: if (strlen($keyPair['value']) === 0) {
                    $errors[self::FIELD_BUDGET] = Yii::t('admin', 'Поле обязательно');
                } elseif (!filter_var($keyPair['value'], FILTER_VALIDATE_INT)) {
                    $errors[self::FIELD_BUDGET] = Yii::t('admin', 'Значение должно быть целым числом');
                } else {
                    self::$packedData[self::FIELD_BUDGET] = trim(strip_tags($keyPair['value']));
                }
                    break;

                case self::FIELD_MODEL: if (strlen($keyPair['value']) === 0) {
                    $errors[self::FIELD_MODEL] = Yii::t('admin', 'Поле обязательно');
                } else {
                    self::$packedData[self::FIELD_MODEL] = trim(strip_tags($keyPair['value']));
                }
                    break;

                case self::FIELD_YEAR: if (strlen($keyPair['value']) === 0) {
                    $errors[self::FIELD_YEAR] = Yii::t('admin', 'Поле обязательно');
                } elseif(!filter_var($keyPair['value'], FILTER_VALIDATE_INT)) {
                    $errors[self::FIELD_YEAR] = Yii::t('admin', 'Год выпуска, цифрой');
                } else {
                    self::$packedData[self::FIELD_YEAR] = trim(strip_tags($keyPair['value']));
                }
                    break;
                case self::FIELD_PRODUCT: if (strlen($keyPair['value']) > 0 && filter_var($keyPair['value'], FILTER_VALIDATE_INT)) {
                    self::$packedData[self::FIELD_PRODUCT] = (int)trim(strip_tags($keyPair['value']));
                }
                    break;
                case self::FIELD_SERVICE: if (strlen($keyPair['value']) > 0) {
                    self::$packedData[self::FIELD_SERVICE] = trim(strip_tags($keyPair['value']));
                }
                    break;

                default: break;
            }
        }
        return $errors;
    }

    public static function search($searchQuery) {
        self::saveSearchQuery($searchQuery);
        $leads = self::find()->orderBy(['created_at' => SORT_DESC]);

        if (strlen($searchQuery['query']) > 0) {
            $leads->andWhere(['LIKE', 'content', $searchQuery['query']]);
        }
        if ($searchQuery['type'] > self::TYPE_ALL) {
            $leads->andWhere(['type' => $searchQuery['type']]);
        }
        if ($searchQuery['status'] > self::STATUS_ALL) {
            $leads->andWhere(['status' => $searchQuery['status']]);
        }
        $searchQuery['totalCount'] = $leads->count();
        if ($searchQuery['totalCount'] > $searchQuery['pageLimit']) {
            $searchQuery['totalPages'] = floor($searchQuery['totalCount'] / $searchQuery['pageLimit']) + (($searchQuery['totalCount'] % $searchQuery['pageLimit']) > 0 ? 1 : 0);
            if ($searchQuery['currentPage'] > $searchQuery['totalPages']) { $searchQuery['currentPage'] = 1; }
        } else {
            $searchQuery['totalPages'] = $searchQuery['currentPage'] = 1;
        }
        return [
            'leads' => $leads->offset(($searchQuery['currentPage'] - 1) * $searchQuery['pageLimit'])
                ->limit($searchQuery['pageLimit'])->asArray()->all(), 'searchQuery' => $searchQuery
        ];
    }

    public static function sendNotify($leadID) {
        if ($form = self::findOne($leadID)) {
            return Yii::$app->mailer->compose('mail-notify', ['form' => $form, 'content' => json_decode($form->content)])
                ->setSubject(Yii::t('admin', 'Новая заявка с сайта'))
                ->setTo(Yii::$app->params['sendNotifyFormToMail'])
                ->setFrom(['no-reply@'.Yii::$app->request->hostName => Yii::$app->name])->send();
        }
        return false;
    }

    public static function fetchGEO($formID) {
        if ($form = self::findOne($formID)) {
            $ch = curl_init(self::GEO_API_URL.$form->ip.'?lang=ru');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            $response = json_decode(curl_exec($ch));
            curl_close($ch);
            if ($response->status == 'success') {
                $form->geo = json_encode((object)[
                    'country' => $response->country,
                    'countryCode' => $response->countryCode,
                    'city' => $response->city,
                    'isp' => $response->isp,
                    'lat' => $response->lat,
                    'lng' => $response->lon
                ]);
                return $form->save();
            }
        }
        return false;
    }

    private static function saveSearchQuery($searchQuery) {
        Yii::$app->response->cookies->add(new Cookie(['name' => 'leads-search-query', 'value' => $searchQuery['query']]));
        Yii::$app->response->cookies->add(new Cookie(['name' => 'leads-search-type', 'value' => $searchQuery['type']]));
        Yii::$app->response->cookies->add(new Cookie(['name' => 'leads-search-status', 'value' => $searchQuery['status']]));
        Yii::$app->response->cookies->add(new Cookie(['name' => 'leads-search-currentPage', 'value' => $searchQuery['currentPage']]));
        Yii::$app->response->cookies->add(new Cookie(['name' => 'leads-search-pageLimit', 'value' => $searchQuery['pageLimit']]));
    }

    public static function cleanNumber($dirtyNumber) {
        return str_replace('-', '', filter_var($dirtyNumber, FILTER_SANITIZE_NUMBER_INT));
    }
}
