<?php namespace app\models;

use Yii;
use yii\base\Model;

class Admin extends Model {

    public static function saveModelField($model, $field, $ID, $value) {
        $class = 'app\\models\\'.$model;
        Yii::$app->cache->flush();
        return $class::updateAll([$field => $value], ['id' => $ID]);
    }
}