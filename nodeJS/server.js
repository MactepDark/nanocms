// const WebSocket = require('ws');
// const ws = new WebSocket.Server({port: 8080});
// ws.on('connection', ws => {
//     ws.on('message', message => {
//         console.log(`Message length: [${message.length}]`);
//         console.log(message);
//     });
//     ws.send('Ok');
// });

class WSApp {
    constructor(serverPort = 8080) {
        let ws = require('ws');
        this.DB = new (require('./src/database'));
        this.WS = new ws.Server({port: serverPort}).on('connection', function(ws) {
            ws.on('message', function(message) {
                let msg = JSON.parse(message);
                // console.log(`Received: [${message}]`);
                let result = {result: false, errors: []};
                switch (msg.method) {
                    case 'userOnline': result.result = true;
                        break;
                    default: result.errors.push('Unknown method!');
                        break;
                }
                ws.send(JSON.stringify(result));
            });
        });
    }
    run() {
        console.log('Running...');
        // console.log(this.WS);
        // console.log(this.DB.delete('item', {id: 8}));
        // console.log(this.DB.update('item', {name: 'TEST'}, {id: 7}));
        // console.log(this.DB.insert('item', {type: 1, name: 'Test', description: 'Lorem ipsum'}));
        // console.log(this.DB.select('user', ['id', 'first_name', 'last_name'], {status: 10}));
    }
}

const server = new WSApp();
server.run();