class DataBase {
    constructor() {
        this.DB = require('mysql').createConnection({
            host: 'db',
            user: 'root',
            password: '1638432768',
            database: 'sewing'
        });
        this.DB.connect();
    }
    select(table, fields = ['*'], where = {}) {
        this.DB.query('SELECT ?? FROM ?? WHERE ?', [fields, table, where], (error, result) => {
            if (!error) { return result; } else { return false; }
        });
    }
    insert(table, values = {}) {
        this.DB.query('INSERT INTO ?? SET ?', [table, values], (error, result) => {
            // console.log(result);
            return result.affectedRows > 0 ? result.insertId : false;
        });
    }
    update(table, values, where) {
        this.DB.query('UPDATE ?? SET ?? WHERE ?', [table, values, where], (error, result) => {
            return result.affectedRows;
            // console.log(db.sql);
            // console.log(result);
        });
    }
    delete(table, where) {
        let db = this.DB.query('DELETE FROM ?? WHERE ?', [table, where], (error, result) => {
            console.log(db.sql);
            console.log(result);
        });
    }
}

module.exports = DataBase;