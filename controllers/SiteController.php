<?php namespace app\controllers;

use app\models\Form;
use app\models\Product;
use app\modules\content\BaseContent;
use app\modules\content\models\Content;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller {

    public function actions() { return ['error' => ['class' => 'yii\web\ErrorAction']]; }

//    public function beforeAction($action) {
//        return parent::beforeAction($action);
//    }

    public function actionIndex() {
        $this->view->registerMetaTag([
            'name' => 'title',
            'content' => Yii::$app->view->title = Content::get(CONTENT_SEO_TITLE, BaseContent::OBJ_NOT_SET)
        ]);
        $this->view->registerMetaTag([
            'name' => 'description',
            'content' => Content::get(CONTENT_SEO_DESCRIPTION, BaseContent::OBJ_NOT_SET)
        ]);
        return $this->render('index', [
            'products' => Product::find()->select(['id', 'name', 'auctionPrice', 'marketPrice'])->where([
                'status' => Product::STATUS_ACTIVE
            ])->orderBy(['priority' => SORT_ASC])->asArray()->all()
        ]);
    }

    public function actionPrivacyPolicy() {
        $this->view->title = Yii::t('app', 'Политика конфиденциальности');
        return $this->render('privacy-policy', [
            'privacyPolicy' => Content::get(CONTENT_PRIVACY_POLICY_TEXT, BaseContent::OBJ_NOT_SET)
        ]);
    }

    public function actionRobots() {
        Yii::$app->response->headers->set('Content-type', 'text/plain');
        Yii::$app->response->format = Response::FORMAT_RAW;
        $siteIndex = Content::get(CONTENT_SEO_INDEXATION, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET);
        return implode(PHP_EOL, [
            'User-agent: *', 'Disallow: /admin', $siteIndex ? 'Allow: /' : 'Disallow: /', 'Host: '.Yii::$app->request->hostInfo
        ]);
    }

    public function actionAjax() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = ['result' => false, 'error' => [], 'post' => $post = Yii::$app->request->post()];
        if (isset($post['method'])) {
            switch ($post['method']) {
                case 'formSubmit': $result = Form::formSubmit($post['type'], $post['data']);
                    break;

                default: $result['error'][] = 'Unknown method'; break;
            }
        }
        return $result;
    }
}
