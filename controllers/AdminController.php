<?php namespace app\controllers;

use app\models\Admin;
use app\models\Cloth;
use app\models\Contact;
use app\models\Form;
use app\models\Language;
use app\models\Message;
use app\models\Product;
use app\models\Repair;
use app\models\Service;
use app\models\SourceMessage;
use app\models\User;
use app\modules\content\BaseContent;
use app\modules\content\ContentAsset;
use app\modules\content\models\Content;
use app\modules\media\MediaAsset;
use nox\components\http\userAgent\UserAgentParser;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\LoginForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\View;

class AdminController extends Controller {

    public $layout = 'admin-main';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index', 'products', 'edit', 'leads', 'contacts', 'settings', 'languages',
                            'users', 'translations', 'privacy-policy', 'logout', 'ajax'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login', 'registration'],
                        'allow' => true,
                        'roles' => ['?']
                    ]
                ],
            ]
        ];
    }

    public function actions() { return ['error' => ['class' => 'yii\web\ErrorAction']]; }

    public function beforeAction($action) {
        if (!in_array($action->id, ['login', 'logout', 'ajax'])) {
            ContentAsset::register($this->view);
            MediaAsset::register($this->view);
            $this->view->registerJs('Admin.ajax.url = "'.Url::toRoute(['admin/ajax']).'";', View::POS_END);
        }
        return parent::beforeAction($action);
    }

    public function actionIndex() {
        $this->view->title = 'Dashboard nanoV2';
        return $this->render('index');
    }

    public function actionProducts() {
        $this->view->title = Yii::t('admin', 'Продукция');
        $this->view->registerJsFile('/js/admin/product.js');
        return $this->render('products', [
            'products' => Product::find()->orderBy(['priority' => SORT_ASC])->asArray()->all()
        ]);
    }

//    public function actionRepairs() {
//        $this->view->title = Yii::t('admin', 'Типы ремонта');
//        $this->view->registerJsFile('/js/admin/service.js');
//        $this->view->registerJs('Service.Model = "Repair";', View::POS_END);
//        return $this->render('repairs', [
//            'repairs' => Repair::find()->orderBy(['priority' => SORT_ASC])->asArray()->all()
//        ]);
//    }

//    public function actionClothes() {
//        $this->view->title = Yii::t('admin', 'Виды одежды');
//
//        $this->view->registerJsFile('/js/admin/service.js');
//        $this->view->registerJs('Service.Model = "Cloth";', View::POS_END);
//        return $this->render('clothes', [
//            'clothes' => Cloth::find()->orderBy(['priority' => SORT_ASC])->asArray()->all()
//        ]);
//    }

    public function actionEdit() {
        $this->view->title = Yii::t('admin', 'Лэндинг');
        return $this->render('edit');
    }

    public function actionLeads() {
        $this->view->title = Yii::t('admin', 'Заявки');
        $searchQuery = [
            'query' => Yii::$app->request->cookies->getValue('leads-search-query', ''),
            'type' => Yii::$app->request->cookies->getValue('leads-search-type', Form::TYPE_ALL),
            'status' => Yii::$app->request->cookies->getValue('leads-search-status', Form::STATUS_ALL),
            'currentPage' => Yii::$app->request->cookies->getValue('leads-search-currentPage', 1),
            'pageLimit' => Yii::$app->request->cookies->getValue('leads-search-pageLimit', Form::LIMIT_10)
        ];
        if ($apiKey = Content::get(CONTENT_CONTACT_API_KEY, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET)) {
            $this->view->registerJsFile(implode('=', ['//maps.googleapis.com/maps/api/js?key', $apiKey]), ['position' => View::POS_HEAD]);
        }
        $this->view->registerJs('Admin.searchQuery = '.json_encode($searchQuery).';', View::POS_END);
        return $this->render('leads', Form::search($searchQuery));
    }

    public function actionContacts() {
        $this->view->title = Yii::t('admin', 'Контакты');
        if ($apiKey = Content::get(CONTENT_CONTACT_API_KEY, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET)) {
            $this->view->registerJsFile(implode('=', ['//maps.googleapis.com/maps/api/js?key', $apiKey]), ['position' => View::POS_HEAD]);
        }
        $lat = Content::get(CONTENT_CONTACT_MAP_LAT, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET);
        $lng = Content::get(CONTENT_CONTACT_MAP_LNG, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET);
        $zoom = Content::get(CONTENT_CONTACT_MAP_ZOOM, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET);
        return $this->render('contacts', [
            'lat' => $lat ? $lat : BaseContent::DEFAULT_MAP_LAT,
            'lng' => $lng ? $lng : BaseContent::DEFAULT_MAP_LNG,
            'zoom' => $zoom ? $zoom : BaseContent::DEFAULT_MAP_ZOOM,
        ]);
    }

    public function actionSettings() {
        $this->view->title = Yii::t('admin', 'Настройки');
        return $this->render('settings');
    }

    public function actionLanguages() {
        $this->view->title = Yii::t('admin', 'Языки');
        return $this->render('languages');
    }

    public function actionUsers() {
        $this->view->title = Yii::t('admin', 'Пользователи');
        return $this->render('users');
    }

    public function actionTranslations() {
        $this->view->title = Yii::t('admin', 'Переводы');
        Yii::$app->cache->delete('site-languages');
        return $this->render('translations', [
            'languages' => Language::getLanguages(),
            'translations' => SourceMessage::getTranslations()
        ]);
    }

    public function actionPrivacyPolicy() {
        $this->view->title = Yii::t('admin', 'Политика конфиденциальности');
        return $this->render('privacy-policy');
    }

    public function actionLogin() {
        if (Yii::$app->user->isGuest) {
            $this->view->title = Yii::t('admin', 'Авторизация');
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) { return $this->goBack(); }
            $model->password = '';
            return $this->render('login', ['model' => $model]);
        } else { return $this->goHome(); }
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionRegistration($token) {
        if ($user = User::findOne(['token' => $token, 'status' => User::STATUS_INVITED])) {
            $user->status = User::STATUS_ACTIVE;
            $user->token = null;
            if ($user->save()) {
                Yii::$app->user->login($user);
                return $this->redirect(['admin/index']);
            } else { die('Unable to update user'); }
        } else { throw new NotFoundHttpException(); }
    }

    public function actionAjax() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = ['result' => false, 'error' => [], 'post' => $post = Yii::$app->request->post()];
        if (isset($post['method'])) {
            switch ($post['method']) {
                case 'fetchLanguages': $result = ['result' => true, 'html' => $this->renderAjax('languages')];
                    break;
                case 'fetchLanguageForm': $result = [
                    'result' => true, 'html' => $this->renderAjax('language-form', [
                        'language' => Language::findOne($post['langID'])
                    ])
                ];
                    break;
                case 'saveModelField': $result['result'] = Admin::saveModelField($post['model'], $post['field'], $post['ID'], $post['value']);
                    break;

                //Language
                case 'addLanguage': $result = Language::create();
                    break;
                case 'setLanguageDefault': $result['result'] = Language::setDefault($post['langID']);
                    break;
                case 'setLanguageSource': $result['result'] = Language::setSource($post['langID']);
                    break;
                case 'removeLanguage': $result['result'] = Language::remove($post['langID']);
                    break;

                //Translations
                case 'refreshTranslations': $result['result'] = SourceMessage::refreshTranslations(); sleep(2);
                    break;
                case 'saveTranslation': $result['result'] = Message::updateAll(['translation' => $post['value']], [
                    'id' => $post['messageID'], 'language' => $post['langCode']
                ]);
                    break;

                //Contacts
                case 'fetchContactList': $result = ['result' => true, 'html' => $this->renderAjax('contact-list')];
                    break;
                case 'addContact': $result = Contact::add($post['typeID']);
                    break;
                case 'selectPrimaryContact': $result['result'] = Contact::selectPrimary($post['contactID']);
                    break;
                case 'changeSelectedContact': $result['result'] = Contact::updateAll(['selected' => $post['state']], ['id' => $post['contactID']]);
                    break;
                case 'raiseContactPriority': $result['result'] = Contact::raisePriority($post['contactID']);
                    break;
                case 'lowerContactPriority': $result['result'] = Contact::lowerPriority($post['contactID']);
                    break;
                case 'removeContact': $result['result'] = Contact::remove($post['contactID']);
                    break;
                case 'saveMapMarker': $result['result'] = Content::saveMapMarker($post['lat'], $post['lng'], $post['zoom']);
                    break;

                //Users
                case 'fetchUsersList': $result = ['result' => true, 'html' => $this->renderAjax('users-list')];
                    break;
                case 'fetchUserForm': $result = [
                    'result' => true, 'html' => $this->renderAjax('user-form', ['user' => User::findOne($post['userID'])])
                ];
                    break;
                case 'sendUserInvite': $result['result'] = User::sendInvite($post['email']);
                    break;
                case 'removeUser': $result['result'] = User::remove($post['userID']);
                    break;
                case 'setUserActive': $result['result'] = User::updateAll([
                    'status' => User::STATUS_ACTIVE
                ], ['id' => $post['userID']]);
                    break;
                case 'setUserInactive': $result['result'] = User::updateAll([
                    'status' => User::STATUS_INACTIVE
                ], ['id' => $post['userID']]);
                    break;
                case 'changeUserPassword': $result = User::changePassword($post['userID'], $post['password'], $post['confirm']);
                    break;

                //Leads
                case 'fetchLeadsList': $result = [
                    'result' => true, 'html' => $this->renderAjax('leads-list', Form::search($post['searchQuery']))
                ];
                    break;
                case 'fetchLeadForm': $result = [
                    'result' => true, 'html' => $this->renderAjax('lead-form', [
                        'lead' => $lead = Form::findOne($post['leadID']), 'content' => json_decode($lead->content),
//                        'parsedUA' => UserAgentParser::parse($lead->ua)
                    ])
                ];
                    break;
                case 'setLeadViewed': $result['result'] = Form::updateAll([
                    'status' => Form::STATUS_VIEW
                ], ['id' => $post['leadID']]);
                    break;
                case 'sendLeadNotify': $result['result'] = Form::sendNotify($post['leadID']);
                    break;
                case 'removeLead': $result['result'] = Form::deleteAll(['id' => $post['leadID']]);
                    break;
                case 'fetchLeadGeo': $result['result'] = Form::fetchGEO($post['leadID']);
                    break;

                //Product
                case 'fetchProductList': $result = [
                    'result' => true, 'html' => $this->renderAjax('product-list', [
                        'products' => Product::find()->orderBy(['priority' => SORT_ASC])->asArray()->all()
                    ])
                ];
                    break;
                case 'fetchProductForm': $result = [
                    'result' => true, 'html' => $this->renderAjax('product-form', [
                        'product' => Product::findOne($post['productID'])
                    ])
                ];
                    break;
                case 'createProduct': $result = Product::create();
                    break;
                case 'saveProduct': $result = Product::saveProduct($post['productID'], $post['data'], $post['content']);
                    break;
                case 'raiseProductPriority': $result['result'] = Product::raisePriority($post['productID']);
                    break;
                case 'lowerProductPriority': $result['result'] = Product::lowerPriority($post['productID']);
                    break;
                case 'setProductStatusDraft': $result['result'] = Product::updateAll([
                    'status' => Product::STATUS_DRAFT
                ], ['id' => $post['productID']]);
                    break;
                case 'setProductStatusActive': $result['result'] = Product::updateAll([
                    'status' => Product::STATUS_ACTIVE
                ], ['id' => $post['productID']]);
                    break;
                case 'removeProduct': $result['result'] = Product::remove($post['productID']);
                    break;

//                //Sewing
//                case 'fetchItemList': $result = [
//                    'result' => true, 'html' => $post['model'] == 'Repair' ?
//                        $this->renderAjax('repairs-list', ['repairs' => Repair::find()->orderBy(['priority' => SORT_ASC])->asArray()->all()]) :
//                        $this->renderAjax('clothes-list', ['clothes' => Cloth::find()->orderBy(['priority' => SORT_ASC])->asArray()->all()])
//                ];
//                    break;
//                case 'fetchItemForm': $result = [
//                    'result' => true, 'html' => $post['model'] == 'Repair' ?
//                        $this->renderAjax('repair-form', ['repair' => Repair::findOne($post['ID'])]) :
//                        $this->renderAjax('cloth-form', ['cloth' => Cloth::findOne($post['ID'])])
//                ];
//                    break;
//                case 'createItem': $result = $post['model'] == 'Repair' ? Repair::create() : Cloth::create();
//                    break;
//                case 'removeItem': $result['result'] = $post['model'] == 'Repair' ?
//                    Repair::remove($post['ID']) : Cloth::remove($post['ID']);
//                    break;
//                case 'raiseItemPriority': $result['result'] = $post['model'] == 'Repair' ?
//                    Repair::raisePriority($post['ID']) : Cloth::raisePriority($post['ID']);
//                    break;
//                case 'lowerItemPriority': $result['result'] = $post['model'] == 'Repair' ?
//                    Repair::lowerPriority($post['ID']) : Cloth::lowerPriority($post['ID']);
//                    break;
//                case 'changeItemStatus': $result['result'] = $post['model'] == 'Repair' ?
//                    Repair::updateAll(['status' => $post['status']], ['id' => $post['ID']]) :
//                    Cloth::updateAll(['status' => $post['status']], ['id' => $post['ID']]);
//                    break;
//                case 'getServiceClothes': $result = [
//                    'result' => true, 'html' => $this->renderAjax('service-clothes', ['repairID' => $post['repairID']])
//                ];
//                    break;
//                case 'getServiceRepairs': $result = [
//                    'result' => true, 'html' => $this->renderAjax('service-repairs', ['clothID' => $post['clothID']])
//                ];
//                    break;
//                case 'addService': $result['result'] = Service::addService($post['repairID'], $post['clothID']);
//                    break;
//                case 'removeService': $result['result'] = Service::deleteAll(['repair_id' => $post['repairID'], 'cloth_id' => $post['clothID']]);
//                    break;


                default: $result['error'][] = 'Unknown method!'; break;
            }
        }
        return $result;
    }
}