<?php namespace app\modules\media;

use yii\web\AssetBundle;

class CropperAsset extends AssetBundle {

    public $sourcePath = '@app/modules/media/assets';
    public $css = ['cropper.min.css'];
    public $js = ['cropper.min.js', 'jquery-cropper.min.js'];
    public $depends = ['yii\web\JqueryAsset', 'yii\bootstrap\BootstrapPluginAsset'];
}
