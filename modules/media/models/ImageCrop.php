<?php namespace app\modules\media\models;

use app\modules\media\Media;
use Imagine\Image\ImageInterface;
use Imagine\Imagick\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "image_crop".
 *
 * @property int $id
 * @property int $image_id
 * @property int $option_id
 * @property string $filename
 * @property int $x
 * @property int $y
 * @property int $width
 * @property int $height
 */
class ImageCrop extends ActiveRecord {

    public static function tableName() { return 'image_crop'; }

    public function rules() {
        return [
            [['image_id', 'option_id', 'x', 'y', 'width', 'height'], 'required'],
            [['image_id', 'option_id', 'x', 'y', 'width', 'height'], 'integer'],
            [['filename'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'image_id' => 'Image ID',
            'option_id' => 'Option ID',
            'filename' => 'Filename',
            'x' => 'X',
            'y' => 'Y',
            'width' => 'Width',
            'height' => 'Height',
        ];
    }

    const FREE_RATIO = 0;
    const RATIO_PRECISION = 3;

    public static function getSrcByRatio($imageID, $ratio, $width = Image::MAX_WIDTH) {
        if ($image = Image::findOne($imageID)) {
            $typeOption = ImageTypeOption::find()->select(['image_crop.*'])->leftJoin(
                'image_crop', 'image_crop.option_id = image_type_option.id'
            )->where([
                'AND',
                ['=', 'image_id', $image->id],
                ['=', 'type_id', $image->type],
                ['=', 'ratio', $ratio]
            ]);
            switch ($width) {
                case Image::MAX_WIDTH:
                    $typeOption->orderBy(['image_type_option.width' => SORT_DESC]);
                    break;
                case Image::MIN_WIDTH:
                default:
                    $typeOption->andWhere(['>', 'image_type_option.width', ImageTypeOption::FREE_WIDTH])
                        ->orderBy(['image_type_option.width' => SORT_ASC]);
                    break;
            }
            return ($link = $typeOption->limit(1)->asArray()->one()) ? $link : false;
        } else { return false; }
    }

    public static function crop($imageID, $ratio, $cropData) {
        if ($image = Image::findOne($imageID)) {
            $imageType = ImageType::findOne($image->type);
            $imageTypeOptions = ImageTypeOption::find()->where([
                'type_id' => $image->type, 'ratio' => $ratio
            ])->asArray()->all();
            $sourceExtension = strtolower(pathinfo(Image::WEB_ROOT.$image->filename, PATHINFO_EXTENSION));
            foreach ($imageTypeOptions as $imageTypeOption) {
                foreach (self::find()->select(['id'])->where([
                    'image_id' => $imageID, 'option_id' => $imageTypeOption['id']
                ])->column() as $imageCropID) { self::removeOne($imageCropID); }
                $imagine = new Imagine();
                $sourceImage = $imagine->open(Image::WEB_ROOT.$image->filename);
                $croppedImage = $sourceImage->crop(
                    new Point($cropData['x'], $cropData['y']), new Box($cropData['width'], $cropData['height'])
                );
                $newCrop = new self;
                $newCrop->image_id = $imageID;
                $newCrop->option_id = $imageTypeOption['id'];
                $fileName = [];
                foreach (json_decode($imageType->template) as $part) {
                    switch ($part) {
                        case '$objectID': if ($imageType->multiple != ImageType::IS_MULTIPLE) {
                            $fileName[] = $image->object;
                        }
                            break;
                        case '$width': if ($imageTypeOption['width'] > 0) { $fileName[] = 'w'.$imageTypeOption['width']; }
                            break;
                        case '$height': if ($imageTypeOption['height'] > 0) { $fileName[] = 'h'.$imageTypeOption['height']; }
                            break;
                        case '$time': if ($imageType->multiple == ImageType::IS_MULTIPLE) {
                            $fileName[] = round(microtime(true) * 1000);
                        }
                            break;
                        default: $fileName[] = $part; break;
                    }
                }
                $fileName[] = $imageTypeOption['id'];
                foreach ($cropData as $key => $value) { $newCrop->$key = $value; }
                switch ($sourceExtension) {
                    case 'gif': $options = ['animated' => true]; break;
                        break;
                    case 'png': $options = ['png_compression' => Image::$pngCompression]; break;
                        break;
                    case 'jpg':
                    case 'jpeg': $options = ['jpeg_quality' => Image::$jpegQuality];
                        break;
                    default: $options = []; break;
                }
                $newCrop->filename = implode(DIRECTORY_SEPARATOR, [
                    '', Image::MEDIA_FOLDER, $imageType->folder, implode('-', $fileName).'.'.$sourceExtension
                ]);
                $ratio = $ratio <> self::FREE_RATIO ? $ratio : round($cropData['width'] / $cropData['height'], self::RATIO_PRECISION);
                if ($imageTypeOption['width'] > 0 && $imageTypeOption['height'] == 0) {
                    $croppedImage->resize(new Box($imageTypeOption['width'], round($imageTypeOption['width'] / $ratio)), ImageInterface::FILTER_LANCZOS);
                } elseif ($imageTypeOption['width'] == 0 && $imageTypeOption['height'] > 0) {
                    $croppedImage->resize(new Box($imageTypeOption['height'] * $ratio, $imageTypeOption['height']), ImageInterface::FILTER_LANCZOS);
                } elseif ($imageTypeOption['width'] > 0 && $imageTypeOption['height'] > 0) {
                    $croppedImage->resize(new Box($imageTypeOption['width'], $imageTypeOption['height']), ImageInterface::FILTER_LANCZOS);
                } elseif ($imageTypeOption['width'] == 0 && $imageTypeOption['height'] == 0) {
                    $croppedImage->resize(new Box($cropData['width'], $cropData['height']), ImageInterface::FILTER_LANCZOS);
                }
                $croppedImage->strip()->save($filePath = implode(DIRECTORY_SEPARATOR, [
                    Image::MEDIA_PATH, $imageType->folder, implode('-', $fileName).'.'.$sourceExtension
                ]), $options);
                if ($newCrop->save()) { Image::optimizeImage($filePath); }
            }
            return true;
        }
        return false;
    }

    public static function resize($imageID, $width, $height) {

    }

    public static function removeAll($imageID) {
        if ($image = Image::findOne($imageID)) {
            foreach (self::find()->select(['id'])->where(['image_id' => $imageID])->column() as $croppedImageID) {
                self::removeOne($croppedImageID);
            }
        }
    }

    public static function removeOne($imageCropID) {
        if ($croppedImage = self::findOne($imageCropID)) {
            @unlink(Image::WEB_ROOT.$croppedImage->filename);
            return $croppedImage->delete();
        } else { return false; }
    }

}
