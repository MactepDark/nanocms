<?php

namespace app\modules\media\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "video_type".
 *
 * @property int $id
 * @property string $name
 * @property string $const
 * @property string $folder
 * @property string $template
 */
class VideoType extends ActiveRecord {

    public static function tableName() { return 'video_type'; }

    public function rules() {
        return [
            [['name', 'const', 'folder'], 'required'],
            [['name', 'const', 'folder', 'template'], 'string', 'max' => 255],
            [['const'], 'unique'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'const' => 'Const',
            'folder' => 'Folder',
            'template' => 'Template',
        ];
    }



}
