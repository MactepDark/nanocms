<?php

namespace app\modules\media\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "video".
 *
 * @property int $id
 * @property int $type
 * @property int $object
 * @property string $poster
 */
class Video extends ActiveRecord {

    public static function tableName() { return 'video'; }

    public function rules() {
        return [
            [['type', 'object'], 'required'],
            [['type', 'object'], 'integer'],
            ['poster', 'string']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'object' => 'Object',
            'poster' => 'Poster'
        ];
    }

    const NO_OBJ = 0;
    const MEDIA_FOLDER = 'media';
    const WEB_ROOT = PROJECT_ROOT.DIRECTORY_SEPARATOR.'web';
    const MEDIA_PATH = self::WEB_ROOT.DIRECTORY_SEPARATOR.self::MEDIA_FOLDER;
    const GENERATED_POSTER_COUNT = 24;
    const CPU_THREADS = 2;
    const Q_SCALE = 2;

    public static function tagVideo($typeID, $objectID = self::NO_OBJ) {
        return Yii::$app->view->render('video/video-tag', [
            'video' => $video = self::findOne(['type' => $typeID, 'object' => $objectID]),
            'sources' => VideoSources::find()->where(['video_id' => $video->id])->asArray()->all()
        ]);
    }

    public static function videoUpload($typeID, $objectID, $videoFile) {
        if (is_file($videoFile['tmp_name'])) {
            if (!$video = self::findOne(['type' => $typeID, 'object' => $objectID])) {
                $video = new self;
                $video->type = $typeID;
                $video->object = $objectID;
                $video->save();
            }
            return VideoSources::uploadSource($video->id, $videoFile);
        } else { return false; }
    }

    public static function remove($videoID) {
        if ($video = self::findOne($videoID)) {
            if (is_file(self::WEB_ROOT.$video->poster)) { unlink(self::WEB_ROOT.$video->poster); }
            VideoSources::removeSources($videoID);
            $video->delete();
            return true;
        } else { return false; }
    }

    public static function generatePosters($videoID) {
        $posters = [];
        if ($video = self::findOne($videoID)) {
            if ($source = VideoSources::find()->where(['video_id' => $videoID])->orderBy(['size' => SORT_DESC])->limit(1)->asArray()->one()) {
                $commandLine = ['ffmpeg', '-i', self::WEB_ROOT.$source['filename']];
                $commandLine[] = '-qscale:v '.self::Q_SCALE;
                $commandLine[] = '-ss 0';
                $commandLine[] = '-frames:v '.self::GENERATED_POSTER_COUNT;
                $commandLine[] = '-threads '.self::CPU_THREADS;
                $commandLine[] = self::MEDIA_PATH.DIRECTORY_SEPARATOR.implode('-', [$videoID, 'poster', '%d.jpg']);
                exec(implode(' ', $commandLine));
                for ($i = 1; $i <= self::GENERATED_POSTER_COUNT; $i++) {
                    $posters[$i] = implode(DIRECTORY_SEPARATOR, [
                        '', self::MEDIA_FOLDER, implode('-', [$videoID, 'poster', $i]).'.jpg'
                    ]);
                }
            }
        }
        return $posters;
    }

    public static function selectPoster($videoID, $posterID) {
        if ($video = self::findOne($videoID)) {
            if (is_file($filePath = implode(DIRECTORY_SEPARATOR, [self::MEDIA_PATH, implode('-', [$videoID, 'poster', $posterID.'.jpg'])]))) {
                $videoType = VideoType::findOne($video->type);
                $video->poster = implode(DIRECTORY_SEPARATOR, ['', self::MEDIA_FOLDER, $videoType->folder, implode('-', ['poster', $videoID, 'video.jpg'])]);
                file_put_contents(self::WEB_ROOT.$video->poster, file_get_contents($filePath));
                for ($i = 1; $i <= self::GENERATED_POSTER_COUNT; $i++) {
                    unlink(implode(DIRECTORY_SEPARATOR, [self::MEDIA_PATH, implode('-', [$videoID, 'poster', $i.'.jpg'])]));
                }
                Image::optimizeImage(self::WEB_ROOT.$video->poster);
                return $video->save();
            }
        }
        return false;
    }
}
