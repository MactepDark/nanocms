<?php

namespace app\modules\media\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "image_type".
 *
 * @property int $id
 * @property string $name
 * @property string $const
 * @property int $multiple
 * @property string $folder
 * @property string $template
 * @property string $alt
 * @property string $title
 */
class ImageType extends ActiveRecord {

    public static function tableName() { return 'image_type'; }

    public function rules() {
        return [
            [['name', 'const', 'multiple', 'folder', 'template'], 'required'],
            [['multiple'], 'integer'],
            [['name', 'const', 'folder', 'template', 'alt', 'title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => Yii::t('admin', 'Название типа изображения'),
            'const' => Yii::t('admin', 'Константа для изображения'),
            'multiple' => Yii::t('admin', 'Галерея'),
            'folder' => Yii::t('admin', 'Каталог хранения файлов'),
            'template' => Yii::t('admin', 'Шаблон имен файлов'),
            'alt' => Yii::t('admin', 'Шаблон альтов'),
            'title' => Yii::t('admin', 'Шаблон тайтлов')
        ];
    }
    const IS_NEW = 0;
    const IS_SINGLE = 0;
    const IS_MULTIPLE = 1;

    public static function saveImageType($imageTypeID, $data) {
        if (!$imageType = self::findOne($imageTypeID)) {
            $imageType = new self;
        }
        $imageType->multiple = self::IS_SINGLE;
        foreach ($data as $keyPair) {
            switch ($field = $keyPair['name']) {
                case 'multiple': $imageType->multiple = self::IS_MULTIPLE;
                    break;
                default: $imageType->$field = trim($keyPair['value']); break;
            }
        }
        return ['result' => $imageType->save(), 'error' => $imageType->errors, 'id' => $imageType->id];
    }
}
