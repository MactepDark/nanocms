<?php

namespace app\modules\media\models;

use app\modules\media\Media;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "video_sources".
 *
 * @property int $id
 * @property int $video_id
 * @property string $filename
 * @property string $format
 * @property int $size
 */
class VideoSources extends ActiveRecord {

    public static function tableName() { return 'video_sources'; }

    public function rules() {
        return [
            [['video_id', 'filename', 'format', 'size'], 'required'],
            [['video_id', 'size'], 'integer'],
            [['filename', 'format'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'video_id' => 'Video ID',
            'filename' => 'Filename',
            'format' => 'Format',
            'size' => 'Size',
        ];
    }

    public static function uploadSource($videoID, $videoFile) {
        if ($video = Video::findOne($videoID)) {
            if ($source = self::findOne(['video_id' => $videoID, 'format' => $videoFile['type']])) {
                @unlink(Video::WEB_ROOT.$source->filename);
            } else {
                $source = new self;
                $source->video_id = $videoID;
            }
            $source->format = $videoFile['type'];
            $source->size = $videoFile['size'];
            return self::moveUploadedFile($video, $source, $videoFile);
        } else { return false; }
    }

    public static function removeSources($videoID) {
        foreach (self::find()->select(['id'])->where(['video_id' => $videoID])->column() as $sourceID) {
            self::removeSource($sourceID);
        }
        return true;
    }

    public static function removeSource($sourceID) {
        if ($source = self::findOne($sourceID)) {
            if (self::find()->where(['video_id' => $source->video_id])->count() == 1) {
                if ($video = Video::findOne($source->video_id)) {
                    if (is_file(Video::WEB_ROOT.$video->poster)) { unlink(Video::WEB_ROOT.$video->poster); }
                    $video->delete();
                }
            }
            if (unlink(Video::WEB_ROOT.$source->filename)) {
                return $source->delete();
            }
        }
        return false;
    }

        /**
     * @param $video Video
     * @param $source self
     * @param $videoFile array
     * @return bool
     */
    private static function moveUploadedFile($video, $source, $videoFile) {
        if ($videoType = VideoType::findOne($video->type)) {
            $sourceExtension = strtolower(pathinfo($videoFile['name'], PATHINFO_EXTENSION));
            $destinationPath = implode(DIRECTORY_SEPARATOR, [Video::MEDIA_PATH, $videoType->folder]);
            if (!is_dir($destinationPath)) { mkdir($destinationPath, 0774, true); }
            $destinationFilename = [];
            foreach (json_decode($videoType->template) as $part) {
                switch ($part) {
                    case '$typeID': $destinationFilename[] = $video->type; break;
                    case '$objectID': $destinationFilename[] = $video->object; break;
                    case '$time': $destinationFilename[] = time(); break;
                    default: $destinationFilename[] = $part; break;
                }
            }
            if (move_uploaded_file($videoFile['tmp_name'], $destinationPath.DIRECTORY_SEPARATOR.implode('-', $destinationFilename).'.'.$sourceExtension)) {
                $source->filename = implode(DIRECTORY_SEPARATOR, ['', Video::MEDIA_FOLDER, $videoType->folder, implode('-', $destinationFilename).'.'.$sourceExtension]);
                return $source->save();
            }
        }
        return false;
    }

}
