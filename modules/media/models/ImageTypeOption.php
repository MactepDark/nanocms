<?php

namespace app\modules\media\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "image_type_option".
 *
 * @property int $id
 * @property int $type_id
 * @property string $name
 * @property string $media
 * @property string $ratio
 * @property int $width
 * @property int $height
 */
class ImageTypeOption extends ActiveRecord {

    public static function tableName() { return 'image_type_option'; }

    public function rules() {
        return [
            [['type_id', 'media', 'ratio'], 'required'],
            [['type_id', 'width', 'height'], 'integer'],
            [['ratio'], 'number'],
            [['name', 'media'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'name' => 'Name',
            'media' => 'Media',
            'ratio' => 'Ratio',
            'width' => 'Width',
            'height' => 'Height',
        ];
    }

    const FREE_WIDTH = 0;
    const FREE_HEIGHT = 0;

    public static function getTypeOptions($typeID) {
        return self::find()->select(['name', 'ratio'])->where([
            'type_id' => $typeID
        ])->groupBy(['ratio', 'name'])->asArray()->all();
    }

    public static function getOptions($imageTypeID) {
        $result = [];
        $options = self::find()->where(['type_id' => $imageTypeID])->asArray()->all();
        foreach ($options as $option) {
            $result[$option['ratio']][$option['id']] = $option;
        }
        return $result;
    }
}
