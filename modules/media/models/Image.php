<?php

namespace app\modules\media\models;

use Imagine\Imagick\Imagine;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property int $type
 * @property int $object
 * @property string $filename
 * @property int $priority
 * @property string $format
 * @property int $size
 * @property int $width
 * @property int $height
 */
class Image extends ActiveRecord {

    public static function tableName() { return 'image'; }

    public function rules() {
        return [
            [['type', 'object', 'filename', 'priority', 'format', 'size', 'width', 'height'], 'required'],
            [['type', 'object', 'priority', 'size', 'width', 'height'], 'integer'],
            [['filename', 'format'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'object' => 'Object',
            'filename' => 'Filename',
            'priority' => 'Priority',
            'format' => 'Format',
            'size' => 'Size',
            'width' => 'Width',
            'height' => 'Height',
        ];
    }

    public static $jpegQuality = 85;
    public static $pngCompression = 7;

    const MIN_WIDTH = -1;
    const MAX_WIDTH = -2;

    const NO_IMAGE = 0;
    const NO_PRIORITY = 0;
    const MEDIA_FOLDER = 'media';
    const WEB_ROOT = PROJECT_ROOT.DIRECTORY_SEPARATOR.'web';
    const MEDIA_PATH = self::WEB_ROOT.DIRECTORY_SEPARATOR.self::MEDIA_FOLDER;

    public static function imageUpload($typeID, $objectID, $imageID, $imageFile) {
        if (is_file($imageFile['tmp_name'])) {
            $imageType = ImageType::findOne($typeID);
            $imagine = new Imagine();
            $sourceImage = $imagine->open($imageFile['tmp_name']);
            if ($imageType->multiple === ImageType::IS_MULTIPLE) {
                if ($imageID === self::NO_IMAGE) {
                    $image = new self;
                    $image->type = $typeID;
                    $image->object = $objectID;
                    $image->priority = self::find()->select(['MAX(priority)'])->where([
                        'type' => $typeID, 'object' => $objectID
                    ])->scalar() + 1;
                    $image->filename = 'temp';
                } else {
                    ImageCrop::removeAll($imageID);
                    $image = self::findOne($imageID);
                }
            } else {
                self::removeImages($typeID, $objectID);
                $image = new self;
                $image->type = $typeID;
                $image->object = $objectID;
                $image->priority = self::NO_PRIORITY;
                $image->filename = 'temp';
            }
            $image->format = $sourceImage->getImagick()->getImageMimeType();
            $image->size = filesize($imageFile['tmp_name']);
            $image->width = $sourceImage->getSize()->getWidth();
            $image->height = $sourceImage->getSize()->getHeight();
            if ($image->save()) {
                if (self::moveUploadedFile($image->id, $imageFile)) {
                    self::normalizeSource($image->id);
                    return true;
                }
            }
        }
        return false;
    }

    public static function raisePriority($imageID) {
        if ($image = self::findOne($imageID)) {
            if ($swapImage = self::findOne([
                'type' => $image->type, 'object' => $image->object, 'priority' => $image->priority - 1
            ])) {
                $image->priority--; $swapImage->priority++;
                return $image->save() && $swapImage->save();
            }
        }
        return false;
    }

    public static function lowerPriority($imageID) {
        if ($image = self::findOne($imageID)) {
            if ($swapImage = self::findOne([
                'type' => $image->type, 'object' => $image->object, 'priority' => $image->priority + 1
            ])) {
                $image->priority++; $swapImage->priority--;
                return $image->save() && $swapImage->save();
            }
        }
        return false;
    }

    public static function removeImages($typeID, $objectID) {
        foreach (self::find()->select(['id'])->where([
            'type' => $typeID, 'object' => $objectID
        ])->column() as $imageID) { self::removeImage($imageID); }
        return true;
    }

    public static function removeImage($imageID) {
        if ($image = self::findOne($imageID)) {
            $imageType = ImageType::findOne($image->type);
            if ($imageType->multiple == ImageType::IS_MULTIPLE) {
                foreach (self::find()->where(['AND',
                    ['=', 'type', $image->type],
                    ['=', 'object', $image->object],
                    ['>', 'priority', $image->priority]
                ])->asArray()->all() as $shiftedImage) {
                    self::updateAll(['priority' => $shiftedImage['priority'] - 1], ['id' => $shiftedImage['id']]);
                }
            }
            ImageCrop::removeAll($imageID);
            @unlink(self::WEB_ROOT.$image->filename);
            return $image->delete();
        }
        return true;
    }

    public static function getImage($typeID, $objectID) {
        return self::findOne(['type' => $typeID, 'object' => $objectID]);
    }

    public static function getSrc($imageID = false, $typeID = false, $objectID = false, $width = self::MAX_WIDTH) {
        if ($imageID !== false) {
            $original = self::findOne($imageID);
        } else {
            $original = self::getImage($typeID, $objectID);
        }
        $croppedImage = ImageCrop::find()->select(['filename'])->where(['image_id' => $original->id]);
        switch ($width) {
            case self::MIN_WIDTH: $croppedImage->orderBy(['width' => SORT_ASC]);
                break;
            case self::MAX_WIDTH: $croppedImage->orderBy(['width' => SORT_DESC]);
                break;
            default: $croppedImage->andWhere(['width' => $width]);
                break;
        }
        if ($croppedImage->count()) {
            return $croppedImage->limit(1)->scalar();
        } else {
            return $original->filename;
        }
    }

    public static function optimizeImage($filePath) {
        if (is_file($filePath)) {
            switch (pathinfo($filePath, PATHINFO_EXTENSION)) {
                case 'jpeg':
                case 'jpg': exec('jpegoptim '.$filePath.' -m'.self::$jpegQuality.' -f --strip-all'); break;
                case 'png': exec('optipng '.$filePath.' -o'.self::$pngCompression); break;
                default: break;
            }
            return true;
        } else { return false; }
    }

    private static function normalizeSource($sourceImageID) {
        if ($image = self::findOne($sourceImageID)) {
            $imagick = new Imagine();
            $sourceImage = $imagick->open(self::WEB_ROOT.$image->filename);
            $exif = exif_read_data(self::WEB_ROOT.$image->filename);
            if (isset($exif['Orientation'])) {
                switch ($exif['Orientation']) {
                    case 3: $sourceImage->rotate(180);
                        break;
                    case 6: $sourceImage->rotate(90);
                        break;
                    case 8: $sourceImage->rotate(-90);
                        break;
                    default: break;
                }
                return $sourceImage->save();
            }
        }
        return false;
    }

    private static function moveUploadedFile($imageID, $file) {
        if ($image = self::findOne($imageID)) {
            $imageType = ImageType::findOne($image->type);
            $sourceExtension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
            $destinationPath = implode(DIRECTORY_SEPARATOR, [self::MEDIA_PATH, $imageType->folder]);
            if (!is_dir($destinationPath)) { mkdir($destinationPath, 0774, true); }
            $destinationFilename = [];
            foreach (json_decode($imageType->template) as $part) {
                switch ($part) {
                    case '$objectID': if ($imageType->multiple != ImageType::IS_MULTIPLE) {
                        $destinationFilename[] = $image->object;
                    }
                        break;
                    case '$width': break;
                        break;
                    case '$height': break;
                        break;
                    case '$time': if ($imageType->multiple == ImageType::IS_MULTIPLE) {
                        $destinationFilename[] = round(microtime(true) * 10000);
                    }
                        break;
                    default: $destinationFilename[] = $part; break;
                }
            }
            if (move_uploaded_file($file['tmp_name'], $destinationPath.DIRECTORY_SEPARATOR.implode('-', $destinationFilename).'.'.$sourceExtension)) {
                $image->filename = implode(DIRECTORY_SEPARATOR, ['', self::MEDIA_FOLDER, $imageType->folder, implode('-', $destinationFilename).'.'.$sourceExtension]);
                return $image->save();
            }
        }
        return false;
    }
}