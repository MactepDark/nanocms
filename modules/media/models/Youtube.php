<?php

namespace app\modules\media\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "youtube".
 *
 * @property int $id
 * @property int $type
 * @property int $object
 * @property string $name
 * @property string $hash
 */
class Youtube extends ActiveRecord {

    public static function tableName() { return 'youtube'; }

    public function rules() {
        return [
            [['type', 'object', 'hash'], 'required'],
            [['type', 'object'], 'integer'],
            [['name', 'hash'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'object' => 'Object',
            'name' => 'Name',
            'hash' => 'Hash',
        ];
    }


}
