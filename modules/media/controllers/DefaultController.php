<?php namespace app\modules\media\controllers;

use app\modules\media\Media;
use app\modules\media\MediaOptionsAsset;
use app\modules\media\models\Image;
use app\modules\media\models\ImageCrop;
use app\modules\media\models\ImageType;
use app\modules\media\models\ImageTypeOption;
use app\modules\media\models\Video;
use app\modules\media\models\VideoSources;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\View;

class DefaultController extends Controller {

    public $layout = '/admin-main';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [['actions' => ['index', 'options', 'ajax'], 'allow' => true, 'roles' => ['@']]]
            ]
        ];
    }

    public function actionIndex() {
        return $this->redirect(['options']);
    }

    public function actionOptions() {
        $this->view->title = 'Media options';
        MediaOptionsAsset::register($this->view);
        return $this->render('options/index');
    }

    public function actionAjax() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = [
            'result' => false, 'error' => [],
            'post' => $post = Yii::$app->request->post(), 'files' => $files = count($_FILES) ? $_FILES : []
        ];
        if (isset($post['method'])) {
            switch ($post['method']) {
                case 'fetchImageField': $result = [
                    'result' => true, 'html' => Media::imageField($post['typeID'], $post['objectID'])
                ];
                    break;
                case 'fetchImageCropView': $result = [
                    'result' => true, 'html' => $this->renderAjax('image/crop-view', [
                        'image' => $image = Image::findOne($post['imageID']),
                        'typeOptions' => ImageTypeOption::getTypeOptions($image->type)
                    ])
                ];
                    break;
                case 'fetchGalleryField': $result = [
                    'result' => true, 'html' => Media::galleryField($post['typeID'], $post['objectID'])
                ];
                    break;
                case 'fetchVideoField': $result = [
                    'result' => true, 'html' => Media::videoField($post['typeID'], $post['objectID'])
                ];
                    break;
                case 'fetchYoutubeField': $result = [
                    'result' => true, 'html' => Media::youtubeField($post['typeID'], $post['objectID'])
                ];
                    break;
                case 'fetchVideoPosters': $result = [
                    'result' => true, 'html' => $this->renderAjax('video/posters', ['posters' => Video::generatePosters($post['videoID'])])
                ];
                    break;
                case 'imageUpload': $result['result'] = Image::imageUpload($post['typeID'], $post['objectID'], $post['imageID'], $files['image']);
                    break;
                case 'galleryUpload': $result['result'] = Image::imageUpload($post['typeID'], $post['objectID'], Image::NO_IMAGE, $files['image']);
                    break;
                case 'videoUpload': $result['result'] = Video::videoUpload($post['typeID'], $post['objectID'], $files['video']);
                    break;
                case 'videoRemove': $result['result'] = Video::remove($post['videoID']);
                    break;
                case 'videoSourceRemove': $result['result'] = VideoSources::removeSource($post['sourceID']);
                    break;
                case 'videoSelectPoster': $result['result'] = Video::selectPoster($post['videoID'], $post['posterID']);
                    break;
                case 'imageCrop': $result['result'] = ImageCrop::crop($post['imageID'], $post['ratio'], $post['data']);
                    break;
                case 'imageRemove': $result['result'] = Image::removeImage($post['imageID']);
                    break;
                case 'galleryImageRemove': $result['result'] = Image::removeImage($post['imageID']);
                    break;
                case 'galleryRaiseImagePriority': $result['result'] = Image::raisePriority($post['imageID']);
                    break;
                case 'galleryLowerImagePriority': $result['result'] = Image::lowerPriority($post['imageID']);
                    break;



                //Options
                case 'fetchImageTypes': $result = ['result' => true, 'html' => $this->renderAjax('options/image-types')];
                    break;
                case 'fetchImageTypeForm': $result = [
                    'result' => true, 'html' => $this->renderAjax('options/image-type-form', [
                        'imageType' => $post['imageTypeID'] > ImageType::IS_NEW ? ImageType::findOne($post['imageTypeID']) : new ImageType()
                    ])
                ];
                    break;
                case 'fetchImageTypeOptions': $result = [
                    'result' => true, 'html' => $this->renderAjax('options/type-options', [
                        'imageType' => ImageType::findOne($post['imageTypeID']),
                        'typeOptions' => ImageTypeOption::getOptions($post['imageTypeID'])
                    ])
                ];
                    break;
                case 'refreshTypes': $result = ['count' => $count = Media::refreshTypeConfig(), 'result' => $count > 0];
                    break;
                case 'saveImageType': $result = ImageType::saveImageType($post['imageTypeID'], $post['data']);
                    break;


                default: $result['error'][] = 'Unknown method'; break;
            }
        }

        return $result;
    }
}
