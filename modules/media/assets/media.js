let Media = {
    $mediaField: undefined, $cropView: undefined, imageID: undefined,
    onload: document.addEventListener('DOMContentLoaded', function() { Media.init(); }),
    init: function() {
        this.bind();
    },
    bind: function() {
        $(document).on('click', 'div.media-field button.image-crop', function(e) {
            e.preventDefault();
            Media.$mediaField = $(this).prop('disabled', true).closest('div.media-field');
            if (Media.$mediaField.hasClass('image-field')) {
                Media.imageID = Media.$mediaField.data('id');
            }
            Media.ajax.fetchImageCropView();
        }).on('click', 'div.crop-view button.crop-close', function(e) {
            e.preventDefault();
            if (Media.crop.state) { Media.crop.destroy(); }
            Media.$cropView.slideUp('fast', function() {
                $(this).remove();
                Media.$cropView = undefined;
                Media.$mediaField.hasClass('image-field') ? Media.ajax.fetchImageField() : null;
                Media.$mediaField.hasClass('gallery-field') ? Media.ajax.fetchGalleryField() : null;
            });
        }).on('click', 'div.media-field button.image-upload', function(e) {
            e.preventDefault();
            Media.$mediaField = $(this).closest('div.media-field');
            let $input = $('<input type="file" accept="image/*">').on('change', function() {
                Media.ajax.imageUpload(this.files[0]);
                $input.remove();
            }).hide();
            $(document.body).append($input);
            $input.click();
        }).on('click', 'div.media-field button.image-remove', function(e) {
            e.preventDefault();
            if (confirm('A you sure?')) {
                Media.$mediaField = $(this).prop('disabled', true).closest('div.media-field');
                Media.ajax.imageRemove();
            }
        }).on('click', 'div.crop-view fieldset[data-ratio]:not(.selected)', function() {
            if (Media.crop.state) { Media.crop.destroy(); }
            Media.crop.$fieldSet = $(this).addClass('selected');
            Media.crop.init();
        }).on('click', 'div.crop-view button.crop-image', function(e) {
            e.preventDefault();
            Media.$cropView.find('button').prop('disabled', true);
            Media.crop.$instance.disable();
            Media.ajax.imageCrop();
        }).on('click', 'div.media-field button.gallery-upload', function(e) {
            e.preventDefault();
            Media.$mediaField = $(this).closest('div.media-field');
            Media.multipleUpload.$progress = Media.$mediaField.find('progress').slideDown('fast');
            let $input = $('<input type="file" accept="image/*" multiple>').on('change', function() {
                $.each(this.files, function(k, v) { Media.multipleUpload.files.push(v); });
                Media.multipleUpload.$progress.prop('max', this.files.length);
                Media.multipleUpload.upload();
                $input.remove();
            }).hide();
            $(document.body).append($input);
            $input.click();
        }).on('click', 'div.media-field div.gallery-image', function() {
            Media.$mediaField = $(this).closest('div.media-field');
            $(this).hasClass('selected') ? Media.gallery.hideControls($(this)) : Media.gallery.showControls($(this));
        }).on('click', 'div.media-field button.gallery-raise-priority', function(e) {
            e.preventDefault();
            Media.ajax.galleryRaiseImagePriority($(this).prop('disabled', true));
        }).on('click', 'div.media-field button.gallery-lower-priority', function(e) {
            e.preventDefault();
            Media.ajax.galleryLowerImagePriority($(this).prop('disabled', true));
        }).on('click', 'div.media-field button.gallery-image-remove', function(e) {
            e.preventDefault();
            if (confirm('A you sure?')) {
                $(this).prop('disabled', true);
                Media.ajax.galleryImageRemove();
            }
        }).on('click', 'div.media-field button.video-upload', function(e) {
            e.preventDefault();
            Media.$mediaField = $(this).closest('div.media-field');
            let $input = $('<input type="file" accept="video/mp4, video/webm">').on('change', function() {
                Media.$mediaField.find('progress').fadeIn('fast', function() {
                    Media.$mediaField.find('button').prop('disabled', true);
                    $input.remove();
                });
                Media.ajax.videoUpload(this.files[0]);
            }).hide();
            $(document.body).append($input);
            $input.click();
        }).on('click', 'div.media-field button.video-remove', function(e) {
            e.preventDefault();
            if (confirm('A you sure?')) {
                Media.$mediaField = $(this).prop('disabled', true).closest('div.media-field');
                Media.ajax.videoRemove();
            }
        }).on('click', 'div.media-field button.source-remove', function(e) {
            e.preventDefault();
            if (confirm('A you sure?')) {
                Media.$mediaField = $(this).prop('disabled', true).closest('div.media-field');
                Media.ajax.videoSourceRemove($(this).closest('span[data-id]').data('id'));
            }
        }).on('click', 'div.media-field button.video-poster', function(e) {
            e.preventDefault();
            Media.$mediaField = $(this).prop('disabled', true).closest('div.media-field');
            Media.poster.show();
        });
    },
    gallery: {
        showControls: function($galleryImage) {
            Media.$mediaField.find('div.gallery-image.selected').removeClass('selected');
            Media.$mediaField.find('div.controls').slideDown('fast', function() {
                Media.imageID = $galleryImage.addClass('selected').data('id');
            });
        },
        hideControls: function($galleryImage) {
            Media.$mediaField.find('div.controls').slideUp('fast', function() {
                $galleryImage.removeClass('selected');
                Media.imageID = undefined;
            });
        }
    },
    multipleUpload: {
        files: [], $progress: undefined,
        upload: function() {
            if (this.files.length > 0) {
                this.$progress.val(this.$progress.val() + 1);
                Media.ajax.galleryUpload(this.files.shift());
            } else {
                this.$progress.fadeOut('fast', function() {
                    Media.ajax.fetchGalleryField(function() {
                        toastr.success('Images uploaded');
                    });
                });
            }
        }
    },
    crop: {
        state: false, sourceImage: undefined, $instance: undefined, $fieldSet: undefined,
        $controls: undefined, data: {x: 0, y: 0, width: 0, height: 0},
        options: {
            aspectRatio: NaN, zoomOnTouch: false, zoomable: false, autoCropArea: 1,
            crop: function(e) { Media.crop.infoView(e.detail); },
            ready: function() { Media.crop.$instance.setData(Media.crop.$fieldSet.find('img.preview').data()); }
        },
        init: function() {
            this.sourceImage = Media.$cropView.find('img.original-image').get(0);
            this.$controls = Media.$cropView.find('div.controls');
            this.$controls.find('button.crop-image').prop('disabled', false);
            this.options.aspectRatio = parseFloat(this.$fieldSet.data('ratio'));
            this.$instance = new Cropper(this.sourceImage, this.options);
            this.state = true;
        },
        destroy: function(callback) {
            this.$instance.destroy();
            Media.$mediaField.find('button.image-upload').prop('disabled', false);
            Media.$mediaField.find('button.image-remove').prop('disabled', false);
            this.$controls.find('button.crop-image').prop('disabled', true);
            this.$controls.find('button.crop-close').prop('disabled', false);
            this.$fieldSet = undefined;
            this.state = false;
            typeof callback === 'function' ? callback() : null;
        },
        infoView: function(detail) {
            ['x', 'y', 'width', 'height'].forEach(function(prop) {
                Media.crop.$controls.find('span.' + prop + ' > span.value').html(Media.crop.data[prop] = Math.round(detail[prop]));
            });
        }
    },
    poster: {
        $window: undefined,
        show: function () {
            Media.ajax.fetchVideoPosters(function() {
                Media.poster.$window.fadeIn('fast', function() {
                    Media.poster.$window.on('click', 'img[data-id]', function() {
                        Media.ajax.videoSelectPoster($(this).data('id'));
                    });
                });
            });
        },
        close: function() {
            this.$window.fadeOut('fast', function() { $(this).remove(); Media.ajax.fetchVideoField(); });
        }
    },
    ajax: {
        url: undefined, obj: {}, send: function(success, failure) {
            $.post(this.url, this.obj, function(answer) {
                Media.ajax.obj = {};
                answer.result && typeof success === 'function' ? success(answer) :
                    !answer.result && typeof failure === 'function' ? failure(answer) : null;
            }, 'JSON');
        },
        fetchImageField: function(callback) {
            this.obj.method = 'fetchImageField';
            this.obj.typeID = Media.$mediaField.data('type');
            this.obj.objectID = Media.$mediaField.data('object');
            this.send(function(answer) {
                Media.$mediaField.parent().html(answer.html).slideDown('fast', function() {
                    typeof callback === 'function' ? callback(answer) : null;
                });
            });
        },
        fetchImageCropView: function(callback) {
            this.obj.method = 'fetchImageCropView';
            this.obj.imageID = Media.imageID;
            this.send(function(answer) {
                if (Media.$cropView) {
                    Media.$cropView.slideUp('fast', function() {
                        $(this).remove();
                        Media.$cropView = $(document.body).append(answer.html).find('div.crop-view').slideDown('fast', function() {
                            typeof callback === 'function' ? callback(answer) : null;
                        });
                    });
                } else {
                    Media.$cropView = $(document.body).append(answer.html).find('div.crop-view').slideDown('fast', function() {
                        typeof callback === 'function' ? callback(answer) : null;
                    });
                }
            });
        },
        fetchGalleryField: function(callback) {
            this.obj.method = 'fetchGalleryField';
            this.obj.typeID = Media.$mediaField.data('type');
            this.obj.objectID = Media.$mediaField.data('object');
            this.send(function(answer) {
                Media.$mediaField.parent().html(answer.html).fadeIn('fast', function() {
                    Media.$mediaField = $(this).children('div.media-field');
                    typeof callback === 'function' ? callback(answer) : null;
                });
            });
        },
        fetchVideoField: function(callback) {
            this.obj.method = 'fetchVideoField';
            this.obj.typeID = Media.$mediaField.data('type');
            this.obj.objectID = Media.$mediaField.data('object');
            this.send(function(answer) {
                Media.$mediaField.parent().html(answer.html).fadeIn('fast', function() {
                    typeof callback === 'function' ? callback(answer) : null;
                });
            });
        },
        fetchYoutubeField: function(callback) {
            this.obj.method = 'fetchYoutubeField';
            this.obj.typeID = Media.$mediaField.data('type');
            this.obj.objectID = Media.$mediaField.data('object');
            this.send(function(answer) {
                Media.$mediaField.parent().html(answer.html).fadeIn('fast', function() {
                    typeof callback === 'function' ? callback(answer) : null;
                });
            });
        },
        fetchVideoPosters: function(callback) {
            this.obj.method = 'fetchVideoPosters';
            this.obj.videoID = Media.$mediaField.data('id');
            this.send(function(answer) {
                Media.poster.$window = $(answer.html).hide();
                $(document.body).append(Media.poster.$window);
                typeof callback === 'function' ? callback() : null;
            });
        },
        imageUpload: function(imageFile) {
            let formData = new FormData();
            formData.append('method', 'imageUpload');
            formData.append('typeID', Media.$mediaField.data('type'));
            formData.append('objectID', Media.$mediaField.data('object'));
            formData.append('imageID', Media.imageID);
            formData.append('image', imageFile);
            $.ajax({
                url: this.url,
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function() {
                    Media.ajax.fetchImageField();
                }
            });
        },
        galleryUpload: function(imageFile) {
            let formData = new FormData();
            formData.append('method', 'galleryUpload');
            formData.append('typeID', Media.$mediaField.data('type'));
            formData.append('objectID', Media.$mediaField.data('object'));
            formData.append('image', imageFile);
            $.ajax({
                url: this.url,
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function() {
                    Media.multipleUpload.upload();
                }
            });
        },
        videoUpload: function(videoFile) {
            let formData = new FormData();
            formData.append('method', 'videoUpload');
            formData.append('typeID', Media.$mediaField.data('type'));
            formData.append('objectID', Media.$mediaField.data('object'));
            formData.append('video', videoFile);
            $.ajax({
                url: this.url,
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function() {
                    Media.ajax.fetchVideoField(function() {
                        Media.$mediaField.find('progress').fadeOut('fast', function() {
                            toastr.info('Video uploaded successfully');
                        });
                    });
                },
                xhr: function() {
                    let xhr = $.ajaxSettings.xhr();
                    xhr.upload.addEventListener('progress', function(e) {
                        Media.$mediaField.find('progress').prop('max', e.total).val(e.loaded);
                    });
                    return xhr;
                }
            });
        },
        videoRemove: function() {
            this.obj.method = 'videoRemove';
            this.obj.videoID = Media.$mediaField.data('id');
            this.send(function() {
                Media.ajax.fetchVideoField(function() {
                    toastr.warning('Video has been deleted');
                });
            });
        },
        videoSourceRemove: function(sourceID) {
            this.obj.method = 'videoSourceRemove';
            this.obj.sourceID = sourceID;
            this.send(function() {
                Media.ajax.fetchVideoField(function() {
                    toastr.warning('Video source has been deleted');
                });
            });
        },
        videoSelectPoster: function(posterID) {
            this.obj.method = 'videoSelectPoster';
            this.obj.videoID = Media.$mediaField.data('id');
            this.obj.posterID = posterID;
            this.send(function() {
                Media.poster.close();
            });
        },
        imageRemove: function() {
            this.obj.method = 'imageRemove';
            this.obj.imageID = Media.$mediaField.data('id');
            this.send(function() {
                Media.ajax.fetchImageField(function() {
                    toastr.warning('Image removed');
                });
            });
        },
        imageCrop: function() {
            this.obj.method = 'imageCrop';
            this.obj.imageID = Media.imageID;
            this.obj.ratio = Media.crop.$fieldSet.data('ratio');
            this.obj.data = Media.crop.data;
            this.send(function() {
                Media.crop.destroy(function() {
                    Media.ajax.fetchImageCropView();
                });
            });
        },
        galleryImageRemove: function() {
            this.obj.method = 'galleryImageRemove';
            this.obj.imageID = Media.imageID;
            this.send(function() {
                Media.ajax.fetchGalleryField(function() {
                    toastr.warning('Image removed');
                });
            });
        },
        galleryRaiseImagePriority: function($btn) {
            this.obj.method = 'galleryRaiseImagePriority';
            this.obj.imageID = Media.imageID;
            this.send(function() {
                Media.ajax.fetchGalleryField(function() {
                    Media.$mediaField.find('div.gallery-image[data-id="' + Media.imageID + '"]').trigger('click');
                });
            }, function() { $btn.prop('disabled', false); });
        },
        galleryLowerImagePriority: function($btn) {
            this.obj.method = 'galleryLowerImagePriority';
            this.obj.imageID = Media.imageID;
            this.send(function() {
                Media.ajax.fetchGalleryField(function() {
                    Media.$mediaField.find('div.gallery-image[data-id="' + Media.imageID + '"]').trigger('click');
                });
            }, function() { $btn.prop('disabled', false); });
        }
    }
};