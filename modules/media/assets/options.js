// var Option = {
//     imageTypeID: undefined,
//     $imageTypes: undefined, $imageTypeForm: undefined, $typeOptions: undefined,
//     onload: document.addEventListener('DOMContentLoaded', function() { Option.init(); }),
//     init: function() {
//         // this.bind();
//     },
//     bind: function() {
//         this.$imageTypes = $('div#image-types');
//         this.$imageTypeForm = $('div#image-type-form');
//         this.$typeOptions = $('div#type-options');
//         $(document).on('click', 'tr.image-type', function() {
//             Option.$imageTypes.find('tr.image-type.selected').removeClass('selected');
//             Option.imageTypeID = $(this).addClass('selected').data('id');
//             Option.ajax.fetchImageTypeForm(function() {
//                 Option.ajax.fetchImageTypeOptions();
//             });
//         }).on('click', 'button#refresh-types', function(e) {
//             e.preventDefault();
//             $(this).prop('disabled', true);
//             Option.ajax.refreshTypes(function() {
//                 Option.ajax.fetchImageTypes();
//             });
//         }).on('click', 'button#save-image-type', function(e) {
//             e.preventDefault();
//
//         }).on('click', 'input[type="checkbox"]', function() {
//             $(this).val($(this).prop('checked') ? 1 : 0);
//         }).on('click', 'button#save-image-type', function(e) {
//             e.preventDefault();
//             Option.ajax.saveImageType($(this).prop('disabled', true));
//         });
//     },
//     ajax: {
//         url: undefined, obj: {}, send: function(success, failure) {
//             $.post(this.url, this.obj, function(answer) {
//                 Option.ajax.obj = {};
//                 answer.result && typeof success === 'function' ? success(answer) :
//                     !answer.result && typeof failure === 'function' ? failure(answer) : null;
//             }, 'JSON');
//         },
//         fetchImageTypes: function(callback) {
//             this.obj.method = 'fetchImageTypes';
//             this.send(function(answer) {
//                 Option.$imageTypes.html(answer.html).fadeIn('fast', function() {
//                     Option.$imageTypeForm = $('div#image-type-form');
//                     if (Option.imageTypeID) {
//                         Option.$imageTypes.find('tr.image-type[data-id="' + Option.imageTypeID + '"]').trigger('click');
//                     }
//                     typeof callback === 'function' ? callback(answer) : null;
//                 });
//             });
//         },
//         fetchImageTypeForm: function(callback) {
//             this.obj.method = 'fetchImageTypeForm';
//             this.obj.imageTypeID = Option.imageTypeID;
//             this.send(function(answer) {
//                 Option.$imageTypeForm.html(answer.html).fadeIn('fast', function() {
//                     typeof callback === 'function' ? callback(answer) : null;
//                 });
//             });
//         },
//         fetchImageTypeOptions: function(callback) {
//             this.obj.method = 'fetchImageTypeOptions';
//             this.obj.imageTypeID = Option.imageTypeID;
//             this.send(function(answer) {
//                 Option.$typeOptions.html(answer.html).fadeIn('fast', function() {
//                     typeof callback === 'function' ? callback(answer) : null;
//                 });
//             });
//         },
//         refreshTypes: function(callback) {
//             this.obj.method = 'refreshTypes';
//             this.send(function(answer) {
//                 toastr.info('Success', 'Generated ' + answer.count + ' bytes');
//                 typeof callback === 'function' ? callback() : null;
//             });
//         },
//         saveImageType: function($btn) {
//             this.obj.method = 'saveImageType';
//             this.obj.imageTypeID = Option.imageTypeID;
//             this.obj.data = $btn.closest('form').serializeArray();
//             this.send(function() {
//                 Option.ajax.fetchImageTypes(function() {
//                     toastr.success('Saved');
//                 });
//             }, function(answer) {
//                 $.each(answer.error, function(k, v) { toastr.error(v); });
//                 $btn.prop('disabled', false);
//             });
//         }
//     }
// };

class MediaOptions {
    imageTypeID = undefined;
    imageTypes = undefined;
    imageTypeForm = undefined;
    typeOptions = undefined;
    Ajax = undefined;
    constructor() {
        this.Ajax = new Ajax(document.querySelector('meta[name="mediaOptionAjaxUrl"]').content);
        this.imageTypes = document.querySelector('div#image-types');
        this.imageTypeForm = document.querySelector('div#image-type-form');
        this.typeOptions = document.querySelector('div#type-options');
        $(document).on('click', 'tr.image-type', (e) => {
            $(this.imageTypes).find('tr.image-type.selected').removeClass('selected');
            let $imageTypeRow = $(e.target).closest('tr.image-type').addClass('selected');
            this.imageTypeID = parseInt($imageTypeRow.data('id'));
            this.fetchImageTypeForm(() => {
                this.fetchImageTypeOptions();
            });
        }).on('click', 'button#refresh-types', (e) => {
            e.preventDefault();
            this.refreshTypes();
        }).on('click', 'button#create-type', (e) => {
            e.preventDefault();
            $(this.typeOptions).fadeOut('fast', () => {
                this.imageTypeID = 0;
                this.fetchImageTypeForm();
            });
        }).on('click', 'button#save-image-type', (e) => {
            e.preventDefault();
            this.saveImageType($(e.target).closest('button').prop('disabled', true));
        }).on('change', 'input[type="checkbox"]', function() {
            this.value = this.checked ? 1 : 0;
        });
    }
    fetchImageTypes(callback = null) {
        this.Ajax.obj.method = 'fetchImageTypes';
        this.Ajax.send((answer) => {
            $(this.imageTypes).fadeOut('fast', () => {
                $(this.imageTypes).html(answer.html).fadeIn('fast', callback);
                this.imageTypeID ? this.imageTypes.querySelector(`tr.image-type[data-id="${this.imageTypeID}"]`).classList.add('selected') : null;
                this.imageTypeForm = document.querySelector('div#image-type-form');
            });
        });
    }
    fetchImageTypeForm(callback = null) {
        this.Ajax.obj.method = 'fetchImageTypeForm';
        this.Ajax.obj.imageTypeID = this.imageTypeID;
        this.Ajax.send((answer) => {
            $(this.imageTypeForm).fadeOut('fast', () => {
                $(this.imageTypeForm).html(answer.html).fadeIn('fast', callback);
            });
        });
    }
    fetchImageTypeOptions(callback = null) {
        this.Ajax.obj.method = 'fetchImageTypeOptions';
        this.Ajax.obj.imageTypeID = this.imageTypeID;
        this.Ajax.send((answer) => {
            $(this.typeOptions).fadeOut('fast', () => {
                $(this.typeOptions).html(answer.html).fadeIn('fast', callback);
            });
        });
    }
    refreshTypes() {
        this.Ajax.obj.method = 'refreshTypes';
        this.Ajax.send((answer) => {
            toastr.info('Success', `Generated ${answer.count} bytes`);
        });
    }
    saveImageType($saveBtn) {
        this.Ajax.obj.method = 'saveImageType';
        this.Ajax.obj.imageTypeID = this.imageTypeID;
        this.Ajax.obj.data = $saveBtn.closest('form').serializeArray();
        this.Ajax.send((answer) => {
            $(this.typeOptions).fadeOut('fast', () => {
                this.imageTypeID = answer.id;
                this.fetchImageTypes(() => {
                    toastr.success('Success', 'Image type saved');
                });
            });
        }, (answer) => {
            $.each(answer.error, (k, v) => { toastr.error(v); });
            $saveBtn.prop('disabled', false);
        });
    }
}

document.addEventListener('DOMContentLoaded', () => { new MediaOptions(); });