class Ajax {
    url = undefined;
    obj = {};
    constructor(url = document.querySelector('meta[name="ajaxUrl"]').content) {
        this.url = url;
    }
    send(success, failure) {
        $.post(this.url, this.obj, (answer) => {
            answer.result ? this.success(answer, success) : this.failure(answer, failure);
        }, 'JSON');
    }
    success(answer, callback = null) {
        typeof callback === 'function' ? callback(answer) : console.log(answer);
    }
    failure(answer, callback = null) {
        typeof callback === 'function' ? callback(answer) : console.log(answer);
    }
}