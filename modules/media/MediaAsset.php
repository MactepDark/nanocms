<?php namespace app\modules\media;

use Yii;
use yii\helpers\Url;
use yii\web\AssetBundle;
use yii\web\View;

class MediaAsset extends AssetBundle {

    function __construct() {
        Yii::$app->view->registerJs('Media.ajax.url = "'.Url::toRoute(['/media/default/ajax']).'";', View::POS_END);
        parent::__construct();
    }

    public $sourcePath = '@app/modules/media/assets';
    public $css = ['media.css'];
    public $js = [
        'vendors.js',
        'media.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\modules\media\CropperAsset'
    ];
}