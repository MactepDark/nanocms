<?php namespace app\modules\media;

use Yii;
use yii\helpers\Url;
use yii\web\AssetBundle;
use yii\web\View;

class MediaOptionsAsset extends AssetBundle {

    function __construct() {
        Yii::$app->view->registerMetaTag([
            'name' => 'mediaOptionAjaxUrl', 'content' => Url::toRoute(['/media/default/ajax'])
        ]);
        parent::__construct();
    }

    public $sourcePath = '@app/modules/media/assets';
    public $css = ['options.css'];
    public $js = [
        'vendors.js',
        'options.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}