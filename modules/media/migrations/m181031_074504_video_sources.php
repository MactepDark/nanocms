<?php

use yii\db\Migration;

/**
 * Class m181031_074504_video_sources
 */
class m181031_074504_video_sources extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('video_sources', [
            'id' => $this->primaryKey(),
            'video_id' => $this->integer()->notNull(),
            'filename' => $this->string()->notNull(),
            'format' => $this->string()->notNull(),
            'size' => $this->integer()->notNull()
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('video_sources');
    }
}
