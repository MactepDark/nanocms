<?php

use yii\db\Migration;

/**
 * Class m181029_125440_image_type_option
 */
class m181029_125440_image_type_option extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('image_type_option', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'media' => $this->string()->notNull(),
            'ratio' => $this->decimal(4, 3)->notNull(),
            'width' => $this->integer()->notNull()->defaultValue(0),
            'height' => $this->integer()->notNull()->defaultValue(0)
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('image_type_option');
    }
}
