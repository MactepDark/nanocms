<?php

use yii\db\Migration;

/**
 * Class m181029_125417_image
 */
class m181029_125417_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('image', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->notNull(),
            'object' => $this->integer()->notNull(),
            'filename' => $this->string()->notNull(),
            'priority' => $this->integer()->notNull(),
            'format' => $this->string()->notNull(),
            'size' => $this->integer()->notNull(),
            'width' => $this->integer()->notNull(),
            'height' => $this->integer()->notNull()
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('image');
    }
}
