<?php

use yii\db\Migration;

/**
 * Class m181029_125431_image_type
 */
class m181029_125431_image_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('image_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'const' => $this->string()->notNull(),
            'multiple' => $this->smallInteger(1)->notNull(),
            'folder' => $this->string()->notNull(),
            'template' => $this->string()->notNull(),
            'alt' => $this->string()->notNull()->defaultValue(json_encode([])),
            'title' => $this->string()->notNull()->defaultValue(json_encode([]))
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('image_type');
    }
}
