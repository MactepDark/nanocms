<?php

use yii\db\Migration;

/**
 * Class m181031_074455_video
 */
class m181031_074455_video extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('video', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->notNull(),
            'object' => $this->integer()->notNull(),
            'poster' => $this->string()
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('video');
    }
}
