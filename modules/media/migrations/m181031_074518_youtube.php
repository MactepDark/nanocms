<?php

use yii\db\Migration;

/**
 * Class m181031_074518_youtube
 */
class m181031_074518_youtube extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('youtube', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->notNull(),
            'object' => $this->integer()->notNull(),
            'name' => $this->string(),
            'hash' => $this->string()->notNull()
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('youtube');
    }
}