<?php

use yii\db\Migration;

/**
 * Class m181110_182850_video_type
 */
class m181110_182850_video_type extends Migration {

    public function up() {
        $this->createTable('video_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'const' => $this->string()->unique()->notNull(),
            'folder' => $this->string()->notNull(),
            'template' => $this->string()->notNull()->defaultValue(json_encode([]))
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    public function down() {
        $this->dropTable('video_type');
    }
}
