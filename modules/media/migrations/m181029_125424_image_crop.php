<?php

use yii\db\Migration;

/**
 * Class m181029_125424_image_crop
 */
class m181029_125424_image_crop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('image_crop', [
            'id' => $this->primaryKey(),
            'image_id' => $this->integer()->notNull(),
            'option_id' => $this->integer()->notNull(),
            'filename' => $this->string(),
            'x' => $this->integer()->notNull(),
            'y' => $this->integer()->notNull(),
            'width' => $this->integer()->notNull(),
            'height' => $this->integer()->notNull()
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('image_crop');
    }
}
