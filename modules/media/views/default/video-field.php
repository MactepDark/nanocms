<?php
/**
 * @var $this yii\web\View
 * @var $typeID integer
 * @var $objectID integer
 * @var $videoType object
 * @var $video object
 * @var $sources array
 */
use app\modules\media\models\Video;
?>
<div class="media-field video-field" data-type="<?= $typeID ?>" data-object="<?= $objectID ?>" data-id="<?= $video ? $video->id : 0 ?>">
    <div class="header">
        <?php if($video): ?>
            <?php foreach ($sources as $source): ?>
            <span class="badge alert-success pull-right" data-id="<?= $source['id'] ?>">
                <span class="glyphicon glyphicon-film"></span>
                <?php list($type, $format) = explode('/', $source['format']) ?>
                <?= implode(' ', [strtoupper($format), '['.Yii::$app->formatter->asShortSize($source['size'], 1).']']) ?>
                <button class="btn btn-xs btn-danger pull-right source-remove" style="font-size: 10px; padding: 0 3px; margin-left: 5px;">
                    <span class="glyphicon glyphicon-remove"></span>
                </button>
            </span>
            <?php endforeach; ?>
        <?php elseif (!$video): ?>
            <span class="badge alert-warning pull-right">
                <span class="glyphicon glyphicon-warning-sign"></span>
                <?= Yii::t('admin', 'Видео не загружено') ?>
            </span>
        <?php endif; ?>
        <span class="badge alert-info">
            <span class="glyphicon glyphicon-facetime-video"></span>
            <?= $videoType->name ?>
        </span>
    </div>
    <?php if(!$video): ?>
        <?= \app\modules\media\Media::NO_VIDEO ?>
    <?php elseif ($video): ?>
        <?= Video::tagVideo($typeID, $objectID) ?>
    <?php elseif (false): ?>
        <video controls style="width: 100%;">
            <?php foreach ($sources as $source): ?>
                <source src="<?= $source['filename'] ?>" type="<?= $source['format'] ?>">
            <?php endforeach; ?>
        </video>
    <?php endif; ?>
    <div class="toolbar">
        <button class="btn btn-xs btn-primary video-upload">
            <span class="glyphicon glyphicon-floppy-open"></span>
            <strong><?= Yii::t('admin', 'Загрузить') ?></strong>
            <span title="<?= Yii::t('admin', 'Максимальный размер загружаемого файла') ?>">
                [<?= ini_get('post_max_size') ?>]
            </span>
        </button>
        <button class="btn btn-xs btn-default video-poster" <?= $video ? '' : 'disabled' ?>>
            <span class="glyphicon glyphicon-picture" style="color: <?= $video ? $video->poster ? 'green' : 'red' : 'silver' ?>;"></span>
            <?= Yii::t('admin', 'Постер') ?>
        </button>
        <button class="btn btn-xs btn-danger video-remove pull-right" <?= $video ? '' : 'disabled' ?>>
            <span class="glyphicon glyphicon-remove"></span>
        </button>
    </div>
    <progress></progress>
</div>
