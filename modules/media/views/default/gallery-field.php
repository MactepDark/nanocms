<?php
/**
 * @var $this yii\web\View
 * @var $typeID integer
 * @var $objectID integer
 * @var $images array
 * @var $imageType object
 */
use app\modules\media\models\Image;
?>
<div class="media-field gallery-field" data-type="<?= $typeID ?>" data-object="<?= $objectID ?>">
    <div class="panel panel-default">
        <div class="panel-heading">
            <button class="btn btn-xs btn-primary pull-right gallery-upload">
                <span class="glyphicon glyphicon-floppy-open"></span>
                <?= Yii::t('admin', 'Загрузить') ?>
            </button>
            <h4 class="panel-title">
                <span class="glyphicon glyphicon-picture"></span>
                <?= $imageType->name ?>
                <span class="badge"><?= count($images) ?></span>
            </h4>
        </div>
        <div class="panel-body">
            <?php foreach ($images as $image): ?>
                <div class="gallery-image" data-id="<?= $image['id'] ?>">
                    <div class="label label-success"><?= $image['priority'] ?></div>
                    <img src="<?= Image::getSrc($image['id']) ?>" />
                </div>
            <?php endforeach; ?>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-xs-4">
                    <div class="btn-group controls">
                        <button class="btn btn-sm btn-default gallery-raise-priority">
                            <span class="glyphicon glyphicon-arrow-left"></span>
                        </button>
                        <button class="btn btn-sm btn-default gallery-lower-priority">
                            <span class="glyphicon glyphicon-arrow-right"></span>
                        </button>
                        <button class="btn btn-sm btn-warning image-crop">
                            <span class="glyphicon glyphicon-scissors"></span>
                            <?= Yii::t('admin', 'Обрезать') ?>
                        </button>
                        <button class="btn btn-sm btn-danger gallery-image-remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </div>
                </div>
                <div class="col-xs-8">
                    <progress max="0" value="0"></progress>
                </div>
            </div>
        </div>
    </div>
</div>