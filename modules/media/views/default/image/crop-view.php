<?php
/**
 * @var $this yii\web\View
 * @var $image object
 * @var $typeOptions array
 */
use app\modules\media\Media;
use app\modules\media\models\ImageCrop;

?>
<div class="crop-view">
    <div class="source">
        <img src="<?= $image->filename.'?'.time() ?>" class="original-image" />
    </div>
    <div class="controls">
        <div class="image-info">
                <span class="badge alert-success x">
                    X:
                    <span class="value">0</span>
                </span>
            <span class="badge alert-success y">
                    Y:
                    <span class="value">0</span>
                </span>
            <span class="badge alert-success width">
                    Width:
                    <span class="value"><?= $image->width ?></span>
                </span>
            <span class="badge alert-success height">
                    Height:
                    <span class="value"><?= $image->height ?></span>
                </span>
        </div>
        <button class="btn btn-sm btn-primary crop-image" disabled>
            <span class="glyphicon glyphicon-scissors"></span>
            <?= Yii::t('admin', 'Обрезать') ?>
        </button>
        <button class="btn btn-sm btn-default pull-right crop-close">
            <span class="glyphicon glyphicon-ban-circle"></span>
            <?= Yii::t('admin', 'Отмена') ?>
        </button>
        <div class="ratios">
            <?php foreach ($typeOptions as $typeOption): ?>
                <fieldset data-ratio="<?= $typeOption['ratio'] ?>">
                    <legend>
                        <small>
                            <strong>[<?= $typeOption['ratio'] ?>]</strong>
                            <?= $typeOption['name'] ?>
                        </small>
                    </legend>
                    <?php if($link = ImageCrop::getSrcByRatio($image->id, $typeOption['ratio'])): ?>
                        <?php if(isset($link['filename']) && strlen($link['filename']) > 0): ?>
                            <img src="<?= $link['filename'].'?'.time() ?>" class="preview" data-option="<?= $link['option_id'] ?>"
                                 data-x="<?= $link['x'] ?>" data-y="<?= $link['y'] ?>"
                                 data-width="<?= $link['width'] ?>" data-height="<?= $link['height'] ?>" />
                        <?php else: ?>
                            <?= Media::NO_IMAGE ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <?= Media::NO_IMAGE ?>
                    <?php endif; ?>
                </fieldset>
            <?php endforeach; ?>
        </div>
    </div>
</div>
