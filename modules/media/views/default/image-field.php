<?php
/**
 * @var $this yii\web\View
 * @var $typeID integer
 * @var $objectID integer
 * @var $image object
 * @var $imageType object
 * @var $typeOptions array
 */
use app\modules\media\models\Image;
use app\modules\media\Media;
?>
<div class="media-field image-field" data-type="<?= $typeID ?>" data-object="<?= $objectID ?>" data-id="<?= $image ? $image->id : 0 ?>">
    <div class="header">
        <?php if($image): ?>
            <span class="badge alert-success pull-right">
                <span class="glyphicon glyphicon-ok-circle"></span>
                <?= implode('x', [$image->width, $image->height]) ?>
            </span>
        <?php elseif (!$image): ?>
            <span class="badge alert-warning pull-right">
                <span class="glyphicon glyphicon-warning-sign"></span>
                <?= Yii::t('admin', 'Изображение отсутствует') ?>
            </span>
        <?php endif; ?>
        <span class="badge alert-info">
            <span class="glyphicon glyphicon-picture"></span>
            <?= $imageType->name ?>
        </span>
    </div>
    <?php if($image): ?>
        <img src="<?= Image::getSrc($image->id).'?'.microtime(true) ?>" class="image-field-preview" />
    <?php elseif (!$image): ?>
        <?= Media::NO_IMAGE ?>
    <?php endif; ?>
    <div class="toolbar">
        <button class="btn btn-xs btn-primary image-upload">
            <span class="glyphicon glyphicon-floppy-open"></span>
            <?= Yii::t('admin', 'Загрузить') ?>
        </button>
        <button class="btn btn-xs btn-warning image-crop" <?= $image ? '' : 'disabled' ?>>
            <span class="glyphicon glyphicon-scissors"></span>
            <?= Yii::t('admin', 'Обрезать') ?>
        </button>
        <button class="btn btn-xs btn-danger image-remove pull-right" <?= $image ? '' : 'disabled' ?>>
            <span class="glyphicon glyphicon-remove"></span>
        </button>
    </div>
</div>