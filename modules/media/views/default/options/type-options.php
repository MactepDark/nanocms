<?php
/**
 * @var $this yii\web\View
 * @var $imageType object
 * @var $typeOptions array
 */
?>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-wrench"></span>
            <?= Yii::t('admin', 'Настройка') ?>
            <strong><?= $imageType->name ?></strong>
        </h4>
    </div>
    <div class="panel-body" style="padding: 5px;">
        <?php foreach ($typeOptions as $ratio => $options): ?>
        <fieldset>
            <legend>
                <button class="btn btn-xs btn-success pull-right" disabled>
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
                <span class="glyphicon glyphicon-scissors"></span>
                <strong><?= $ratio ?></strong>
            </legend>
            <table class="table table-condensed table-bordered table-striped table-hover" style="margin-bottom: 0;">
                <thead>
                <tr>
                    <th>
                        <span class="glyphicon glyphicon-pencil"></span>
                        <?= Yii::t('admin', 'Название') ?>
                    </th>
                    <th>
                        <span class="glyphicon glyphicon-leaf"></span>
                        <?= Yii::t('admin', 'Медиа') ?>
                    </th>
                    <th>
                        <span class="glyphicon glyphicon-resize-horizontal"></span>
                        <?= Yii::t('admin', 'Ширина') ?>
                    </th>
                    <th>
                        <span class="glyphicon glyphicon-resize-vertical"></span>
                        <?= Yii::t('admin', 'Высота') ?>
                    </th>
                    <th><span class="glyphicon glyphicon-remove"></span></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($options as $optionID => $option): ?>
                <tr>
                    <td>
                        <input type="text" class="form-control input-sm" value="<?= $option['name'] ?>" />
                    </td>
                    <td>
                        <input type="number" class="form-control input-sm" value="<?= $option['media'] ?>" />
                    </td>
                    <td>
                        <input type="number" class="form-control input-sm" value="<?= $option['width'] ?>" />
                    </td>
                    <td>
                        <input type="number" class="form-control input-sm" value="<?= $option['height'] ?>" />
                    </td>
                    <td style="width: 30px;">
                        <button class="btn btn-xs btn-danger btn-block">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </fieldset>
        <?php endforeach; ?>
    </div>
    <div class="panel-footer" style="padding: 3px;">
        <button class="btn btn-sm btn-success" disabled>
            <span class="glyphicon glyphicon-floppy-disk"></span>
            <?= Yii::t('admin', 'Сохранить') ?>
        </button>
        <button class="btn btn-sm btn-primary" disabled>
            <span class="glyphicon glyphicon-play"></span>
            <?= Yii::t('admin', 'Применить') ?>
        </button>
    </div>
</div>