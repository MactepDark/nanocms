<?php
/**
 * @var $this yii\web\View
 */
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-xs-7" id="image-types">
        <?= $this->render('image-types') ?>
    </div>
    <div class="col-xs-5" id="type-options"></div>
</div>