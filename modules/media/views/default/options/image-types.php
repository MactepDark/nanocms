<?php
/**
 * @var $this yii\web\View
 */

use app\modules\media\models\ImageType;
use app\modules\media\models\Image;
?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <button class="btn btn-xs btn-success pull-right" id="create-type">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('admin', 'Добавить') ?>
        </button>
        <button class="btn btn-xs btn-default pull-right" id="refresh-types">
            <span class="glyphicon glyphicon-refresh"></span>
        </button>
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-picture"></span>
            <?= Yii::t('admin', 'Типы изображений') ?>
        </h4>
    </div>
    <table class="table table-condensed table-bordered panel-body">
        <thead>
        <tr>
            <th>ID</th>
            <th><?= Yii::t('admin', 'Галерея') ?></th>
            <th><?= Yii::t('admin', 'Каталог') ?></th>
            <th><?= Yii::t('admin', 'Название') ?></th>
            <th><?= Yii::t('admin', 'Константа') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach (ImageType::find()->asArray()->all() as $imageType): ?>
        <tr class="image-type" data-id="<?= $imageType['id'] ?>">
            <td style="width: 50px; background-color: #eee; font-weight: bold;">
                <?= $imageType['id'] ?>
            </td>
            <td style="width: 75px; background-color: #eee;">
                <?php if($imageType['multiple'] == ImageType::IS_MULTIPLE): ?>
                    <span class="glyphicon glyphicon-ok"></span>
                <?php endif; ?>
            </td>
            <td style="width: 100px; background-color: #eee;">
                <?= $imageType['folder'] ?>
            </td>
            <td>
                <span class="badge alert-info pull-right">
                    <span class="glyphicon glyphicon-picture"></span>
                    <?= Image::find()->where(['type' => $imageType['id']])->count() ?>
                </span>
                <strong><?= $imageType['name'] ?></strong>
            </td>
            <td>
                <code><?= $imageType['const'] ?></code>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="panel-footer">
        <div id="image-type-form"></div>
    </div>
</div>