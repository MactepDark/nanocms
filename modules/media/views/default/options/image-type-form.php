<?php
/**
 * @var $this yii\web\View
 * @var $imageType object
 */
use app\modules\media\models\ImageType;
?>
<form class="form-horizontal">
    <div class="row form-group">
        <div class="col-xs-8" style="font-size: 16px; font-weight: bold; margin-top: 14px; padding-left: 25px;">
            <span class="glyphicon glyphicon-edit"></span>
            <strong><?= $imageType->name ?></strong>
        </div>
        <div class="col-xs-4">
            <button class="btn btn-xs btn-success btn-block" id="save-image-type">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                <?= Yii::t('admin', 'Сохранить') ?>
            </button>
        </div>
    </div>
    <div class="row form-group">
        <label class="col-xs-2 control-label">ID:</label>
        <div class="col-xs-10">
            <input type="number" name="id" class="form-control input-sm" value="<?= $imageType->id ?>" placeholder="<?= Yii::t('admin', 'Будет присвоен новый ID') ?>" />
        </div>
    </div>
    <div class="row form-group">
        <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Галерея') ?>:</label>
        <div class="col-xs-10">
            <input type="checkbox" class="checkbox" name="multiple"
                   value="<?= $imageType->multiple == ImageType::IS_MULTIPLE ? '1' : '0' ?>"
                <?= $imageType->multiple == ImageType::IS_MULTIPLE ? 'checked' : '' ?>
                <?= $imageType->id ? 'disabled' : '' ?> />
        </div>
    </div>
    <div class="row form-group">
        <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Каталог') ?>:</label>
        <div class="col-xs-10">
            <input type="text" name="folder" class="form-control input-sm" value="<?= $imageType->folder ?>" placeholder="<?= Yii::t('admin', 'Каталог хранения, например, article') ?>" />
        </div>
    </div>
    <div class="row form-group">
        <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Название') ?>:</label>
        <div class="col-xs-10">
            <input type="text" name="name" class="form-control input-sm" value="<?= $imageType->name ?>" placeholder="<?= Yii::t('admin', 'Название типа изображения') ?>" />
        </div>
    </div>
    <div class="row form-group">
        <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Константа') ?>:</label>
        <div class="col-xs-10">
            <input type="text" name="const" class="form-control input-sm" value="<?= $imageType->const ?>" placeholder="<?= Yii::t('admin', 'Например, IMAGE_TYPE_ARTICLE_SLIDER') ?>" />
        </div>
    </div>
    <div class="row form-group">
        <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Шаблон') ?>:</label>
        <div class="col-xs-10">
            <input type="text" name="template" class="form-control input-sm" value="<?= htmlspecialchars($imageType->template) ?>" placeholder="<?= Yii::t('admin', 'JSON-строка шаблона имени файлов') ?>" />
        </div>
    </div>
    <div class="row form-group">
        <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Альты') ?>:</label>
        <div class="col-xs-10">
            <input type="text" name="alt" class="form-control input-sm" value="<?= htmlspecialchars($imageType->alt) ?>" placeholder="<?= Yii::t('admin', 'JSON-строка шаблона генератора альтов') ?>" />
        </div>
    </div>
    <div class="row">
        <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Тайтлы') ?>:</label>
        <div class="col-xs-10">
            <input type="text" name="title" class="form-control input-sm" value="<?= htmlspecialchars($imageType->title) ?>" placeholder="<?= Yii::t('admin', 'JSON-строка шаблона генератора тайтлов') ?>" />
        </div>
    </div>
</form>