<?php
/**
 * @var $this yii\web\View
 * @var $posters array
 */
?>
<div class="video-posters">
    <?php foreach ($posters as $posterID => $posterUrl): ?>
        <img src="<?= $posterUrl ?>" data-id="<?= $posterID ?>" />
    <?php endforeach; ?>
</div>