<?php
/**
 * @var $this yii\web\View
 * @var $video object
 * @var $sources array
 */
?>
<video controls <?= $video->poster ? ' poster="'.$video->poster.'"' : '' ?> style="width: 100%;">
    <?php foreach ($sources as $source): ?>
        <source src="<?= $source['filename'] ?>" type="<?= $source['format'] ?>">
    <?php endforeach; ?>
</video>
