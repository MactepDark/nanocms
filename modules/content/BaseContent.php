<?php namespace app\modules\content;

use app\modules\content\models\Content;
use app\models\Language;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\Module;

class BaseContent extends Module implements BootstrapInterface {

    public $controllerNamespace = 'app\modules\content\controllers';

    public function bootstrap($app) {
        $app->getUrlManager()->addRules([
            'contentIndex' => 'content/default/index',
            'contentAjax' => 'content/default/ajax'
        ], false);
    }

    public function init() {
        Language::setupLanguages();
        parent::init();
    }

    const CONTENT_FIELD_VIEW = '@app/modules/content/views/default/content-field';

    const FIELD_INPUT = 1;
    const FIELD_TEXTAREA = 2;
    const FIELD_EDITOR = 3;
    const FIELD_CHECKBOX = 4;
    const FIELD_CODE_MIRROR = 5;

    public static $fieldViews = [
        self::FIELD_INPUT => 'field-input',
        self::FIELD_TEXTAREA => 'field-textarea',
        self::FIELD_EDITOR => 'field-editor',
        self::FIELD_CHECKBOX => 'field-checkbox',
        self::FIELD_CODE_MIRROR => 'field-code-mirror'
    ];

    const OBJ_NOT_SET = 0;
    const LANG_NOT_SET = '0';

    const DEFAULT_MAP_LAT = 49;
    const DEFAULT_MAP_LNG = 32;
    const DEFAULT_MAP_ZOOM = 5;



    public static function contentField($typeID, $objectID = self::OBJ_NOT_SET, $fieldType = self::FIELD_INPUT, $selectedLang = false) {
        $selectedLang = $selectedLang === false ? Yii::$app->language : $selectedLang;
        $content = Content::getContent($typeID, $objectID, $selectedLang);
        $langButtons = [];
        if ($selectedLang == self::LANG_NOT_SET) {
            $langButtons[self::LANG_NOT_SET] = [
                'view' => $allLang = Yii::t('admin', 'Все'),
                'name' => $allLang,
                'class' => implode(' ', [
                    'btn', 'btn-xs', 'btn-primary',
                    strlen(Content::getContent($typeID, $objectID, self::LANG_NOT_SET)->value) > 0 ? 'has-content' : ''
                ])
            ];
        } else {
            foreach (Language::find()->select(['language.*', 'LENGTH(content.value) as length'])->leftJoin(
                'content', implode(' AND ', [
                    'content.type = '.$typeID,
                    'content.object = '.$objectID,
                    'content.language = language.code'
                ])
            )->asArray()->all() as $lang) {
                $langButtons[$lang['code']] = [
                    'view' => $lang['view'],
                    'name' => Content::get(CONTENT_LANGUAGE_NAME, $lang['id'], $lang['code']),
                    'class' => implode(' ', [
                        'btn', 'btn-xs',
                        $selectedLang == $lang['code'] ? 'btn-primary' : 'btn-default',
                        $lang['length'] > 0 ? 'has-content' : ''
                    ])
                ];
            }
        }
        return Yii::$app->view->render(self::CONTENT_FIELD_VIEW, [
            'content' => $content, 'typeID' => $typeID, 'objectID' => $objectID,
            'fieldType' => $fieldType, 'fieldView' => self::$fieldViews[$fieldType],
            'selectedLangCode' => $selectedLang === false ? Yii::$app->language : $selectedLang,
            'langButtons' => $langButtons
        ]);
    }

    public static function saveContent($ID, $value) {
        Yii::$app->cache->flush();
        return Content::updateAll(['value' => $value], ['id' => $ID]);
    }
}
