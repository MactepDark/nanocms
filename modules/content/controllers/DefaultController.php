<?php namespace app\modules\content\controllers;

use app\modules\content\BaseContent;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class DefaultController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['actions' => ['index', 'ajax'], 'allow' => true, 'roles' => ['@']]
                ]
            ]
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionAjax() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = ['result' => false, 'error' => [], 'post' => $post = Yii::$app->request->post()];
        if (isset($post['method'])) {
            switch ($post['method']) {
                case 'fetchContentField': $result = [
                    'result' => true, 'html' => BaseContent::contentField($post['typeID'], $post['objectID'], $post['fieldType'], $post['langCode'])
                ];
                    break;
                case 'saveContent': $result['result'] = BaseContent::saveContent($post['ID'], $post['value']);
                    break;

                default: $result['error'][] = 'Unknown method'; break;
            }
        }
        return $result;
    }
}
