<?php namespace app\modules\content;

use Yii;
use yii\helpers\Url;
use yii\web\AssetBundle;
use yii\web\View;

class ContentAsset extends AssetBundle {

    function __construct() {
        Yii::$app->view->registerJs('Content.ajax.url = "'.Url::toRoute(['content/default/ajax']).'";', View::POS_END);
        parent::__construct();
    }

    public $sourcePath = '@app/modules/content/assets';
    public $css = ['content.css'];
    public $js = ['content.js'];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\modules\content\CodeMirrorAsset',
        'app\modules\content\SummerNoteAsset'
    ];
}