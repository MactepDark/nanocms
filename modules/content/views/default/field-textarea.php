<?php
/**
 * @var $this yii\web\View
 * @var $content object
 */
?>
<textarea class="form-control content-value" data-id="<?= $content->id ?>" rows="4"><?= htmlspecialchars($content->value) ?></textarea>