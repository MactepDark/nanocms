<?php
/**
 * @var $this yii\web\View
 * @var $content object
 */
?>
<textarea class="form-control content-value code-mirror" data-id="<?= $content->id ?>"><?= htmlspecialchars($content->value) ?></textarea>