<?php
/**
 * @var $this yii\web\View
 * @var $content object
 * @var $typeID integer
 * @var $objectID integer
 * @var $fieldType integer
 * @var $fieldView string
 * @var $selectedLangCode string
 * @var $langButtons array
 */

?>
<div class="content-field" data-type="<?= $typeID ?>" data-object="<?= $objectID ?>" data-field="<?= $fieldType ?>">
    <div class="btn-group">
        <?php foreach ($langButtons as $langCode => $langButton): ?>
        <button class="<?= $langButton['class'] ?>" data-lang="<?= $langCode ?>" title="<?= $langButton['name'] ?>">
            <?= $langButton['view'] ?>
        </button>
        <?php endforeach; ?>
    </div>
    <?= $this->render($fieldView, ['content' => $content]) ?>
</div>