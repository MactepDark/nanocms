<?php
/**
 * @var $this yii\web\View
 * @var $content object
 */
?>
<button class="btn btn-xs btn-success pull-right editor-save" disabled>
    <span class="glyphicon glyphicon-floppy-disk"></span>
    <?= Yii::t('admin', 'Сохранить') ?>
</button>
<textarea class="form-control content-value editor" data-id="<?= $content->id ?>" rows="4" style="display: none;"><?= htmlspecialchars($content->value) ?></textarea>