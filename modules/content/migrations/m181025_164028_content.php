<?php

use yii\db\Migration;

/**
 * Class m181025_164028_content
 */
class m181025_164028_content extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('content', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->notNull(),
            'object' => $this->integer()->notNull(),
            'language' => $this->string()->notNull(),
            'value' => $this->text()
        ], DEFAULT_MYSQL_TABLE_OPTIONS);
        $this->createIndex('typeID', 'content', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('content');
    }
}
