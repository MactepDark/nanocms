var Content = {
    $contentField: undefined,
    onload: document.addEventListener('DOMContentLoaded', function() { Content.init(); }),
    init: function() {
        this.initCodeMirror();
        this.initSummerNote();
        this.bind();
    },
    bind: function() {
        $(document).on('click', 'div.content-field button[data-lang]', function(e) {
            e.preventDefault();
            Content.$contentField = $(this).closest('div.content-field');
            Content.ajax.fetchContentField($(this).prop('disabled', true).data('lang'));
        }).on('input change', 'div.content-field .content-value', function() {
            Content.$contentField = $(this).closest('div.content-field').addClass('has-changes');
        }).on('blur', 'div.content-field.has-changes .content-value', function() {
            Content.$contentField = $(this).closest('div.content-field');
            Content.ajax.saveContent();
        }).on('click', 'div.content-field .content-value.checkbox', function() {
            $(this).val($(this).prop('checked') ? 1 : 0);
        }).on('click', 'div.content-field button.editor-save', function(e) {
            e.preventDefault();
            Content.$contentField = $(this).closest('div.content-field');
            Content.ajax.saveContent();
        });
    },
    initCodeMirror: function() {
        $.each($('textarea.code-mirror'), function(k, v) {
            let editor = CodeMirror.fromTextArea(v, {lineNumbers: true, mode: 'htmlmixed', theme: 'cobalt'});
            editor.on('change', function(instance) {
                Content.$contentField = $(instance.getTextArea()).closest('div.content-field').addClass('has-changes');
            });
            editor.on('blur', function(instance) {
                Content.$contentField = $(instance.getTextArea()).closest('div.content-field');
                if (Content.$contentField.hasClass('has-changes')) {
                    Content.$contentField.find('.content-value').val(instance.getValue());
                    Content.ajax.saveContent();
                }
            });
            console.log(editor.setSize('100%', 'calc(100vh - 125px)'));
        });
    },
    initSummerNote: function() {
        $.each($('textarea.content-value.editor'), function(k, v) {
            $(v).summernote({
                lang: 'ru-RU',
                height: 'calc(100vh - 250px)',
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['insert', ['link', 'hr', 'table']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['misc', ['fullscreen', 'codeview']]
                ],
                callbacks: {
                    onChange: function() {
                        let $contentField = $(this).closest('div.content-field').addClass('has-changes');
                        $contentField.find('button.editor-save').prop('disabled', false);
                        $contentField.find('button[data-lang]').prop('disabled', true);
                    }
                }
            })
        });
    },
    callbacks: {
        afterSave: undefined,
        changeLang: undefined
    },
    ajax: {
        obj: {}, url: undefined, send: function(success, failure) {
            $.post(this.url, this.obj, function(answer) {
                Content.ajax.obj = {};
                answer.result && typeof success === 'function' ? success(answer) :
                    !answer.result && typeof failure === 'function' ? failure(answer) : null;
            }, 'JSON');
        },
        fetchContentField: function(langCode) {
            this.obj.method = 'fetchContentField';
            this.obj.typeID = Content.$contentField.data('type');
            this.obj.objectID = Content.$contentField.data('object');
            this.obj.fieldType = Content.$contentField.data('field');
            this.obj.langCode = langCode;
            this.send(function(answer) {
                Content.$contentField.parent().html(answer.html).fadeIn('fast', function() {
                    if (Content.$contentField.find('textarea.code-mirror')) { Content.initCodeMirror(); }
                    if (Content.$contentField.find('textarea.editor')) { Content.initSummerNote(); }
                });
                typeof Content.callbacks.changeLang === 'function' ? Content.callbacks.changeLang(langCode) : null;
            });
        },
        saveContent: function(callback) {
            this.obj.method = 'saveContent';
            this.obj.ID = Content.$contentField.find('.content-value').data('id');
            this.obj.value = Content.$contentField.find('.content-value').val();
            this.send(function() {
                Content.ajax.fetchContentField(Content.$contentField.find('button.btn-primary').data('lang'));
                toastr.success('Saved');
                typeof callback === 'function' ? callback() :
                    typeof Content.callbacks.afterSave === 'function' ? Content.callbacks.afterSave() : null;
            }, function() {
                Content.ajax.fetchContentField(Content.$contentField.find('button.btn-primary').data('lang'));
            });
        }
    }
};