<?php namespace app\modules\content;

use yii\web\AssetBundle;

class SummerNoteAsset extends AssetBundle {

    public $sourcePath = '@app/modules/content/assets';
    public $css = ['summernote/summernote.css'];
    public $js = [
        'summernote/summernote.min.js',
        'summernote/summernote-ru-RU.js'
    ];
    public $depends = ['yii\web\YiiAsset'];
}
