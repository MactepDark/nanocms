<?php

namespace app\modules\content\models;

use app\modules\content\BaseContent;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "content".
 *
 * @property int $id
 * @property int $type
 * @property int $object
 * @property string $language
 * @property string $value
 */
class Content extends ActiveRecord {

    public static function tableName() { return 'content'; }

    public function rules() {
        return [
            [['type', 'object', 'language'], 'required'],
            [['type', 'object'], 'integer'],
            [['value'], 'string'],
            [['language'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'object' => 'Object',
            'language' => 'Language',
            'value' => 'Value',
        ];
    }

    public static function getContent($typeID, $objectID, $langCode = false) {
        if ($content = self::findOne([
            'type' => $typeID,
            'object' => $objectID,
            'language' => $langCode === false ? Yii::$app->language : $langCode
        ])) {
            return $content;
        } else {
            return self::add($typeID, $objectID, $langCode);
        }
    }

    public static function get($typeID, $objectID, $langCode = false) {
        if ($content = self::getContent($typeID, $objectID, $langCode)) {
            return $content->value;
        } else { return null; }
    }

    public static function add($typeID, $objectID, $langCode = false, $value = null) {
        $newContent = new self;
        $newContent->type = $typeID;
        $newContent->object = $objectID;
        $newContent->language = $langCode === false ? Yii::$app->language : $langCode;
        $newContent->value = $value;
        return $newContent->save() ? $newContent : new self;
    }

    public static function set($typeID, $objectID, $langCode = false, $value = null) {
        if ($content = self::getContent($typeID, $objectID, $langCode)) {
            $content->value = $value;
            $content->save();
        } else {
            self::add($typeID, $objectID, $langCode, $value);
        }
        return true;
    }

    public static function remove($typeID, $objectID) {
        return self::deleteAll(['type' => $typeID, 'object' => $objectID]);
    }


    public static function saveMapMarker($lat, $lng, $zoom) {
        self::set(CONTENT_CONTACT_MAP_LAT, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET, $lat);
        self::set(CONTENT_CONTACT_MAP_LNG, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET, $lng);
        self::set(CONTENT_CONTACT_MAP_ZOOM, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET, $zoom);
        return true;
    }
}
