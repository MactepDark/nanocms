<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\content;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CodeMirrorAsset extends AssetBundle {

    public $sourcePath = '@app/modules/content/assets';
    public $css = ['codeMirror/codemirror.css', 'codeMirror/theme/cobalt.css'];
    public $js = [
        'codeMirror/codemirror.js',
        'codeMirror/mode/css/css.js',
        'codeMirror/mode/xml/xml.js',
        'codeMirror/mode/javascript/javascript.js',
        'codeMirror/mode/vbscript/vbscript.js',
        'codeMirror/mode/htmlmixed/htmlmixed.js',
        'codeMirror/mode/php/php.js'
    ];
    public $depends = ['yii\web\JqueryAsset'];
    public $jsOptions = ['defer' => 'defer'];
}
