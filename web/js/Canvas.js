class Canvas {
    constructor(selectorID = 'canvas') {
        if (!window.Canvas) {
            this.container = document.getElementById(selectorID);
            this.ctx = this.container.getContext('2d');
            this.container.width = this.Width = this.container.clientWidth;
            this.container.height = this.Height = this.container.clientHeight;
        } else {
            this.container = window.Canvas.container;
            this.ctx = window.Canvas.ctx;
        }
    }
    static getWidth() { return window.Canvas.Width; }
    static getHeight() { return window.Canvas.Height; }
}

class Point extends Canvas {
    constructor(x = null, y = null, size = 3) {
        super();
        this.X = x ? parseInt(x) : 0;
        this.Y = y ? parseInt(y) : 0;
        if (size) {
            this.ctx.beginPath();
            this.ctx.arc(this.X, this.Y, size, 0, 2 * Math.PI);
            this.ctx.stroke();
        }
    }
    static getRandomPoint(size = 3) {
        return new Point(
            Math.round(Math.random() * parseInt(Canvas.getWidth())),
            Math.round(Math.random() * parseInt(Canvas.getHeight())),
            size
        );
    };
}

class Line extends Canvas {
    constructor(pointA, pointB) {
        super();
        this.pointA = pointA;
        this.pointB = pointB;
        this.ctx.moveTo(pointA.X, pointA.Y);
        this.ctx.lineTo(pointB.X, pointB.Y);
        this.ctx.stroke();
    }
}

class Rectangle extends Canvas {
    constructor(pointA, pointB) {
        super();
        if (pointA.X < pointB.X) {
            this.left = new Line(pointA, new Point(pointA.X, pointB.Y));
            this.right = new Line(pointB, new Point(pointB.X, pointA.Y));
        } else {
            this.left = new Line(pointB, new Point(pointB.X, pointA.Y));
            this.right = new Line(pointA, new Point(pointA.X, pointB.Y));
        }
        if (pointA.Y < pointB.Y) {
            this.top = new Line(pointA, new Point(pointB.X, pointA.Y));
            this.bottom = new Line(pointB, new Point(pointA.X, pointB.Y));
        } else {
            this.top = new Line(pointB, new Point(pointA.X, pointB.Y));
            this.bottom = new Line(pointA, new Point(pointB.X, pointA.Y));
        }
    }
}



(() => { document.addEventListener('DOMContentLoaded', () => {
    window.Canvas = new Canvas();
    window.Canvas.container.onmouseover = (e) => {
        console.log(e.clientX, e.clientY);
        let point = new Point(e.offsetX, e.offsetY, 5);
        window.Canvas.container.onmousemove = (e) => {
            //fuck
        };
    };
    // let pointA = Point.getRandomPoint();
    // let pointB = Point.getRandomPoint();
    // let pointC = Point.getRandomPoint();
    // let pointD = Point.getRandomPoint();
    // let rectangleX = new Rectangle(pointA, pointB);
    // let rectangleZ = new Rectangle(pointC, pointD);
}) })();