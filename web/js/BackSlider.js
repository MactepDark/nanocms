let BackSlider = {
    onload: document.addEventListener('DOMContentLoaded', function() { BackSlider.init(); }),
    $backSlider: undefined, slides: [], pointer: 0, delay: 10000,
    init: function() {
        this.$backSlider = $('div.back-slider');
        let images = [];
        $.each(this.$backSlider.find('div[data-src]'), function(k, v) { BackSlider.slides.push(v); });
        const lazyLoad = (src) => {
            let img = document.createElement('img');
            img.onload = () => {
                images.push(img);
                if (this.slides.length > 0) {
                    lazyLoad(this.slides.shift().dataset.src);
                } else { this.slides = images; this.run(); }
            };
            img.src = src;
        };
        // this.slides.length > 0 ? lazyLoad(this.slides.shift().dataset.src) : null;
    },
    run: function() {
        this.slides.length > 0 ? setInterval(() => { this.next(); }, this.delay) : console.warn('No images!');
    },
    next: function () {
        this.pointer < this.slides.length - 1 ? this.pointer++ : this.pointer = 0;
        this.apply();
    },
    prev: function () {
        this.pointer > 0 ? this.pointer-- : this.pointer = this.slides.length - 1;
        this.apply();
    },
    apply: function () {
        this.$backSlider.css('background', `url(${this.slides[this.pointer].src}) no-repeat`);
    },
};