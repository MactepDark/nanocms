var Admin = {
    ID: undefined, searchQuery: {},
    $pageContainer: undefined, $pageForm: undefined, $modelField: undefined,
    onload: document.addEventListener('DOMContentLoaded', function() { Admin.init(); }),
    init: function() {
        toastr.options.timeOut = 1234;
        toastr.options.positionClass = 'toast-top-center';
        toastr.options.showMethod = 'slideDown';
        toastr.options.hideMethod = 'slideUp';
        Admin.ID = localStorage.getItem('userID');
        if (Admin.ID) {
            setTimeout(function() {
                Admin.ajax.fetchUsersList(function() {
                    Admin.ajax.fetchUserForm(function() {
                        localStorage.removeItem('userID');
                    });
                });
            }, 555);
        }
        if (this.map.container) { this.map.init(); this.map.markerBind(); }
        this.inputMaskInit();
        this.bind();
    },
    inputMaskInit: function() {
        // $('input.phone').inputmask({alias: 'phone'}).on('keydown', function(e) {
        //     if ($(this).inputmask('isComplete')) { e.preventDefault(); return false; }
        // });
        // $('input.email').inputmask({alias: 'email'});
    },
    map: {
        container: document.getElementById('map'), obj: undefined, marker: undefined,
        options: {center: {lat: 0, lng: 0}, scrollWheel: true, zoom: 0},
        init: function() {
            if (typeof google !== 'undefined') {
                this.options.center.lat = parseFloat(this.container.getAttribute('data-lat'));
                this.options.center.lng = parseFloat(this.container.getAttribute('data-lng'));
                this.options.zoom = parseInt(this.container.getAttribute('data-zoom'));
                this.obj = new google.maps.Map(this.container, this.options);
                this.marker = new google.maps.Marker({position: this.options.center, map: this.obj});
            }
        },
        markerBind: function() {
            if (this.obj) {
                this.obj.addListener('click', function(e) {
                    if (Admin.map.marker) { Admin.map.marker.setMap(null); }
                    Admin.map.marker = new google.maps.Marker({position: e.latLng, map: Admin.map.obj});
                    Admin.ajax.saveMapMarker(e.latLng.lat(), e.latLng.lng(), Admin.map.obj.zoom);
                });
            }
        }
    },
    bind: function() {
        this.$pageContainer = $('div#page-container').on('click', 'tr.language[data-id]', function() {
            Admin.$pageContainer.find('tr.language.selected').removeClass('selected');
            Admin.ID = $(this).addClass('selected').data('id');
            Admin.$pageForm.fadeOut('fast', function() {
                Admin.ajax.fetchLanguageForm();
            });
        }).on('click', 'button#add-language', function(e) {
            e.preventDefault();
            Admin.ajax.addLanguage();
        }).on('input', '[name][data-id][data-model]', function() {
            Admin.$modelField = $(this).addClass('has-changes');
        }).on('blur', '[name][data-id][data-model].has-changes', function() {
            Admin.$modelField = $(this).removeClass('has-changes').prop('readonly', true);
            Admin.ajax.saveModelField();
        }).on('click', 'button.set-language-default', function(e) {
            e.preventDefault();
            Admin.ajax.setLanguageDefault();
        }).on('click', 'button.set-language-source', function(e) {
            e.preventDefault();
            Admin.ajax.setLanguageSource();
        }).on('click', 'button.remove-language', function(e) {
            e.preventDefault();
            if (confirm('A you sure?')) { Admin.ajax.removeLanguage(); }
        }).on('click', 'button#refresh-translations', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            Admin.ajax.refreshTranslations();
        }).on('click', 'ul.translation-sections > li > a', function(e) {
            e.preventDefault();
            Admin.$pageContainer.find('ul.translation-sections > li.active').removeClass('active');
            Admin.$pageContainer.find('div.translation').hide();
            $(this).parent().addClass('active');
            Admin.$pageContainer.find('div.translation[data-id="' + $(this).data('section') + '"]').fadeIn('fast');
        }).on('input', 'tr.translation[data-id] input[data-lang]', function() {
            $(this).addClass('has-changes');
        }).on('blur', 'tr.translation[data-id] input[data-lang].has-changes', function() {
            var messageID = $(this).prop('readonly', true).closest('tr.translation').data('id');
            Admin.ajax.saveTranslation(messageID, $(this));
        }).on('click', 'button.add-contact', function(e) {
            e.preventDefault();
            Admin.ajax.addContact($(this).prop('disabled', true).data('type'));
        }).on('click', 'button.add-social', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            Admin.ajax.addContact($('select#social-type').prop('disabled', true).val());
        }).on('change', 'select#social-type', function() {
            $(this).closest('div.input-group').find('button.add-social').prop('disabled', false);
        }).on('mousedown', 'tr.contact input[type="radio"]', function() {
            if (!$(this).prop('checked')) {
                Admin.ajax.selectPrimaryContact($(this).closest('tr.contact').data('id'));
            }
        }).on('click', 'tr.contact input[type="checkbox"]', function() {
            Admin.ajax.changeSelectedContact($(this).closest('tr.contact').data('id'), $(this).prop('checked') ? 1 : 0);
        }).on('click', 'tr.contact button.raise-priority', function(e) {
            e.preventDefault();
            Admin.ajax.raiseContactPriority($(this).prop('disabled', true).closest('tr.contact').data('id'));
        }).on('click', 'tr.contact button.lower-priority', function(e) {
            e.preventDefault();
            Admin.ajax.lowerContactPriority($(this).prop('disabled', true).closest('tr.contact').data('id'));
        }).on('click', 'tr.contact button.remove-contact', function(e) {
            e.preventDefault();
            if (confirm('A you sure?')) {
                Admin.ajax.removeContact($(this).prop('disabled', true).closest('tr.contact').data('id'));
            }
        }).on('click', 'tr.user', function() {
            Admin.$pageContainer.find('tr.user.selected').removeClass('selected');
            Admin.ID = $(this).addClass('selected').data('id');
            Admin.ajax.fetchUserForm();
        }).on('click', 'button#send-invite', function(e) {
            e.preventDefault();
            let email = prompt('Enter e-mail address');
            if (email) {
                $(this).prop('disabled', true);
                Admin.ajax.sendUserInvite(email);
            }
        }).on('click', 'button#remove-user', function(e) {
            e.preventDefault();
            if (confirm('A you sure?')) {
                Admin.ajax.removeUser();
            }
        }).on('click', 'button#set-user-active', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            Admin.ajax.setUserActive();
        }).on('click', 'button#set-user-inactive', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            Admin.ajax.setUserInactive();
        }).on('input', 'input[type="password"]', function() {
            let pwdLength = $('input[name="password"]').val().length > 0 && $('input[name="password-confirm"]').val().length > 0;
            $('button#change-password').prop('disabled', !pwdLength);
        }).on('click', 'button#change-password', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            $('input[type="password"]').prop('readonly', true);
            Admin.ajax.changeUserPassword();
        }).on('click', 'tr.lead-item[data-id]', function() {
            Admin.$pageContainer.find('tr.lead-item.selected').removeClass('selected');
            Admin.ID = $(this).addClass('selected').data('id');
            Admin.$pageForm.fadeOut('fast', function() { Admin.ajax.fetchLeadForm(); });
        }).on('click', 'button#leads-refresh', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            Admin.ajax.fetchLeadsList(function() {
                $('button#leads-refresh').prop('disabled', false);
            });
        }).on('click', 'button#set-lead-view', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            Admin.ajax.setLeadViewed();
        }).on('click', 'button#send-notify', function(e) {
            e.preventDefault();
            Admin.ajax.sendLeadNotify($(this).prop('disabled', true));
        }).on('click', 'button#remove-lead', function(e) {
            e.preventDefault();
            if (confirm('A you sure?')) { Admin.ajax.removeLead(); }
        }).on('click', 'button#fetch-geo', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            Admin.ajax.fetchLeadGeo();
        }).on('input', 'input#leads-search-query', function() {
            $('button#search').prop('disabled', $(this).val().length === 0);
        }).on('keydown', 'input#leads-search-query', function(e) {
            switch (e.keyCode) {
                case 13: $('button#search').trigger('click'); break;
                case 27: $('button#clear').trigger('click'); break;
                default: break;
            }
        }).on('click', 'button#search', function(e) {
            e.preventDefault();
            Admin.searchQuery.query = $('input#leads-search-query').val().trim();
            Admin.ajax.fetchLeadsList();
        }).on('click', 'button#clear', function(e) {
            e.preventDefault();
            Admin.searchQuery.query = '';
            Admin.ajax.fetchLeadsList();
        }).on('change', 'select#leads-search-type', function() {
            Admin.searchQuery.type = parseInt($(this).val());
            Admin.ajax.fetchLeadsList();
        }).on('change', 'select#leads-search-status', function() {
            Admin.searchQuery.status = parseInt($(this).val());
            Admin.ajax.fetchLeadsList();
        });
        this.$pageForm = $('div#page-form');
        $(document).on('mouseenter mouseleave', '[data-hover]', function() {
            var text = $(this).data('hover');
            $(this).data('hover', $(this).text().trim());
            $(this).html(text);
        }).on('click', 'a#current-user', function(e) {
            e.preventDefault();
            localStorage.setItem('userID', $(this).data('id'));
            location.href = $(this).data('href');
        }).on('click', 'a#site-preview', function(e) {
            e.preventDefault();
            if ($(this).parent().hasClass('active')) {
                $(this).css({color: 'silver'}).parent().removeClass('active');
                Admin.$pageContainer.parent().css('background-color', 'white').find('iframe').fadeOut('fast', function() {
                    $(this).remove();
                    Admin.$pageContainer.fadeIn('fast', function() {});
                });
            } else {
                let $iFrame = $('<iframe frameborder="0">');
                $(this).css({color: 'cyan'}).parent().addClass('active');
                Admin.$pageContainer.parent().css('background-color', '#ddd').append($iFrame.prop('src', $(this).data('href')));
                Admin.$pageContainer.fadeOut('fast', function() { $iFrame.fadeIn('fast', function() {}); });
            }
        }).on('click', 'div.page-limits > button', function(e) {
            e.preventDefault();
            Admin.searchQuery.pageLimit = parseInt($(this).prop('disabled', true).text());
            Admin.ajax.fetchLeadsList();
        }).on('click', 'button.prev-page', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            Admin.searchQuery.currentPage--;
            Admin.ajax.fetchLeadsList();
        }).on('click', 'button.next-page', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            Admin.searchQuery.currentPage++;
            Admin.ajax.fetchLeadsList();
        }).on('click', 'button.page', function(e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            Admin.searchQuery.currentPage = parseInt($(this).text());
            Admin.ajax.fetchLeadsList();
        });
    },
    callbacks: {
        afterModelSave: undefined
    },
    ajax: {
        obj: {}, url: undefined, send: function(success, failure) {
            $.post(this.url, this.obj, function(answer) {
                Admin.ajax.obj = {};
                answer.result && typeof success === 'function' ? success(answer) :
                    !answer.result && typeof failure === 'function' ? failure(answer) : null;
            }, 'JSON');
        },
        fetchLanguages: function(callback) {
            this.obj.method = 'fetchLanguages';
            this.send(function(answer) {
                Admin.$pageContainer.html(answer.html).fadeIn('fast', function() {
                    if (Admin.ID) {
                        Admin.$pageContainer.find('tr.language[data-id="' + Admin.ID + '"]').addClass('selected');
                    }
                    Admin.$pageForm = $('div#page-form');
                    typeof callback === 'function' ? callback(answer) : null;
                });
            });
        },
        fetchLanguageForm: function(callback) {
            this.obj.method = 'fetchLanguageForm';
            this.obj.langID = Admin.ID;
            this.send(function(answer) {
                Admin.$pageForm.html(answer.html).fadeIn('fast', function() {
                    typeof callback === 'function' ? callback() : null;
                });
            });
        },
        fetchContactList: function(callback) {
            this.obj.method = 'fetchContactList';
            this.send(function(answer) {
                Admin.$pageContainer.find('div#contact-list').html(answer.html).fadeIn('fast', function() {
                    Admin.inputMaskInit();
                    typeof callback === 'function' ? callback() : null;
                });
            });
        },
        fetchUsersList: function(callback) {
            this.obj.method = 'fetchUsersList';
            this.send(function(answer) {
                Admin.$pageContainer.find('div#users-list').html(answer.html).fadeIn('fast', function() {
                    if (Admin.ID) {
                        Admin.$pageContainer.find('tr.user[data-id="' + Admin.ID + '"]').addClass('selected');
                    }
                    typeof callback === 'function' ? callback(answer) : null;
                });
            });
        },
        fetchUserForm: function(callback) {
            this.obj.method = 'fetchUserForm';
            this.obj.userID = Admin.ID;
            this.send(function(answer) {
                Admin.$pageForm.html(answer.html).fadeIn('fast', function() {
                    typeof callback === 'function' ? callback(answer) : null;
                });
            });
        },
        fetchLeadsList: function(callback) {
            this.obj.method = 'fetchLeadsList';
            this.obj.searchQuery = Admin.searchQuery;
            this.send(function(answer) {
                Admin.$pageContainer.find('div#leads-list').html(answer.html).fadeIn('fast', function() {
                    if (Admin.ID) {
                        Admin.$pageContainer.find('tr.lead-item[data-id="' + Admin.ID + '"]').addClass('selected');
                    }
                    typeof callback === 'function' ? callback(answer) : null;
                });
            });
        },
        fetchLeadForm: function(callback) {
            this.obj.method = 'fetchLeadForm';
            this.obj.leadID = Admin.ID;
            this.send(function(answer) {
                Admin.$pageForm.html(answer.html).fadeIn('fast', function() {
                    Admin.map.container = document.getElementById('map');
                    if (Admin.map.container) { Admin.map.init(); }
                    typeof callback === 'function' ? callback(answer) : null;
                });
            });
        },
        fetchLeadGeo: function() {
            this.obj.method = 'fetchLeadGeo';
            this.obj.leadID = Admin.ID;
            this.send(function() {
                Admin.ajax.fetchLeadForm(function() {
                    toastr.info('Fetched GEO info');
                });
            });
        },
        saveModelField: function() {
            this.obj.method = 'saveModelField';
            this.obj.model = Admin.$modelField.data('model');
            this.obj.field = Admin.$modelField.prop('name');
            this.obj.ID = Admin.$modelField.data('id');
            this.obj.value = Admin.$modelField.val();
            this.send(function() {
                toastr.success('Saved successfully');
                Admin.$modelField.prop('readonly', false);
                typeof Admin.callbacks.afterModelSave === 'function' ? Admin.callbacks.afterModelSave() : null;
            }, function() {
                toastr.warning('No changes has been made');
                Admin.$modelField.prop('readonly', false);
            });
        },
        addLanguage: function() {
            this.obj.method = 'addLanguage';
            this.send(function(answer) {
                Admin.ID = answer.id;
                Admin.ajax.fetchLanguages(function() {
                    Admin.ajax.fetchLanguageForm(function() {
                        Admin.$pageForm.find('input[name="code"]').focus();
                    });
                });
            });
        },
        setLanguageDefault: function() {
            this.obj.method = 'setLanguageDefault';
            this.obj.langID = Admin.ID;
            this.send(function() {
                Admin.ajax.fetchLanguages(function() {
                    Admin.ajax.fetchLanguageForm(function() {
                        toastr.info('Default language selected');
                    });
                });
            });
        },
        setLanguageSource: function() {
            this.obj.method = 'setLanguageSource';
            this.obj.langID = Admin.ID;
            this.send(function() {
                Admin.ajax.fetchLanguages(function() {
                    Admin.ajax.fetchLanguageForm(function() {
                        toastr.info('Source language selected');
                    });
                });
            });
        },
        removeLanguage: function() {
            this.obj.method = 'removeLanguage';
            this.obj.langID = Admin.ID;
            this.send(function() {
                Admin.$pageForm.fadeOut('fast', function() {
                    Admin.ID = undefined;
                    Admin.ajax.fetchLanguages(function() {
                        toastr.warning('Language has been deleted');
                    });
                });
            });
        },
        refreshTranslations: function() {
            this.obj.method = 'refreshTranslations';
            this.send(function() {
                Admin.$pageContainer.fadeOut('fast', function() { location.reload(); });
            });
        },
        saveTranslation: function(messageID, $input) {
            this.obj.method = 'saveTranslation';
            this.obj.messageID = messageID;
            this.obj.langCode = $input.data('lang');
            this.obj.value = $input.val();
            this.send(function() {
                toastr.success('Translation saved');
                $input.removeClass('has-changes').prop('readonly', false);
            }, function() {
                toastr.warning('No changes has been made');
                $input.removeClass('has-changes').prop('readonly', false);
            });
        },
        addContact: function(typeID) {
            this.obj.method = 'addContact';
            this.obj.typeID = typeID;
            this.send(function(answer) {
                Admin.ajax.fetchContactList(function() {
                    Admin.$pageContainer.find('#contact-list [data-id="' + answer.id + '"]').focus();
                    toastr.success('New contact was added');
                });
            });
        },
        selectPrimaryContact: function(contactID) {
            this.obj.method = 'selectPrimaryContact';
            this.obj.contactID = contactID;
            this.send(function() {
                toastr.info('Contact selected as primary');
            });
        },
        changeSelectedContact: function(contactID, state) {
            this.obj.method = 'changeSelectedContact';
            this.obj.contactID = contactID;
            this.obj.state = state;
            this.send(function() {
                toastr.info(state ? 'Contact is selected' : 'Removing selection');
            });
        },
        raiseContactPriority: function(contactID) {
            this.obj.method = 'raiseContactPriority';
            this.obj.contactID = contactID;
            this.send(function() { Admin.ajax.fetchContactList(); });
        },
        lowerContactPriority: function(contactID) {
            this.obj.method = 'lowerContactPriority';
            this.obj.contactID = contactID;
            this.send(function() { Admin.ajax.fetchContactList(); });
        },
        removeContact: function(contactID) {
            this.obj.method = 'removeContact';
            this.obj.contactID = contactID;
            this.send(function() {
                Admin.ajax.fetchContactList(function() {
                    toastr.warning('Contact has been removed');
                });
            });
        },
        saveMapMarker: function(lat, lng, zoom) {
            this.obj.method = 'saveMapMarker';
            this.obj.lat = lat;
            this.obj.lng = lng;
            this.obj.zoom = zoom;
            this.send(function() {
                toastr.success('Map coordinates saved');
            });
        },
        sendUserInvite: function(email) {
            this.obj.method = 'sendUserInvite';
            this.obj.email = email;
            this.send(function() {
                Admin.ajax.fetchUsersList(function() {
                    $('button#send-invite').prop('disabled', false);
                    toastr.info('User invite has been sent');
                });
            }, function() {
                toastr.error('There an error while sending email');
                $('button#send-invite').prop('disabled', false);
            });
        },
        removeUser: function() {
            this.obj.method = 'removeUser';
            this.obj.userID = Admin.ID;
            this.send(function() {
                Admin.ID = undefined;
                Admin.$pageForm.fadeOut('fast', function() {
                    Admin.ajax.fetchUsersList(function() {
                        toastr.warning('User has been removed');
                    });
                });
            });
        },
        setUserActive: function() {
            this.obj.method = 'setUserActive';
            this.obj.userID = Admin.ID;
            this.send(function() {
                Admin.$pageForm.fadeOut('fast', function() {
                    Admin.ajax.fetchUsersList(function() {
                        Admin.ajax.fetchUserForm(function() {
                            toastr.info('User has been activated');
                        });
                    });
                });
            });
        },
        setUserInactive: function() {
            this.obj.method = 'setUserInactive';
            this.obj.userID = Admin.ID;
            this.send(function() {
                Admin.$pageForm.fadeOut('fast', function() {
                    Admin.ajax.fetchUsersList(function() {
                        Admin.ajax.fetchUserForm(function() {
                            toastr.info('User has been banned');
                        });
                    });
                });
            });
        },
        changeUserPassword: function() {
            this.obj.method = 'changeUserPassword';
            this.obj.userID = Admin.ID;
            this.obj.password = $('input[name="password"]').val();
            this.obj.confirm = $('input[name="password-confirm"]').val();
            this.send(function() {
                Admin.$pageForm.fadeOut('fast', function() {
                    Admin.ajax.fetchUserForm(function() {
                        toastr.info('Password has been changed');
                    });
                });
            }, function(answer) {
                $('button#change-password').prop('disabled', false);
                $('input[type="password"]').prop('readonly', false);
                toastr.error(answer.error);
            });
        },
        setLeadViewed: function() {
            this.obj.method = 'setLeadViewed';
            this.obj.leadID = Admin.ID;
            this.send(function() {
                Admin.ajax.fetchLeadsList(function() {
                    Admin.ajax.fetchLeadForm(function() {
                        toastr.info('Mark as viewed');
                    });
                });
            });
        },
        sendLeadNotify: function($sendBtn) {
            this.obj.method = 'sendLeadNotify';
            this.obj.leadID = Admin.ID;
            this.send(function() {
                toastr.info('Notify is send');
                $sendBtn.prop('disabled', false);
            });
        },
        removeLead: function() {
            this.obj.method = 'removeLead';
            this.obj.leadID = Admin.ID;
            this.send(function() {
                Admin.$pageForm.fadeOut('fast', function() {
                    Admin.ID = undefined;
                    Admin.ajax.fetchLeadsList(function() {
                        toastr.warning('Lead has been removed');
                    });
                });
            });
        }
    }
};