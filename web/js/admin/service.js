let Service = {
    onload: document.addEventListener('DOMContentLoaded', function() { Service.init(); }),
    Model: undefined, ID: undefined, $list: undefined, $form: undefined, $services: undefined,
    init: function() {
        this.ajax.url = Admin.ajax.url;
        Content.callbacks.afterSave = function() {
            Service.ajax.fetchItemList(function() {
                Service.ajax.fetchItemForm();
            });
        };
        Admin.callbacks.afterModelSave = function() {
            Service.ajax.fetchItemList();
        };
        this.bind();
    },
    bind: function() {
        this.$list = $('div#list');
        this.$form = $('div#form');
        $(document).on('click', 'button#create', function(e) {
            e.preventDefault();
            Service.ajax.createItem(function() {
                Service.ajax.fetchItemList(function() {
                    Service.ajax.fetchItemForm(function() {
                        Service.$form.find('input[type="text"]').focus();
                    });
                });
            });
        }).on('click', 'tr[data-id]', function() {
            $('tr[data-id].selected').removeClass('selected');
            $(this).addClass('selected');
            Service.ID = $(this).data('id');
            Service.ajax.fetchItemForm(function() {
                Service.$form.find('input[type="text"]').focus();
            });
        }).on('click', 'button.set-item-status', function(e) {
            e.preventDefault();
            Service.ajax.changeItemStatus($(this).prop('disabled', true).data('status'));
        }).on('click', 'button.raise-item-priority', function(e) {
            e.preventDefault();
            Service.ajax.raiseItemPriority($(this).prop('disabled', true));
        }).on('click', 'button.lower-item-priority', function(e) {
            e.preventDefault();
            Service.ajax.lowerItemPriority($(this).prop('disabled', true));
        }).on('click', 'button#remove-item', function(e) {
            e.preventDefault();
            if (confirm('A you sure?')) {
                Service.ajax.removeItem();
            }
        }).on('click', 'button.add-service-cloth', function(e) {
            e.preventDefault();
            Service.ajax.addService(Service.ID, $(this).prop('disabled', true).data('id'));
        }).on('click', 'button.remove-service-cloth', function(e) {
            e.preventDefault();
            Service.ajax.removeService(Service.ID, $(this).prop('disabled', true).data('id'));
        }).on('click', 'button.add-service-repair', function(e) {
            e.preventDefault();
            Service.ajax.addService($(this).prop('disabled', true).data('id'), Service.ID);
        }).on('click', 'button.remove-service-repair', function(e) {
            e.preventDefault();
            Service.ajax.removeService($(this).prop('disabled', true).data('id'), Service.ID);
        });
    },
    ajax: {
        obj: {}, url: undefined, send: function(success, failure) {
            this.obj.model = Service.Model;
            $.post(this.url, this.obj, function(answer) {
                answer.result && typeof success === 'function' ? success(answer) :
                    !answer.result && typeof failure === 'function' ? failure(answer) : null;
            }, 'JSON');
        },
        fetchItemList: function(callback) {
            this.obj.method = 'fetchItemList';
            this.send(function(answer) {
                Service.$list.html(answer.html).fadeIn('fast', function() {
                    if (Service.ID) {
                        Service.$list.find(`tr[data-id=${Service.ID}]`).addClass('selected');
                    }
                    typeof callback === 'function' ? callback() : null;
                });
            });
        },
        fetchItemForm: function(callback) {
            this.obj.method = 'fetchItemForm';
            this.obj.ID = Service.ID;
            this.send(function(answer) {
                Service.$form.html(answer.html).fadeIn('fast', function() {
                    Service.$services = $('div#services');
                    typeof callback === 'function' ? callback() : null;
                });
            });
        },
        createItem: function(callback) {
            this.obj.method = 'createItem';
            this.send(function(answer) {
                Service.ID = answer.id;
                typeof callback === 'function' ? callback() : null;
            });
        },
        changeItemStatus: function(status) {
            this.obj.method = 'changeItemStatus';
            this.obj.ID = Service.ID;
            this.obj.status = status;
            this.send(function() {
                Service.ajax.fetchItemList(function() {
                    Service.ajax.fetchItemForm();
                });
            });
        },
        raiseItemPriority: function($btn) {
            this.obj.method = 'raiseItemPriority';
            this.obj.ID = Service.ID;
            this.send(function() {
                Service.ajax.fetchItemList(function() {
                    $btn.prop('disabled', false);
                });
            }, function() { $btn.prop('disabled', false); });
        },
        lowerItemPriority: function($btn) {
            this.obj.method = 'lowerItemPriority';
            this.obj.ID = Service.ID;
            this.send(function() {
                Service.ajax.fetchItemList(function() {
                    $btn.prop('disabled', false);
                });
            }, function() { $btn.prop('disabled', false); });
        },
        removeItem: function() {
            this.obj.method = 'removeItem';
            this.obj.ID = Service.ID;
            this.send(function() {
                Service.ID = undefined;
                Service.$form.fadeOut('fast', function() {
                    Service.$list.fadeOut('fast', function() {
                        Service.ajax.fetchItemList(function() {
                            toastr.warning('Item removed');
                        });
                    });
                });
            });
        },
        getServiceClothes: function() {
            this.obj.method = 'getServiceClothes';
            this.obj.repairID = Service.ID;
            this.send(function(answer) {
                Service.$services.html(answer.html);
            });
        },
        getServiceRepairs: function() {
            this.obj.method = 'getServiceRepairs';
            this.obj.clothID = Service.ID;
            this.send(function(answer) {
                Service.$services.html(answer.html);
            });
        },
        addService: function(repairID, clothID) {
            this.obj.method = 'addService';
            this.obj.repairID = repairID;
            this.obj.clothID = clothID;
            this.send(function() {
                Service.Model === 'Repair' ? Service.ajax.getServiceClothes() : Service.ajax.getServiceRepairs();
            });
        },
        removeService: function(repairID, clothID) {
            this.obj.method = 'removeService';
            this.obj.repairID = repairID;
            this.obj.clothID = clothID;
            this.send(function() {
                Service.Model === 'Repair' ? Service.ajax.getServiceClothes() : Service.ajax.getServiceRepairs();
            });
        }
    }
};