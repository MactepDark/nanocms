// class Ajax {
//     constructor(url = '/admin/ajax') {
//         this.url = url;
//         this.obj = {method: null};
//     }
//     send(callback = null) {
//         $.post(this.url, this.obj, (answer) => {
//             answer.result && typeof this.success === 'function' ? this.success(answer) :
//                 !answer.result && typeof this.failure === 'function' ? this.failure(answer.error) : null;
//             typeof callback === 'function' ? callback(answer) : null;
//         }, 'JSON');
//     }
//     success(answer) {
//         console.log('success', answer);
//     }
//     failure(errors) {
//         console.log('failure', errors);
//     }
// }