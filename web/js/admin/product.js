let Product = {
    ID: undefined, $productList: undefined, $productForm: undefined,
    onload: document.addEventListener('DOMContentLoaded', () => { Product.bind(); }),
    bind: function() {
        this.$productList = $('div#product-list').on('click', 'button#create-product', function(e) {
            e.preventDefault();
            this.disabled = true;
            Product.createProduct(() => {
                Product.$productForm.find('input[name="name"]').focus();
                toastr.success('Продукт создан успешно', 'Product');
            });
        }).on('click', 'tr.product[data-id]', function() {
            Product.$productList.find('tr.product.selected').removeClass('selected');
            Product.ID = parseInt(this.dataset.id);
            Product.fetchProductForm(() => { this.classList.add('selected'); });
        });
        this.$productForm = $('div#product-form').on('input', 'input[name]', function() {
            document.querySelector('button.save-product').disabled = this.value.length === 0;
        }).on('click', 'button.save-product', function(e) {
            e.preventDefault();
            this.disabled = true;
            let content = {};
            $.each(Product.$productForm.find('div.product-content input'), (k, v) => {
                content[v.dataset.type] = v.value;
            });
            Product.saveProduct(content, () => { toastr.success('Успешно сохранено', 'Product') });
        }).on('click', 'button.set-status-draft', function(e) {
            e.preventDefault();
            this.disabled = true;
            Product.setProductStatusDraft(() => { this.disabled = false; });
        }).on('click', 'button.set-status-active', function(e) {
            e.preventDefault();
            this.disabled = true;
            Product.setProductStatusActive(() => { this.disabled = false; });
        }).on('click', 'button.raise-priority', function(e) {
            e.preventDefault();
            this.disabled = true;
            Product.raiseProductPriority(() => { this.disabled = false; });
        }).on('click', 'button.lower-priority', function(e) {
            e.preventDefault();
            this.disabled = true;
            Product.lowerProductPriority(() => { this.disabled = false; });
        }).on('click', 'button.remove-product', function(e) {
            e.preventDefault();
            this.disabled = true;
            if (confirm('Вы точно уверены?')) {
                Product.removeProduct(() => {
                    toastr.warning('Продукт был удален', 'Product');
                });
            } else { this.disabled = false; }
        });
    },
    fetchProductList: function(callback = null) {
        let ajax = new Ajax();
        ajax.obj.method = 'fetchProductList';
        ajax.success = (answer) => {
            Product.$productList.html(answer.html).fadeIn('fast', callback);
            if (Product.ID) {
                Product.$productList.find(`tr.product[data-id="${Product.ID}"]`).addClass('selected');
            }
        };
        ajax.send();
    },
    fetchProductForm: function(callback = null) {
        let ajax = new Ajax();
        ajax.obj.method = 'fetchProductForm';
        ajax.obj.productID = parseInt(Product.ID);
        ajax.success = (answer) => {
            Product.$productForm.html(answer.html).fadeIn('fast', callback);
        };
        ajax.send();
    },
    createProduct: function(callback = null) {
        let ajax = new Ajax();
        ajax.obj.method = 'createProduct';
        ajax.success = (answer) => {
            Product.ID = parseInt(answer.id);
            Product.$productForm.fadeOut('fast', () => {
                Product.fetchProductList(() => {
                    Product.fetchProductForm(callback);
                });
            });
        };
        ajax.failure = (answer) => { console.log(answer); };
        ajax.send();
    },
    saveProduct: function(content, callback = null) {
        let ajax = new Ajax();
        ajax.obj.method = 'saveProduct';
        ajax.obj.productID = parseInt(Product.ID);
        ajax.obj.data = Product.$productForm.find('form').serializeArray();
        ajax.obj.content = content;
        ajax.success = () => {
            Product.$productForm.fadeOut('fast', () => {
                Product.fetchProductList(() => {
                    Product.fetchProductForm(callback);
                });
            });
        };
        ajax.failure = (answer) => {
            $.each(answer.error, (k, v) => { toastr.error(v, 'Product'); });
            document.querySelector('button.save-product').disabled = false;
        };
        ajax.send();
    },
    setProductStatusDraft: function(callback = null) {
        let ajax = new Ajax();
        ajax.obj.method = 'setProductStatusDraft';
        ajax.obj.productID = parseInt(Product.ID);
        ajax.success = () => {
            Product.$productList.fadeOut('fast', () => {
                Product.fetchProductForm(() => {
                    Product.fetchProductList(callback);
                });
            });
        };
        ajax.send();
    },
    setProductStatusActive: function(callback = null) {
        let ajax = new Ajax();
        ajax.obj.method = 'setProductStatusActive';
        ajax.obj.productID = parseInt(Product.ID);
        ajax.success = () => {
            Product.$productList.fadeOut('fast', () => {
                Product.fetchProductForm(() => {
                    Product.fetchProductList(callback);
                });
            });
        };
        ajax.send();
    },
    raiseProductPriority: function(callback = null) {
        let ajax = new Ajax();
        ajax.obj.method = 'raiseProductPriority';
        ajax.obj.productID = parseInt(Product.ID);
        ajax.success = () => {
            Product.$productList.fadeOut('fast', () => {
                Product.fetchProductList(callback);
            });
        };
        ajax.send();
    },
    lowerProductPriority: function (callback = null) {
        let ajax = new Ajax();
        ajax.obj.method = 'lowerProductPriority';
        ajax.obj.productID = parseInt(Product.ID);
        ajax.success = () => {
            Product.$productList.fadeOut('fast', () => {
                Product.fetchProductList(callback);
            });
        };
        ajax.send();
    },
    removeProduct: function(callback = null) {
        let ajax = new Ajax();
        ajax.obj.method = 'removeProduct';
        ajax.obj.productID = parseInt(Product.ID);
        ajax.success = () => {
            Product.ID = undefined;
            Product.$productForm.fadeOut('fast', () => {
                Product.$productList.fadeOut('fast', () => {
                    Product.fetchProductList(callback);
                });
            });
        };
        ajax.send();
    }
};