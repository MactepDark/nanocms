let Form = {
    onload: document.addEventListener('DOMContentLoaded', function() { Form.bind(); }),
    bind: function() {
        // $('input[name="phone"]').inputmask({alias: 'phone', showMaskOnHover: false});
        $(document).on('click', 'form button[type="submit"]', function(e) {
            e.preventDefault();
            this.disabled = true;
            let form = this.closest('form[data-type]');
            $.post(document.querySelector('meta[name="ajaxUrl"]').content, {
                method: 'formSubmit',
                type: parseInt(form.dataset.type),
                data: $(form).serializeArray()
            }, (answer) => {
                if (answer.result) {
                    form.reset();
                    if (form.closest('div.modal')) { modal(form.closest('div.modal').id); }
                    modal('thq-modal');
                } else {
                    $.each(answer.error, (k, v) => { $(form).find(`[name=${k}]`).addClass('has-error').val(v); });
                }
                this.disabled = false;
            });
        }).on('focus', 'form [name].has-error', function() {
            this.classList.remove('has-error');
            this.value = '';
        }).on('click', '.btn-close-modal', function() {
            modal(this.closest('div[id]').id);
        }).on('click', 'section#services button.btn-type-3', function() {
            document.querySelector('div#order-modal-extended input[type="hidden"]').value = this.closest('div.item').querySelector('div.title').innerText;
            modal('order-modal-extended');
        }).on('click', 'section#examples button.btn-type-2', function() {
            document.querySelector('div#order-modal input[type="hidden"]').value = parseInt(this.dataset.id);
            modal('order-modal');
        });
    }
};