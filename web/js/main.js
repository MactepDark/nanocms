$(document).ready(function () {

    $('.hamburger').on('click', function () {
        $('.hamburger').toggleClass('is-active');
        $('.mobile-nav').toggleClass('open');
        $('body').toggleClass('no-scroll');
    });

    const checkTop = () => {
        let height = $(window).scrollTop();
        if (height > 0) {
            $('.header').addClass('active')
        } else $('.header').removeClass('active')
    };

    $(window).scroll(function () {
        checkTop()
    });

    checkTop();


    $('.scroll-to').on("click", function (event) {
        event.preventDefault();
        var id  = $(this).attr('data-attr'),
            top = $(id).offset().top-30;

        if ($(window).width() >= '1079') {
            $('body,html').animate({scrollTop: top}, 800);
        }
        else {
            $('.hamburger').toggleClass('is-active');
            $('.mobile-nav').toggleClass('open');
            $('body').toggleClass('no-scroll');
            setTimeout(function () {
                $('body,html').animate({scrollTop: top}, 800);
            }, 300)
        }

    });

    const slider = $('.slider');

    slider.slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]
    });

    $('.slick-prev').click(function () {
        $('.slider').slick('slickPrev');
    });

    $('.slick-next').click(function () {
        $('.slider').slick('slickNext');
    });

    slider.on('afterChange', function () {
        let currentSlideIndex = parseInt($('.slick-active').attr('data-slick-index'), 0) + 1;
        $('#currentSlide').html(currentSlideIndex);
    });

    $('#totalSlides').html($('.slick-slide').not('.slick-cloned').length);


    $("#accordion").accordion({
        heightStyle: "content"
    });

    window.mdc.autoInit();
});

const modal = (id) => {
    $('#' + id).toggleClass('active');
    $('body').toggleClass('no-scroll');
};

function initGoogleMap(lat, lng, zoom = 17) {
    new google.maps.Marker({
        position: {lat: parseFloat(lat), lng: parseFloat(lng)},
        map: new google.maps.Map(document.getElementById('map'), {
            zoom: zoom, center: {lat: parseFloat(lat), lng: parseFloat(lng)}
        })
    });
}

