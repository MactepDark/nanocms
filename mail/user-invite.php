<?php
/**
 * @var $this yii\web\View
 * @var $username string
 * @var $password string
 * @var $link string
 */
?>
Hello, <strong><?= $username ?></strong>!<br/><br/>
You have been invited to registration<br/>
Please, click <a href="<?= $link ?>">here</a> to complete registration!<br/>
<strong>
    If link above does not work, please copy link manually, and paste in browser address bar:
</strong>
<div style="background-color: #eee; margin: 5px; padding: 5px; border: 1px dotted silver;">
    <?= $link ?>
</div>
Your randomly generated password is <code><?= $password ?></code>
<hr/>
