<?php
/**
 * @var $this yii\web\View
 * @var $form object
 * @var $content object
 */
use app\models\Form;
use app\models\Product;
use yii\helpers\Html;
?>
<h3><?= Form::getTypeNames($form->type) ?></h3>
<hr/>
<ul>
    <?php foreach ($content as $key => $value): ?>
    <li>
        <?= Form::getFieldNames($key) ?>:
        <strong>
            <?php if($key == Form::FIELD_PHONE): ?>
                <?= Html::a($value, implode(':', ['tel', Form::cleanNumber($value)])) ?>
            <?php elseif($key == Form::FIELD_PRODUCT): ?>
                <?php if($product = Product::findOne($value)): ?>
                    <?= $product->name ?>
                <?php else: ?>
                    N/A
                <?php endif; ?>
            <?php else: ?>
                <?= $value ?>
            <?php endif; ?>
        </strong>
    </li>
    <?php endforeach; ?>
</ul>