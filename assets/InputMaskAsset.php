<?php namespace app\assets;

use yii\web\AssetBundle;

class InputMaskAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [
        '/js/inputMask/jquery.inputmask.bundle.min.js',
        '/js/inputMask/phone.min.js',
        '/js/inputMask/inputmask.extensions.min.js'
    ];
    public $depends = ['yii\web\JqueryAsset'];
    public $jsOptions = ['defer' => 'defer'];
}
