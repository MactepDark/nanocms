<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ToastrAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = ['/css/admin/toastr.min.css'];
    public $js = ['/js/admin/toastr.min.js'];
    public $depends = ['yii\web\JqueryAsset'];
    public $jsOptions = ['defer' => 'defer'];
}
