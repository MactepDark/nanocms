<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle {

    function __construct() {
        if (!Yii::$app->user->isGuest) { $this->js[] = '/js/admin/admin.js'; }
        parent::__construct();
    }

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = ['/css/admin/admin.css'];
    public $js = ['/js/admin/common.js'];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\ToastrAsset',
        'app\assets\InputMaskAsset'
    ];
}
