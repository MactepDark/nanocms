<?php namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/base-reset.css',
        '/css/slick.css',
        '//unpkg.com/material-components-web@v4.0.0/dist/material-components-web.min.css',
        '//fonts.googleapis.com/icon?family=Material+Icons',
        '/css/pages/main.css'
    ];
    public $js = [
        '//code.jquery.com/ui/1.12.1/jquery-ui.js',
        '/js/slick.min.js',
        '/js/main.js',
        '/js/Form.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\InputMaskAsset'
    ];
}
