<?php
/**
 * @var $this yii\web\View
 * @var $searchQuery array
 * @var $leads array
 */
?>
<div class="panel panel-primary full-screen">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-flash"></span>
            <?= $this->title ?>
        </h4>
    </div>
    <div class="panel-body" style="padding: 15px;">
        <div class="row">
            <div class="col-xs-7" id="leads-list">
                <?= $this->render('leads-list', ['searchQuery' => $searchQuery, 'leads' => $leads]) ?>
            </div>
            <div class="col-xs-5" id="page-form"></div>
        </div>
    </div>
</div>