<?php
/**
 * @var $this yii\web\View
 */
?>
<div class="panel panel-danger full-screen">
    <div class="panel-heading">
        <button class="btn btn-xs btn-default pull-right" id="send-invite">
            <span class="glyphicon glyphicon-send"></span>
            <?= Yii::t('admin', 'Выслать приглашение') ?>
        </button>
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-user"></span>
            <?= $this->title ?>
        </h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-7" id="users-list">
                <?= $this->render('users-list') ?>
            </div>
            <div class="col-xs-5" id="page-form"></div>
        </div>
    </div>
</div>