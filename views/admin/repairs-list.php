<?php
/**
 * @var $this yii\web\View
 * @var $repairs array
 */
use app\modules\content\models\Content;
use app\models\Repair;
?>
<table class="table table-condensed table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th><?= Yii::t('admin', 'Название') ?></th>
        <th><?= Yii::t('admin', 'Базовая цена') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($repairs as $repair): ?>
    <tr data-id="<?= $repair['id'] ?>">
        <td style="width: 50px; text-align: center; background-color: #eee;">
            <div class="label label-primary"><?= $repair['priority'] ?></div>
        </td>
        <td>
            <div class="pull-right">
                <?= Repair::$statusIcons[$repair['status']] ?>
            </div>
            <?= Content::get(CONTENT_REPAIR_NAME, $repair['id']) ?>
        </td>
        <td style="width: 100px; text-align: center; font-weight: bold;">
            <span class="badge alert-success"><?= $repair['price'] ?></span>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>