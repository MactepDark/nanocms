<?php
/**
 * @var $this yii\web\View
 * @var $lat string
 * @var $lng string
 * @var $zoom string
 */
use app\modules\content\BaseContent;
?>
<div class="panel panel-info full-screen">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-info-sign"></span>
            <?= $this->title ?>
        </h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="row">
                <div class="col-xs-6">
                    <div class="row form-group">
                        <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Страна') ?>:</label>
                        <div class="col-xs-10">
                            <?= BaseContent::contentField(CONTENT_CONTACT_COUNTRY) ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Город') ?>:</label>
                        <div class="col-xs-10">
                            <?= BaseContent::contentField(CONTENT_CONTACT_CITY) ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Адрес') ?>:</label>
                        <div class="col-xs-10">
                            <?= BaseContent::contentField(CONTENT_CONTACT_ADDRESS) ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-xs-2 control-label">G-Maps API key:</label>
                        <div class="col-xs-10">
                            <?= BaseContent::contentField(CONTENT_CONTACT_API_KEY, BaseContent::OBJ_NOT_SET, BaseContent::FIELD_INPUT, BaseContent::LANG_NOT_SET) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div id="map" data-lat="<?= $lat ?>" data-lng="<?= $lng ?>" data-zoom="<?= $zoom ?>"></div>
                </div>
            </div>
            <hr width="99%" size="1" noshade />
            <div id="contact-list">
                <?= $this->render('contact-list') ?>
            </div>
        </form>
    </div>
</div>