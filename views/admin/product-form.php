<?php
/**
 * @var $this yii\web\View
 * @var $product object
 */
use app\modules\media\Media;
use app\models\Product;
use app\modules\content\BaseContent;
use app\modules\content\models\Content;
?>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-edit"></span>
            <?= $product->name ?>
        </h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="row form-group">
                <label class="col-xs-3 control-label">Название:</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" name="name" value="<?= $product->name ?>" />
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-5 control-label">Цена аукциона:</label>
                <div class="col-xs-7">
                    <input type="number" class="form-control" name="auctionPrice" min="0" max="999999" value="<?= $product->auctionPrice ?>" />
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-5 control-label">Цена на рынке:</label>
                <div class="col-xs-7">
                    <input type="number" class="form-control" name="marketPrice" min="0" max="999999" value="<?= $product->marketPrice ?>" />
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-xs-6">
                    <?php foreach (Product::$productExpensive as $contentTypeID => $expensiveName): ?>
                        <div class="row form-group product-content">
                            <label class="col-xs-8 control-label input-sm"><?= $expensiveName ?>:</label>
                            <div class="col-xs-4">
                                <input type="number" class="form-control input-sm" data-type="<?= $contentTypeID ?>" min="0" max="99999"
                                       value="<?= Content::get($contentTypeID, $product->id, BaseContent::LANG_NOT_SET) ?>" />
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="col-xs-6">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <?= Media::imageField(TYPE_PRODUCT_IMAGE, $product->id) ?>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-footer" style="padding: 5px;">
        <button class="btn btn-sm btn-danger pull-right remove-product">
            <span class="glyphicon glyphicon-remove"></span>
        </button>
        <?php if($product->status == Product::STATUS_DRAFT): ?>
            <button class="btn btn-sm btn-default set-status-active" title="<?= Product::$statusNames[Product::STATUS_ACTIVE] ?>">
                <span class="glyphicon glyphicon-ok" style="color: green;"></span>
            </button>
        <?php elseif ($product->status == Product::STATUS_ACTIVE): ?>
            <button class="btn btn-sm btn-default set-status-draft" title="<?= Product::$statusNames[Product::STATUS_DRAFT] ?>">
                <span class="glyphicon glyphicon-unchecked" style="color: grey;"></span>
            </button>
        <?php endif; ?>
        <div class="btn-group">
            <button class="btn btn-sm btn-default raise-priority">
                <span class="glyphicon glyphicon-arrow-up" style="color: green;"></span>
            </button>
            <button class="btn btn-sm btn-default lower-priority">
                <span class="glyphicon glyphicon-arrow-down" style="color: red;"></span>
            </button>
        </div>
        <button class="btn btn-sm btn-success save-product">
            <span class="glyphicon glyphicon-floppy-disk"></span>
            Сохранить
        </button>
    </div>
</div>
