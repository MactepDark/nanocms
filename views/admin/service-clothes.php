<?php
/**
 * @var $this yii\web\View
 * @var $repairID integer
 */

use app\models\Service;
use app\modules\content\models\Content;

?>
<div class="row">
    <div class="col-xs-6">
        <ul style="list-style-type: none;">
            <?php foreach (Service::getSelectedClothes($repairID) as $selectedCloth): ?>
            <li style="margin-bottom: 4px;">
                <div class="input-group">
                    <input type="text" class="form-control input-sm" readonly style="width: calc(100% - 128px);"
                           value="<?= Content::get(CONTENT_CLOTH_NAME, $selectedCloth['cloth_id']) ?>" />
                    <input type="number" class="form-control input-sm" name="price_min" data-model="Service" data-id="<?= $selectedCloth['id'] ?>"
                           value="<?= $selectedCloth['price_min'] ?>" min="5" max="99999" placeholder="Цена от..." style="width: 64px;" />
                    <input type="number" class="form-control input-sm" name="price_max" data-model="Service" data-id="<?= $selectedCloth['id'] ?>"
                           value="<?= $selectedCloth['price_max'] ?>" min="5" max="99999" placeholder="Цена до..." style="width: 64px;" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-secondary remove-service-cloth" data-id="<?= $selectedCloth['cloth_id'] ?>">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </span>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-xs-6">
        <?php foreach (Service::getAvailableClothes($repairID) as $availableClothID): ?>
            <button class="btn btn-sm btn-default btn-block add-service-cloth" data-id="<?= $availableClothID ?>">
                <span class="glyphicon glyphicon-arrow-left pull-left"></span>
                <?= Content::get(CONTENT_CLOTH_NAME, $availableClothID) ?>
            </button>
        <?php endforeach; ?>
    </div>
</div>