<?php
/**
 * @var $this yii\web\View
 */

use app\models\Language;
use app\modules\content\models\Content;
?>
<div class="panel panel-primary full-screen">
    <div class="panel-heading">
        <button class="btn btn-xs btn-success pull-right" id="add-language">
            <span class="glyphicon glyphicon-plus"></span>
            <?= Yii::t('admin', 'Добавить язык') ?>
        </button>
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-flag"></span>
            <?= Yii::t('admin', 'Управление языками') ?>
        </h4>
    </div>
    <div class="row panel-body">
        <div class="col-xs-8">
            <table class="table table-condensed table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th style="width: 100px;"><?= Yii::t('admin', 'Язык') ?></th>
                    <th style="width: 50px;"><?= Yii::t('admin', 'Код') ?></th>
                    <th colspan="2"><?= Yii::t('admin', 'Название') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (Language::find()->asArray()->all() as $language): ?>
                <tr class="language" data-id="<?= $language['id'] ?>">
                    <td style="text-align: center;">
                        <div class="label label-primary"><?= $language['view'] ?></div>
                    </td>
                    <td style="text-align: center;">
                        <span class="badge"><?= $language['code'] ?></span>
                    </td>
                    <td>
                        <?= Content::get(CONTENT_LANGUAGE_NAME, $language['id']) ?>
                    </td>
                    <td style="width: 100px; text-align: center;">
                        <?php if($language['default'] == Language::IS_DEFAULT): ?>
                            <span class="badge alert-success">
                                <span class="glyphicon glyphicon-ok"></span>
                                <?= Yii::t('admin', 'Основной') ?>
                            </span>
                        <?php endif; ?>
                        <?php if($language['source'] == Language::IS_SOURCE): ?>
                            <span class="badge alert-info">
                                <span class="glyphicon glyphicon-text-size"></span>
                                <?= Yii::t('admin', 'Исходный') ?>
                            </span>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-xs-4" id="page-form"></div>
    </div>
</div>