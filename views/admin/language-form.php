<?php
/**
 * @var $this yii\web\View
 * @var $language object
 */

use app\models\Language;
use app\modules\content\models\Content;
?>
<fieldset>
    <legend>
        <span class="glyphicon glyphicon-flag"></span>
        <strong>[<?= $language->view ?>]</strong>
        <?= Content::get(CONTENT_LANGUAGE_NAME, $language->id) ?>
    </legend>
    <form class="form-horizontal">
        <div class="row form-group">
            <label class="col-xs-4 control-label"><?= Yii::t('admin', 'Код языка') ?>:</label>
            <div class="col-xs-8">
                <input type="text" class="form-control" name="code" value="<?= $language->code ?>" data-id="<?= $language->id ?>" data-model="Language" />
            </div>
        </div>
        <div class="row form-group">
            <label class="col-xs-4 control-label"><?= Yii::t('admin', 'Отображение') ?>:</label>
            <div class="col-xs-8">
                <input type="text" class="form-control" name="view" value="<?= $language->view ?>" data-id="<?= $language->id ?>" data-model="Language" />
            </div>
        </div>
        <div class="row form-group">
            <label class="col-xs-4 control-label"><?= Yii::t('admin', 'Название') ?>:</label>
            <div class="col-xs-8">
                <?= \app\modules\content\BaseContent::contentField(CONTENT_LANGUAGE_NAME, $language->id) ?>
            </div>
        </div>
    </form>
    <div class="btn-group">
        <?php if($language->default == Language::IS_NOT): ?>
            <button class="btn btn-xs btn-success set-language-default">
                <span class="glyphicon glyphicon-ok"></span>
                <?= Yii::t('admin', 'Сделать основным') ?>
            </button>
        <?php endif; ?>
        <?php if($language->source == Language::IS_NOT): ?>
            <button class="btn btn-xs btn-info set-language-source">
                <span class="glyphicon glyphicon-font"></span>
                <?= Yii::t('admin', 'Сделать исходным') ?>
            </button>
        <?php endif; ?>
        <button class="btn btn-xs btn-danger remove-language">
            <span class="glyphicon glyphicon-remove"></span>
            <?= Yii::t('admin', 'Удалить') ?>
        </button>
    </div>
</fieldset>