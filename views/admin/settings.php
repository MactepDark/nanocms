<?php
/**
 * @var $this yii\web\View
 */
use app\modules\content\BaseContent;
?>
<div class="panel panel-danger full-screen">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-wrench"></span>
            <?= $this->title ?>
        </h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="row form-group">
                <label class="col-xs-3 control-label"><?= Yii::t('admin', 'Заголовок') ?>:</label>
                <div class="col-xs-9">
                    <?= BaseContent::contentField(CONTENT_SEO_TITLE) ?>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-3 control-label"><?= Yii::t('admin', 'Описание') ?>:</label>
                <div class="col-xs-9">
                    <?= BaseContent::contentField(CONTENT_SEO_DESCRIPTION, BaseContent::OBJ_NOT_SET, BaseContent::FIELD_TEXTAREA) ?>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-3 control-label"><?= Yii::t('admin', 'Индексация') ?>:</label>
                <div class="col-xs-9">
                    <?= BaseContent::contentField(CONTENT_SEO_INDEXATION, BaseContent::OBJ_NOT_SET, BaseContent::FIELD_CHECKBOX, BaseContent::LANG_NOT_SET) ?>
                </div>
            </div>
        </form>
    </div>
</div>