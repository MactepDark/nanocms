<?php
/**
 * @var $this yii\web\View
 */
use app\modules\content\BaseContent;
?>
<div class="panel panel-danger full-screen">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-briefcase"></span>
            <?= $this->title ?>
        </h4>
    </div>
    <div class="panel-body" style="padding: 5px;">
        <?= BaseContent::contentField(CONTENT_PRIVACY_POLICY_TEXT, BaseContent::OBJ_NOT_SET, BaseContent::FIELD_EDITOR) ?>
    </div>
</div>
