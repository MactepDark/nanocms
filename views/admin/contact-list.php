<?php
/**
 * @var $this yii\web\View
 */
use app\models\Contact;

?>
<div class="row">
    <div class="col-xs-4">
        <fieldset>
            <legend>
                <button class="btn btn-sm btn-success pull-right add-contact" data-type="<?= Contact::TYPE_PHONE ?>">
                    <span class="glyphicon glyphicon-plus"></span>
                    <?= Yii::t('admin', 'Номер телефона') ?>
                </button>
                <span class="glyphicon glyphicon-earphone"></span>
                <?= Yii::t('admin', 'Телефоны') ?>:
            </legend>
            <table class="table table-condensed table-bordered table-striped">
                <thead>
                <tr>
                    <th>№</th>
                    <th><span class="glyphicon glyphicon-ok"></span></th>
                    <th><span class="glyphicon glyphicon-check"></span></th>
                    <th><?= Yii::t('admin', 'Номер телефона') ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (Contact::getContactsByType(['type' => Contact::TYPE_PHONE]) as $phone): ?>
                    <tr class="contact" data-id="<?= $phone['id'] ?>">
                        <td style="width: 35px;">
                            <div class="label label-primary"><?= $phone['priority'] ?></div>
                        </td>
                        <td style="width: 32px;">
                            <input type="radio" name="primary-phone" <?= $phone['primary'] == Contact::PRIMARY ? 'checked' : '' ?> data-id="<?= $phone['id'] ?>" data-model="Contact" />
                        </td>
                        <td style="width: 32px;">
                            <input type="checkbox" name="selected" <?= $phone['selected'] == Contact::SELECTED ? 'checked' : '' ?> data-id="<?= $phone['id'] ?>" data-model="Contact" />
                        </td>
                        <td>
                            <input type="text" class="form-control input-sm phone" name="value" value="<?= $phone['value'] ?>" data-id="<?= $phone['id'] ?>" data-model="Contact" />
                        </td>
                        <td style="width: 80px;">
                            <div class="btn-group">
                                <button class="btn btn-xs btn-default raise-priority">
                                    <span class="glyphicon glyphicon-arrow-up" style="color: green;"></span>
                                </button>
                                <button class="btn btn-xs btn-default lower-priority">
                                    <span class="glyphicon glyphicon-arrow-down" style="color: red;"></span>
                                </button>
                                <button class="btn btn-xs btn-danger remove-contact">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </fieldset>
    </div>
    <div class="col-xs-4">
        <fieldset>
            <legend>
                <button class="btn btn-sm btn-success pull-right add-contact" data-type="<?= Contact::TYPE_EMAIL ?>">
                    <span class="glyphicon glyphicon-plus"></span>
                    <?= Yii::t('admin', 'Новый e-mail') ?>
                </button>
                <span class="glyphicon glyphicon-envelope"></span>
                E-mails:
            </legend>
            <table class="table table-condensed table-bordered table-striped">
                <thead>
                <tr>
                    <th>№</th>
                    <th><span class="glyphicon glyphicon-ok"></span></th>
                    <th><span class="glyphicon glyphicon-check"></span></th>
                    <th><?= Yii::t('admin', 'Адрес e-mail') ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (Contact::getContactsByType(['type' => Contact::TYPE_EMAIL]) as $email): ?>
                    <tr class="contact" data-id="<?= $email['id'] ?>">
                        <td style="width: 35px;">
                            <div class="label label-primary"><?= $email['priority'] ?></div>
                        </td>
                        <td style="width: 32px;">
                            <input type="radio" name="primary-email" <?= $email['primary'] == Contact::PRIMARY ? 'checked' : '' ?> data-id="<?= $email['id'] ?>" data-model="Contact" />
                        </td>
                        <td style="width: 32px;">
                            <input type="checkbox" name="selected" <?= $email['selected'] == Contact::SELECTED ? 'checked' : '' ?> data-id="<?= $email['id'] ?>" data-model="Contact" />
                        </td>
                        <td>
                            <input type="text" class="form-control input-sm email" name="value" value="<?= $email['value'] ?>" data-id="<?= $email['id'] ?>" data-model="Contact" />
                        </td>
                        <td style="width: 80px;">
                            <div class="btn-group">
                                <button class="btn btn-xs btn-default raise-priority">
                                    <span class="glyphicon glyphicon-arrow-up" style="color: green;"></span>
                                </button>
                                <button class="btn btn-xs btn-default lower-priority">
                                    <span class="glyphicon glyphicon-arrow-down" style="color: red;"></span>
                                </button>
                                <button class="btn btn-xs btn-danger remove-contact">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </fieldset>
    </div>
    <div class="col-xs-4">
        <fieldset>
            <legend>
                <div class="input-group pull-right" style="width: 50%;">
                    <select class="form-control input-sm" id="social-type">
                        <option selected disabled><?= Yii::t('admin', 'Выберите соцсеть') ?>...</option>
                        <?php foreach (Contact::$socialTypes as $typeID): ?>
                            <option value="<?= $typeID ?>"><?= Contact::getTypeNames($typeID) ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-success add-social" disabled>
                            <span class="glyphicon glyphicon-plus"></span>
                        </button>
                    </span>
                </div>
                <span class="glyphicon glyphicon-globe"></span>
                <?= Yii::t('admin', 'Соцсети') ?>:
            </legend>
            <table class="table table-condensed table-bordered table-striped">
                <thead>
                <tr>
                    <th>№</th>
                    <th><span class="glyphicon glyphicon-ok"></span></th>
                    <th><span class="glyphicon glyphicon-check"></span></th>
                    <th><?= Yii::t('admin', 'Соцсеть') ?></th>
                    <th><?= Yii::t('admin', 'Ссылка') ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach (Contact::getContactsByType(['IN', 'type', Contact::$socialTypes]) as $social): ?>
                    <tr class="contact" data-id="<?= $social['id'] ?>">
                        <td style="width: 35px;">
                            <div class="label label-primary"><?= $social['priority'] ?></div>
                        </td>
                        <td style="width: 32px;">
                            <input type="radio" name="<?= $social['type'] ?>" <?= $social['primary'] == Contact::PRIMARY ? 'checked' : '' ?> data-id="<?= $social['id'] ?>" data-model="Contact" />
                        </td>
                        <td style="width: 32px;">
                            <input type="checkbox" name="selected" <?= $social['selected'] == Contact::SELECTED ? 'checked' : '' ?> data-id="<?= $social['id'] ?>" data-model="Contact" />
                        </td>
                        <td style="width: 100px; font-size: 12px; font-weight: bold; background-color: #eee;">
                            <?= Contact::getTypeNames($social['type']) ?>
                        </td>
                        <td>
                            <input type="text" class="form-control input-sm social" name="value" value="<?= $social['value'] ?>" data-id="<?= $social['id'] ?>" data-model="Contact" />
                        </td>
                        <td style="width: 80px;">
                            <div class="btn-group">
                                <button class="btn btn-xs btn-default raise-priority">
                                    <span class="glyphicon glyphicon-arrow-up" style="color: green;"></span>
                                </button>
                                <button class="btn btn-xs btn-default lower-priority">
                                    <span class="glyphicon glyphicon-arrow-down" style="color: red;"></span>
                                </button>
                                <button class="btn btn-xs btn-danger remove-contact">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </fieldset>
    </div>
</div>