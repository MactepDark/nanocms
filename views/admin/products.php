<?php
/**
 * @var $this yii\web\View
 * @var $products array
 */
?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-lg-4" id="product-list">
        <?= $this->render('product-list', ['products' => $products]) ?>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-8" id="product-form"></div>
</div>