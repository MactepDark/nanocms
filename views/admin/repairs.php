<?php
/**
 * @var $this yii\web\View
 * @var $repairs array
 */
?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h4 class="panel-title">
            <button class="btn btn-xs btn-success pull-right" id="create">
                <span class="glyphicon glyphicon-plus"></span>
                <?= Yii::t('admin', 'Добавить') ?>
            </button>
            <span class="glyphicon glyphicon-wrench"></span>
            <?= $this->title ?>
        </h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6" id="list">
                <?= $this->render('repairs-list', ['repairs' => $repairs]) ?>
            </div>
            <div class="col-xs-6" id="form"></div>
        </div>
    </div>
</div>