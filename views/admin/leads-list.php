<?php
/**
 * @var $this yii\web\View
 * @var $searchQuery array
 * @var $leads array
 */
use app\models\Form;
?>
<div class="row" style="margin-bottom: 10px;">
    <div class="col-xs-4">
        <div class="input-group">
            <input type="text" class="form-control input-sm" id="leads-search-query" value="<?= $searchQuery['query'] ?>" placeholder="<?= Yii::t('admin', 'Поиск текстом') ?>..." />
            <div class="input-group-btn">
                <button class="btn btn-sm btn-primary" id="search" <?= strlen($searchQuery['query']) == 0 ? 'disabled' : '' ?>>
                    <span class="glyphicon glyphicon-search"></span>
                </button>
                <button class="btn btn-sm btn-default" id="clear" <?= strlen($searchQuery['query']) == 0 ? 'disabled' : '' ?>>
                    <span class="glyphicon glyphicon-remove-sign"></span>
                </button>
            </div>
        </div>
    </div>
    <div class="col-xs-3">
        <select class="form-control input-sm" id="leads-search-type">
            <option value="<?= Form::TYPE_ALL ?>"><?= Yii::t('admin', 'Все типы форм') ?></option>
            <?php foreach (Form::getTypeNames() as $typeID => $typeName): ?>
            <option value="<?= $typeID ?>" <?= $typeID == $searchQuery['type'] ? 'selected' : '' ?>>
                <?= $typeName ?>
            </option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-xs-3">
        <select class="form-control input-sm" id="leads-search-status">
            <option value="<?= Form::STATUS_ALL ?>"><?= Yii::t('admin', 'Все статусы') ?></option>
            <?php foreach (Form::getStatusNames() as $statusID => $statusName): ?>
                <option value="<?= $statusID ?>" <?= $statusID == $searchQuery['status'] ? 'selected' : '' ?>>
                    <?= $statusName ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-xs-2">
        <button class="btn btn-sm btn-primary btn-block" id="leads-refresh">
            <span class="glyphicon glyphicon-refresh"></span>
        </button>
    </div>
</div>
<table class="table table-condensed table-bordered table-striped table-hover">
    <tbody>
    <?php foreach ($leads as $lead): ?>
    <tr class="<?= $lead['status'] == Form::STATUS_NEW ? 'lead-item new' : 'lead-item' ?>" data-id="<?= $lead['id'] ?>">
        <td style="width: 50px;">
            <?= Form::$statusIcons[$lead['status']] ?>
        </td>
        <td style="width: 175px;">
            <?= Form::getTypeNames($lead['type']) ?>
        </td>
        <td>
            <?php if($content = json_decode($lead['content'])): ?>
                <strong class="pull-left">
                    <span class="glyphicon glyphicon-user"></span>
                    <?= $content->name ?>
                </strong>
                <code class="pull-right"><?= $content->phone ?></code>
            <?php endif; ?>
        </td>
        <td data-hover="<?= date('d.m.Y H:i:s', $lead['created_at']) ?>" style="width: 150px;">
            <?= Yii::$app->formatter->asRelativeTime($lead['created_at']) ?>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="row">
    <div class="col-xs-12">
        <div class="btn-group pull-left page-limits">
            <?php foreach (Form::$pageLimits as $pageLimit): ?>
            <button class="btn btn-xs <?= $searchQuery['pageLimit'] == $pageLimit ? 'btn-success' : 'btn-default' ?>">
                <?= $pageLimit ?>
            </button>
            <?php endforeach; ?>
        </div>
        <div class="btn-group pull-right">
            <button class="btn btn-xs btn-default prev-page" <?= $searchQuery['currentPage'] == 1 ? 'disabled' : '' ?>>
                <span class="glyphicon glyphicon-arrow-left"></span>
            </button>
            <?php for($p = 1; $p <= $searchQuery['totalPages']; $p++): ?>
            <button class="btn btn-xs page <?= $searchQuery['currentPage'] == $p ? 'btn-primary' : 'btn-default' ?>">
                <?= $p ?>
            </button>
            <?php endfor; ?>
            <button class="btn btn-xs btn-default next-page" <?= $searchQuery['currentPage'] == $searchQuery['totalPages'] ? 'disabled' : '' ?>>
                <span class="glyphicon glyphicon-arrow-right"></span>
            </button>
        </div>
    </div>
</div>