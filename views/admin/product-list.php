<?php
/**
 * @var $this yii\web\View
 * @var $products array
 */
use app\models\Product;
?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <button class="btn btn-xs btn-success pull-right" id="create-product">
            <span class="glyphicon glyphicon-plus"></span>
            Создать продукт
        </button>
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-star"></span>
            Список продукции
            <span class="badge"><?= count($products) ?></span>
        </h4>
    </div>
    <table class="table table-condensed table-bordered table-hover panel-body">
        <thead>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>Статус</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($products as $product): ?>
        <tr class="product" data-id="<?= $product['id'] ?>">
            <td style="width: 50px;">
                <div class="label label-primary"><?= $product['priority'] ?></div>
            </td>
            <td>
                <?= $product['name'] ?>
            </td>
            <td style="width: 100px;">
                <?= Product::$statusNames[$product['status']] ?>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<!--<pre>--><?php //print_r($products) ?><!--</pre>-->