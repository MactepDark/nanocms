<?php
/**
 * @var $this yii\web\View
 * @var $user object
 */
use app\models\User;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-user"></span>
            <?= $user->name ?>
        </h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="row form-group">
                <label class="col-xs-2 control-label"><?= Yii::t('admin', 'Имя') ?>:</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" name="name" value="<?= $user->name ?>" data-id="<?= $user->id ?>" data-model="User" />
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-2 control-label">E-mail:</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" name="email" value="<?= $user->email ?>" data-id="<?= $user->id ?>" data-model="User" />
                </div>
            </div>
            <hr/>
            <div class="row form-group">
                <label class="col-xs-12">
                    <?= Yii::t('admin', 'Для смены пароля, укажите его дважды') ?>:
                </label>
                <div class="col-xs-4">
                    <input type="password" class="form-control input-sm" name="password" autocomplete="new-password" />
                </div>
                <div class="col-xs-4">
                    <input type="password" class="form-control input-sm" name="password-confirm" autocomplete="new-password" />
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-sm btn-block btn-primary" disabled id="change-password">
                        <span class="glyphicon glyphicon-floppy-disk"></span>
                        <?= Yii::t('admin', 'Сменить') ?>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <?php if($user->id !== Yii::$app->user->id): ?>
        <div class="panel-footer" style="padding: 3px;">
            <div class="row">
                <div class="col-xs-3">
                    <?php if($user->status == User::STATUS_INACTIVE): ?>
                        <button class="btn btn-sm btn-default" id="set-user-active">
                            <?= User::$statusIcons[User::STATUS_ACTIVE] ?>
                            <?= Yii::t('admin', 'Активировать') ?>
                        </button>
                    <?php elseif ($user->status == User::STATUS_ACTIVE): ?>
                        <button class="btn btn-sm btn-default" id="set-user-inactive">
                            <?= User::$statusIcons[User::STATUS_INACTIVE] ?>
                            <?= Yii::t('admin', 'Забанить') ?>
                        </button>
                    <?php endif; ?>
                </div>
                <div class="col-xs-7"></div>
                <div class="col-xs-2">
                    <button class="btn btn-sm btn-danger pull-right" id="remove-user">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>