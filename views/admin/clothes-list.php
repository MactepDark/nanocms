<?php
/**
 * @var $this yii\web\View
 * @var $clothes array
 */
use app\modules\content\models\Content;
use app\models\Cloth;
?>
<table class="table table-condensed table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th><?= Yii::t('admin', 'Название') ?></th>
        <th><?= Yii::t('admin', 'Базовая цена') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($clothes as $cloth): ?>
    <tr data-id="<?= $cloth['id'] ?>">
        <td style="width: 50px; text-align: center; background-color: #eee;">
            <div class="label label-primary"><?= $cloth['priority'] ?></div>
        </td>
        <td>
            <div class="pull-right">
                <?= Cloth::$statusIcons[$cloth['status']] ?>
            </div>
            <?= Content::get(CONTENT_CLOTH_NAME, $cloth['id']) ?>
        </td>
        <td style="width: 100px; text-align: center; font-weight: bold;">
            <span class="badge alert-success"><?= $cloth['price'] ?></span>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>