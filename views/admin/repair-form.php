<?php
/**
 * @var $this yii\web\View
 * @var $repair object
 */

use app\models\Repair;
use app\modules\content\models\Content;
use app\modules\content\BaseContent;
?>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-wrench"></span>
            <?= Content::get(CONTENT_REPAIR_NAME, $repair->id) ?>
        </h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="row form-group">
                <label class="col-xs-4 control-label"><?= Yii::t('admin', 'Название') ?>:</label>
                <div class="col-xs-8">
                    <?= BaseContent::contentField(CONTENT_REPAIR_NAME, $repair->id, BaseContent::FIELD_INPUT) ?>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-4 control-label"><?= Yii::t('admin', 'Базовая цена') ?>:</label>
                <div class="col-xs-8">
                    <input type="number" class="form-control" name="price" data-model="Repair" data-id="<?= $repair->id ?>" value="<?= $repair->price ?>" />
                </div>
            </div>
        </form>
        <hr/>
        <fieldset>
            <legend><?= Yii::t('admin', 'Виды одежды') ?>:</legend>
            <div id="services">
                <?= $this->render('service-clothes', ['repairID' => $repair->id]) ?>
            </div>
        </fieldset>
    </div>
    <div class="panel-footer" style="display: flex; justify-content: space-between;">
        <?php if($repair->status == Repair::STATUS_DRAFT): ?>
            <button class="btn btn-sm btn-default set-item-status" data-status="<?= Repair::STATUS_ACTIVE ?>">
                <?= Repair::$statusIcons[Repair::STATUS_ACTIVE] ?>
                <?= Yii::t('admin', 'Включить') ?>
            </button>
        <?php elseif ($repair->status == Repair::STATUS_ACTIVE): ?>
            <button class="btn btn-sm btn-default set-item-status" data-status="<?= Repair::STATUS_DRAFT ?>">
                <?= Repair::$statusIcons[Repair::STATUS_DRAFT] ?>
                <?= Yii::t('admin', 'Выключить') ?>
            </button>
        <?php endif; ?>
        <div class="btn-group">
            <button class="btn btn-sm btn-default raise-item-priority">
                <span class="glyphicon glyphicon-arrow-up" style="color: green;"></span>
            </button>
            <button class="btn btn-sm btn-default lower-item-priority">
                <span class="glyphicon glyphicon-arrow-down" style="color: red;"></span>
            </button>
        </div>
        <button class="btn btn-sm btn-danger pull-right" id="remove-item">
            <span class="glyphicon glyphicon-remove"></span>
            <?= Yii::t('admin', 'Удалить') ?>
        </button>
    </div>
</div>