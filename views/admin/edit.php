<?php
/**
 * @var $this yii\web\View
 */
use app\modules\content\BaseContent;
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-pencil"></span>
            <?= $this->title ?>
        </h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="row form-group">
                <label class="col-xs-6 control-label">Стоимость базовой услуги:</label>
                <div class="col-xs-6">
                    <?= BaseContent::contentField(CONTENT_BASIC_SERVICE_PRICE, BaseContent::OBJ_NOT_SET, BaseContent::FIELD_INPUT, BaseContent::LANG_NOT_SET) ?>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-6 control-label">Стоимость расширенной услуги:</label>
                <div class="col-xs-6">
                    <?= BaseContent::contentField(CONTENT_ADVANCED_SERVICE_PRICE, BaseContent::OBJ_NOT_SET, BaseContent::FIELD_INPUT, BaseContent::LANG_NOT_SET) ?>
                </div>
            </div>
        </form>
    </div>
</div>