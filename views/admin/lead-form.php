<?php
/**
 * @var $this yii\web\View
 * @var $lead object
 * @var $content object
 * @var $parsedUA object
 */
use app\models\Form;
use yii\helpers\Html;
use app\models\Product;
?>
<div class="panel panel-info">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-user"></span>
            <span class="badge pull-right">ID: <?= $lead->id ?></span>
            <?= $content->name ?>
        </h4>
    </div>
    <div class="panel-body" style="padding: 3px;">
        <table class="table table-condensed table-bordered table-hover" style="margin: 0;">
            <tbody>
            <?php foreach ($content as $key => $value): ?>
            <tr>
                <td style="background-color: #eee; text-align: right; padding-right: 5px; width: 125px;">
                    <?= Form::getFieldNames($key) ?>:
                </td>
                <td style="text-align: left; font-weight: bold; padding-left: 5px;">
                    <?php if($key == Form::FIELD_PHONE): ?>
                        <?= Html::a($value, implode(':', ['tel', Form::cleanNumber($value)])) ?>
                    <?php elseif($key == Form::FIELD_PRODUCT): ?>
                        <?php if($product = Product::findOne($value)): ?>
                            <?= $product->name ?>
                        <?php else: ?>
                            N/A
                        <?php endif; ?>
                    <?php else: ?>
                        <?= $value ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php if(false): ?>
        <table class="table table-condensed table-bordered table-hover" style="margin: 5px 0;">
            <tbody>
            <tr>
                <td style="background-color: #efe; text-align: right; padding: 1px 5px; width: 100px;">
                    <?= Yii::t('admin', 'Платформа') ?>:
                </td>
                <td style="text-align: left; padding: 1px 5px; font-weight: bold;">
                    <?= $parsedUA->platform ?>
                </td>
            </tr>
            <tr>
                <td style="background-color: #efe; text-align: right; padding: 1px 5px; width: 100px;">
                    <?= Yii::t('admin', 'Браузер') ?>:
                </td>
                <td style="text-align: left; padding: 1px 5px; font-weight: bold;">
                    <?= $parsedUA->browser ?>
                    <small>[<?= $parsedUA->version ?>]</small>
                </td>
            </tr>
            <tr>
                <td style="background-color: #efe; text-align: right; padding: 1px 5px; width: 100px;">GEO:</td>
                <td style="text-align: left; padding: 1px 5px; font-weight: bold;">
                    <?php if($lead->geo): ?>
                        <?php if($geo = json_decode($lead->geo)): ?>
                        <dl class="geo-info">
                            <dt><?= Yii::t('admin', 'IP-адрес') ?>:</dt>
                            <dd>
                                <code><?= $lead->ip ?></code>
                            </dd>
                            <dt><?= Yii::t('admin', 'Страна') ?>:</dt>
                            <dd>
                                <span class="badge"><?= $geo->countryCode ?></span>
                                <?= $geo->country ?>
                            </dd>
                            <dt><?= Yii::t('admin', 'Город') ?>:</dt>
                            <dd>
                                <?= $geo->city ?>
                            </dd>
                            <dt><?= Yii::t('admin', 'Провайдер') ?>:</dt>
                            <dd>
                                <?= $geo->isp ?>
                            </dd>
                        </dl>
                        <div id="map" data-lat="<?= $geo->lat ?>" data-lng="<?= $geo->lng ?>" data-zoom="12" style="height: 200px;"></div>
                        <?php endif; ?>
                    <?php elseif (!$lead->geo): ?>
                        <button class="btn btn-xs btn-info pull-right" id="fetch-geo" <?= filter_var($lead->ip, FILTER_VALIDATE_IP) ? 'disabled' : 'disabled' ?>>
                            <span class="glyphicon glyphicon-info-sign"></span>
                        </button>
                        <code><?= $lead->ip ?></code>
                    <?php endif; ?>
                </td>
            </tr>
            </tbody>
        </table>
        <?php endif; ?>
    </div>
    <div class="panel-footer" style="padding: 3px;">
        <button class="btn btn-xs btn-danger pull-right" id="remove-lead">
            <span class="glyphicon glyphicon-remove"></span>
            <?= Yii::t('admin', 'Удалить') ?>
        </button>
        <button class="btn btn-xs btn-default" id="set-lead-view" <?= $lead->status == Form::STATUS_VIEW ? 'disabled' : '' ?>>
            <span class="glyphicon glyphicon-eye-close"></span>
            <?= Yii::t('admin', 'Отметить как просмотренное') ?>
        </button>
        <button class="btn btn-xs btn-primary" id="send-notify">
            <span class="glyphicon glyphicon-send"></span>
        </button>
    </div>
</div>