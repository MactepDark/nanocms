<?php
/**
 * @var $this yii\web\View
 */

use app\models\User;

?>
<table class="table table-condensed table-bordered table-striped">
    <thead>
    <tr>
        <th><?= Yii::t('admin', 'Имя') ?></th>
        <th><?= Yii::t('admin', 'Логин') ?>(e-mail)</th>
        <th><?= Yii::t('admin', 'Статус') ?></th>
        <th><?= Yii::t('admin', 'Дата создания') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach (User::find()->orderBy(['created_at' => SORT_DESC])->asArray()->all() as $user): ?>
    <tr class="user" data-id="<?= $user['id'] ?>">
        <td>
            <?= $user['name'] ?>
        </td>
        <td>
            <?= $user['email'] ?>
        </td>
        <td style="width: 150px;">
            <?= User::$statusIcons[$user['status']] ?>
            <?= User::getStatusNames($user['status']) ?>
        </td>
        <td style="width: 150px;" data-hover="<?= date('d.m.Y H:i:s', $user['created_at']) ?>">
            <?= Yii::$app->formatter->asRelativeTime($user['created_at']) ?>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
