<?php
/**
 * @var $this yii\web\View
 * @var $cloth object
 */

use app\models\Cloth;
use app\modules\content\models\Content;
use app\modules\content\BaseContent;
?>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-leaf"></span>
            <?= Content::get(CONTENT_CLOTH_NAME, $cloth->id) ?>
        </h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="row form-group">
                <label class="col-xs-4 control-label"><?= Yii::t('admin', 'Название') ?>:</label>
                <div class="col-xs-8">
                    <?= BaseContent::contentField(CONTENT_CLOTH_NAME, $cloth->id, BaseContent::FIELD_INPUT) ?>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-xs-4 control-label"><?= Yii::t('admin', 'Базовая цена') ?>:</label>
                <div class="col-xs-8">
                    <input type="number" class="form-control" name="price" data-model="Cloth" data-id="<?= $cloth->id ?>" value="<?= $cloth->price ?>" />
                </div>
            </div>
        </form>
        <hr/>
        <fieldset>
            <legend><?= Yii::t('admin', 'Типы ремонта') ?>:</legend>
            <div id="services">
                <?= $this->render('service-repairs', ['clothID' => $cloth->id]) ?>
            </div>
        </fieldset>
    </div>
    <div class="panel-footer" style="display: flex; justify-content: space-between;">
        <?php if($cloth->status == Cloth::STATUS_DRAFT): ?>
            <button class="btn btn-sm btn-default set-item-status" data-status="<?= Cloth::STATUS_ACTIVE ?>">
                <?= Cloth::$statusIcons[Cloth::STATUS_ACTIVE] ?>
                <?= Yii::t('admin', 'Включить') ?>
            </button>
        <?php elseif ($cloth->status == Cloth::STATUS_ACTIVE): ?>
            <button class="btn btn-sm btn-default set-item-status" data-status="<?= Cloth::STATUS_DRAFT ?>">
                <?= Cloth::$statusIcons[Cloth::STATUS_DRAFT] ?>
                <?= Yii::t('admin', 'Выключить') ?>
            </button>
        <?php endif; ?>
        <div class="btn-group">
            <button class="btn btn-sm btn-default raise-item-priority">
                <span class="glyphicon glyphicon-arrow-up" style="color: green;"></span>
            </button>
            <button class="btn btn-sm btn-default lower-item-priority">
                <span class="glyphicon glyphicon-arrow-down" style="color: red;"></span>
            </button>
        </div>
        <button class="btn btn-sm btn-danger pull-right" id="remove-item">
            <span class="glyphicon glyphicon-remove"></span>
            <?= Yii::t('admin', 'Удалить') ?>
        </button>
    </div>
</div>