<?php
/**
 * @var $this yii\web\View
 * @var $languages array
 * @var $translations array
 */
use app\models\Language;
use app\models\SourceMessage;

?>
<!--<pre>--><?php //print_r($languages) ?><!--</pre>-->
<!--<pre>--><?php //print_r($translations) ?><!--</pre>-->
<?php if(true): ?>
<div class="panel panel-default full-screen">
    <div class="panel-heading">
        <button class="btn btn-xs btn-default pull-right" id="refresh-translations">
            <span class="glyphicon glyphicon-refresh"></span>
            <?= Yii::t('admin', 'Обновить') ?>
        </button>
        <h4 class="panel-title">
            <span class="glyphicon glyphicon-globe"></span>
            <?= Yii::t('admin', 'Переводы') ?>
        </h4>
    </div>
    <div class="panel-body" style="padding: 0;">
        <ul class="nav nav-pills translation-sections">
            <?php foreach ($translations as $section => $translation): ?>
            <li role="presentation">
                <a href="#" data-section="<?= $section ?>">
                    <span class="glyphicon glyphicon-file"></span>
                    <?= SourceMessage::getSectionName($section) ?>
                    <span class="badge"><?= count($translation) ?></span>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php foreach ($translations as $section => $translation): ?>
            <div class="translation" data-id="<?= $section ?>" style="display: none;">
                <table class="table table-condensed table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>
                            <div class="label label-primary"><?= strtoupper(Yii::$app->sourceLanguage) ?></div>
                            <?= Language::getName(Yii::$app->sourceLanguage) ?>
                        </th>
                        <?php foreach ($languages as $langView => $langCode): ?>
                            <?php if($langCode != Yii::$app->sourceLanguage): ?>
                                <th>
                                    <div class="label label-success"><?= strtoupper($langView) ?></div>
                                    <?= Language::getName($langCode) ?>
                                </th>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($translation as $ID => $message): ?>
                        <tr class="translation" data-id="<?= $ID ?>">
                            <td style="vertical-align: middle;"><strong><?= $message['source'] ?></strong></td>
                            <?php foreach ($languages as $langView => $langCode): ?>
                                <?php if($langCode != Yii::$app->sourceLanguage): ?>
                                    <td style="width: <?= 100 / count($languages) ?>%;">
                                        <input type="text" class="form-control input-sm" value="<?= $message['translations'][$langCode] ?>"
                                               placeholder="<?= strtoupper($langView) ?>" data-lang="<?= $langCode ?>" />
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>