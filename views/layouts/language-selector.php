<?php
/**
 * @var $this yii\web\View
 */

use app\modules\content\models\Content;
use app\models\Language;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="flex-box lang">
    <?php foreach (Language::getSiteLanguages() as $language): ?>
        <?= Html::a(Content::get(CONTENT_LANGUAGE_NAME, $language['id']), Url::current(['language' => $language['code']]), ['class' => Yii::$app->language == $language['code'] ? 'btn-lang active' : 'btn-lang']) ?>
    <?php endforeach; ?>
</div>