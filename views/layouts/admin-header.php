<?php
/**
 * @var $this yii\web\View
 */

use app\models\Language;
use yii\helpers\Url;
use app\models\User;
use app\modules\content\models\Content;
?>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= Url::toRoute(['admin/index']) ?>">
                <span class="glyphicon glyphicon-heart"></span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li <?= Yii::$app->controller->action->id == 'products' ? 'class="active"' : '' ?>>
                    <a href="<?= Url::toRoute(['admin/products']) ?>">
                        <span class="glyphicon glyphicon-star"></span>
                        <?= Yii::t('admin', 'Продукция') ?>
                    </a>
                </li>
                <li <?= Yii::$app->controller->action->id == 'edit' ? 'class="active"' : '' ?>>
                    <a href="<?= Url::toRoute(['admin/edit']) ?>">
                        <span class="glyphicon glyphicon-pencil"></span>
                        <?= Yii::t('admin', 'Услуги и стоимость') ?>
                    </a>
                </li>
                <li <?= Yii::$app->controller->action->id == 'leads' ? 'class="active"' : '' ?>>
                    <a href="<?= Url::toRoute(['admin/leads']) ?>">
                        <span class="glyphicon glyphicon-flash"></span>
                        <?= Yii::t('admin', 'Заявки') ?>
                    </a>
                </li>
                <li <?= Yii::$app->controller->action->id == 'contacts' ? 'class="active"' : '' ?>>
                    <a href="<?= Url::toRoute(['admin/contacts']) ?>">
                        <span class="glyphicon glyphicon-info-sign"></span>
                        <?= Yii::t('admin', 'Контакты') ?>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href id="current-user" data-id="<?= Yii::$app->user->id ?>" data-href="<?= Url::toRoute(['admin/users']) ?>">
                        <span class="glyphicon glyphicon-user"></span>
                        <?= User::getName() ?>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="glyphicon glyphicon-flag"></span>
                        <?= Language::getView() ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach (Language::getLanguages() as $langView => $langCode): ?>
                        <li <?= $langCode == Yii::$app->language ? 'class="active"' : '' ?>>
                            <a href="<?= Url::current(['language' => $langCode])?>">
                                [<?= strtoupper($langView) ?>]
                                <?= Content::get(CONTENT_LANGUAGE_NAME, Language::findOne(['code' => $langCode])->id, $langCode) ?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li class="dropdown <?= in_array(Yii::$app->controller->action->id, ['settings', 'languages', 'users', 'translations']) ? 'active' : '' ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="glyphicon glyphicon-cog"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li <?= Yii::$app->controller->action->id == 'settings' ? 'class="active"' : '' ?>>
                            <a href="<?= Url::toRoute(['admin/settings']) ?>">
                                <span class="glyphicon glyphicon-wrench"></span>
                                <?= Yii::t('admin', 'Настройки') ?>
                            </a>
                        </li>
                        <li <?= Yii::$app->controller->action->id == 'languages' ? 'class="active"' : 'class="disabled"' ?>>
                            <a href="<?= Url::toRoute(['admin/languages']) ?>">
                                <span class="glyphicon glyphicon-flag"></span>
                                <?= Yii::t('admin', 'Языки') ?>
                            </a>
                        </li>
                        <li <?= Yii::$app->controller->action->id == 'users' ? 'class="active"' : '' ?>>
                            <a href="<?= Url::toRoute(['admin/users']) ?>">
                                <span class="glyphicon glyphicon-user"></span>
                                <?= Yii::t('admin', 'Пользователи') ?>
                            </a>
                        </li>
                        <li <?= Yii::$app->controller->action->id == 'translations' ? 'class="active"' : '' ?>>
                            <a href="<?= Url::toRoute(['admin/translations']) ?>">
                                <span class="glyphicon glyphicon-globe"></span>
                                <?= Yii::t('admin', 'Переводы') ?>
                            </a>
                        </li>
                        <li <?= Yii::$app->controller->action->id == 'privacy-policy' ? 'class="active"' : 'class="disabled"' ?>>
                            <a href="<?= Url::toRoute(['admin/privacy-policy']) ?>">
                                <span class="glyphicon glyphicon-briefcase"></span>
                                <?= Yii::t('admin', 'Политика конфиденциальности') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li <?= Yii::$app->controller->action->id == 'options' ? 'class="active"' : 'class="disabled"' ?>>
                            <a href="<?= Url::toRoute(['media/default/options']) ?>">
                                <span class="glyphicon glyphicon-scissors"></span>
                                <?= Yii::t('admin', 'Настройки кроппера') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li>
                    <a href id="site-preview" data-href="<?= Url::toRoute(['site/index'], true) ?>">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::toRoute(['site/index']) ?>" target="_blank">
                        <span class="glyphicon glyphicon-home"></span>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::toRoute(['admin/logout']) ?>">
                        <span class="glyphicon glyphicon-log-out"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>