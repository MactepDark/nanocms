<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use app\models\Form;
use app\modules\content\models\Content;
use app\modules\content\BaseContent;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="ajaxUrl" content="<?= Url::toRoute(['site/ajax']) ?>">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <title><?= Html::encode($this->title) ?></title>
</head>
<body>
<?php $this->beginBody() ?>

<?= $this->render('header') ?>

<?= $content ?>

<?= $this->render('footer') ?>

<div class="flex-box justify-center align-center modal thq-modal" id="thq-modal">
    <div class="flex-box column inner">
        <button class="btn-close-modal"></button>

        <p class="font-style-secondary font-size-24 color-white fw-400 title"><?= Yii::t('app', 'Ваша заявка принята!') ?></p>

        <div class="h16px"></div>

        <p class="font-size-20 color-white fw-400 description"><?= Yii::t('app', 'Менеджер свяжется с Вами в ближайшее время') ?></p>
    </div>
</div>

<div class="flex-box justify-center align-center modal order-modal" id="order-modal">
    <div class="flex-box column inner">
        <button class="btn-close-modal"></button>
        <form class="flex-box column" data-type="<?= Form::TYPE_QUICK ?>">
            <input type="hidden" name="<?= Form::FIELD_PRODUCT ?>" value="" />
            <div class="mdc-text-field" data-mdc-auto-init="MDCTextField">
                <input class="mdc-text-field__input  mdc-theme--primary" type="text" name="<?= Form::FIELD_NAME ?>">
                <label class="mdc-floating-label mdc-theme--primary"><?= Yii::t('app', 'Имя') ?></label>
                <div class="mdc-line-ripple"></div>
            </div>
            <div class="mdc-text-field" data-mdc-auto-init="MDCTextField">
                <input class="mdc-text-field__input  mdc-theme--primary" type="text" name="<?= Form::FIELD_PHONE ?>">
                <label class="mdc-floating-label mdc-theme--primary"><?= Yii::t('app', 'Телефон') ?></label>
                <div class="mdc-line-ripple"></div>
            </div>

            <button type="submit" class="btn-type-1"><?= Yii::t('app', 'ПОДОБРАТЬ АВТО') ?></button>
        </form>
    </div>
</div>

<div class="flex-box justify-center align-center modal order-modal-extended" id="order-modal-extended">
    <div class="flex-box column inner">
        <button class="btn-close-modal"></button>
        <form class="flex-box column" data-type="<?= Form::TYPE_DEFAULT ?>">
            <input type="hidden" name="service" value="" />
            <?= $this->render('/site/forms/default-form') ?>
        </form>
    </div>
</div>

<script src="//unpkg.com/material-components-web@v4.0.0/dist/material-components-web.min.js"></script>

<script>
    function initMap() {
        initGoogleMap(
            parseFloat(<?= Content::get(CONTENT_CONTACT_MAP_LAT, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET) ?>),
            parseFloat(<?= Content::get(CONTENT_CONTACT_MAP_LNG, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET) ?>)
        );
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDocckFLbOj59qNlvEVktjbybt0X5CD74M&callback=initMap"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
