<?php
/**
 * @var $this yii\web\View
 */

use app\models\Contact;
use app\modules\content\models\Content;
use app\modules\content\BaseContent;

$phone = Contact::find()->select(['value'])->where(['type' => Contact::TYPE_PHONE])->scalar();
$facebook = Contact::find()->select(['value'])->where(['type' => Contact::TYPE_FACEBOOK])->scalar();
$instagram = Contact::find()->select(['value'])->where(['type' => Contact::TYPE_INSTAGRAM])->scalar();
?>

<header class="header">
    <div class="pre-header flex-box align-center">
        <div class="container flex-box justify-space-between align-center">
            <?= $this->render('/layouts/language-selector') ?>
            <div class="flex-box justify-end align-center">
                <a href="tel:<?= Contact::cleanNumber($phone) ?>" class="font-size-16 fw-600 color-orange hover-color-white"><?= $phone ?></a>

                <p class="font-size-14 color-white address">
                    <?= Content::get(CONTENT_CONTACT_ADDRESS, BaseContent::OBJ_NOT_SET) ?>
                </p>

                <div class="flex-box social-icons">
                    <?php if($instagram): ?>
                    <a href="<?= $instagram ?>" class="social">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M2.8 2H21.2C21.6425 2 22 2.3575 22 2.8V21.2C22 21.6425 21.6425 22 21.2 22H2.8C2.3575 22 2 21.6425 2 21.2V2.8C2 2.3575 2.3575 2 2.8 2ZM9.3301 12C9.3301 10.531 10.5289 9.3322 11.9978 9.3322C13.4668 9.3322 14.6656 10.531 14.6656 12C14.6656 13.469 13.4668 14.6678 11.9978 14.6678C10.5289 14.6678 9.3301 13.469 9.3301 12ZM19.9994 11.5959C19.9993 11.7304 19.9991 11.8651 19.9991 12C19.9991 12.1508 19.9993 12.3014 19.9996 12.4519C20.001 13.4038 20.0024 14.35 19.9471 15.3022C19.885 16.5831 19.5948 17.7179 18.6562 18.6565C17.7196 19.5931 16.5829 19.8853 15.302 19.9474C14.3339 20.0018 13.3719 20.0008 12.4039 19.9997C12.2694 19.9996 12.1347 19.9994 11.9999 19.9994C11.8653 19.9994 11.7309 19.9996 11.5965 19.9997C10.6298 20.0008 9.66761 20.0018 8.69768 19.9474C7.41684 19.8853 6.28209 19.5951 5.34348 18.6565C4.40686 17.7199 4.11467 16.5831 4.05263 15.3022C3.99816 14.334 3.99923 13.3705 4.0003 12.4034C4.00045 12.269 4.0006 12.1345 4.0006 12C4.0006 11.8654 4.00045 11.731 4.0003 11.5966C3.99923 10.6299 3.99816 9.6677 4.05263 8.69777C4.11467 7.4169 4.40486 6.28214 5.34348 5.3435C6.28009 4.40687 7.41684 4.11467 8.69768 4.05263C9.66581 3.99816 10.6278 3.99923 11.5957 4.0003C11.7303 4.00045 11.865 4.0006 11.9999 4.0006C12.1344 4.0006 12.2689 4.00045 12.4032 4.0003C13.3699 3.99923 14.3321 3.99816 15.302 4.05263C16.5829 4.11467 17.7176 4.40487 18.6562 5.3435C19.5928 6.28014 19.885 7.4169 19.9471 8.69777C20.0015 9.66591 20.0005 10.6279 19.9994 11.5959ZM7.89315 12C7.89315 14.2715 9.72636 16.1048 11.9978 16.1048C14.2693 16.1048 16.1025 14.2715 16.1025 12C16.1025 9.72847 14.2693 7.89523 11.9978 7.89523C9.72636 7.89523 7.89315 9.72847 7.89315 12ZM15.312 7.72711C15.312 8.25747 15.7403 8.68576 16.2707 8.68576C16.3966 8.68592 16.5213 8.66123 16.6377 8.61311C16.7541 8.56499 16.8598 8.49438 16.9489 8.40533C17.0379 8.31628 17.1085 8.21054 17.1566 8.09416C17.2048 7.97778 17.2294 7.85305 17.2293 7.72711C17.2293 7.19675 16.801 6.76847 16.2707 6.76847C15.7403 6.76847 15.312 7.19675 15.312 7.72711Z" fill="#fff"/></svg>
                    </a>
                    <?php endif; ?>
                    <?php if($facebook): ?>
                    <a href="<?= $facebook ?>" class="social">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21.2 2H2.8C2.3575 2 2 2.3575 2 2.8V21.2C2 21.6425 2.3575 22 2.8 22H21.2C21.6425 22 22 21.6425 22 21.2V2.8C22 2.3575 21.6425 2 21.2 2ZM18.89 7.8375H17.2925C16.04 7.8375 15.7975 8.4325 15.7975 9.3075V11.235H18.7875L18.3975 14.2525H15.7975V22H12.68V14.255H10.0725V11.235H12.68V9.01C12.68 6.4275 14.2575 5.02 16.5625 5.02C17.6675 5.02 18.615 5.1025 18.8925 5.14V7.8375H18.89Z" fill="#fff"/></svg>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="nav">
        <div class="container flex-box justify-center align-center">
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#about-us"><?= Yii::t('app', 'О нас') ?></button>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#why-usa-cars"><?= Yii::t('app', 'преимущества') ?></button>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#stages"><?= Yii::t('app', 'этапы работы') ?></button>
            <button class="btn-header-nav logo">
                <img src="/img/logo.svg">
            </button>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#services"><?= Yii::t('app', 'услуги и стоимость') ?></button>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#faq"><?= Yii::t('app', 'помощь') ?></button>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#contacts"><?= Yii::t('app', 'контакты') ?></button>
        </div>
    </div>
</header>

<div class="mobile-nav">
    <div class="pre-header flex-box ">
        <div class="container flex-box column">

            <div class="h24px"></div>

            <div class="flex-box justify-space-between">
                <?= $this->render('/layouts/language-selector') ?>

                <div class="hamburger"></div>

            </div>


            <div class="flex-box column align-center">

                <div class="h24px"></div>

                <a href="tel:<?= Contact::cleanNumber($phone) ?>" class="font-size-16 fw-600 color-orange hover-color-white"><?= $phone ?></a>

                <div class="h16px"></div>

                <p class="font-size-14 color-white address">
                    <?= Content::get(CONTENT_CONTACT_ADDRESS, BaseContent::OBJ_NOT_SET) ?>
                </p>

                <div class="h18px"></div>

                <div class="flex-box social-icons">
                    <?php if($instagram): ?>
                        <a href="<?= $instagram ?>" class="social">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M2.8 2H21.2C21.6425 2 22 2.3575 22 2.8V21.2C22 21.6425 21.6425 22 21.2 22H2.8C2.3575 22 2 21.6425 2 21.2V2.8C2 2.3575 2.3575 2 2.8 2ZM9.3301 12C9.3301 10.531 10.5289 9.3322 11.9978 9.3322C13.4668 9.3322 14.6656 10.531 14.6656 12C14.6656 13.469 13.4668 14.6678 11.9978 14.6678C10.5289 14.6678 9.3301 13.469 9.3301 12ZM19.9994 11.5959C19.9993 11.7304 19.9991 11.8651 19.9991 12C19.9991 12.1508 19.9993 12.3014 19.9996 12.4519C20.001 13.4038 20.0024 14.35 19.9471 15.3022C19.885 16.5831 19.5948 17.7179 18.6562 18.6565C17.7196 19.5931 16.5829 19.8853 15.302 19.9474C14.3339 20.0018 13.3719 20.0008 12.4039 19.9997C12.2694 19.9996 12.1347 19.9994 11.9999 19.9994C11.8653 19.9994 11.7309 19.9996 11.5965 19.9997C10.6298 20.0008 9.66761 20.0018 8.69768 19.9474C7.41684 19.8853 6.28209 19.5951 5.34348 18.6565C4.40686 17.7199 4.11467 16.5831 4.05263 15.3022C3.99816 14.334 3.99923 13.3705 4.0003 12.4034C4.00045 12.269 4.0006 12.1345 4.0006 12C4.0006 11.8654 4.00045 11.731 4.0003 11.5966C3.99923 10.6299 3.99816 9.6677 4.05263 8.69777C4.11467 7.4169 4.40486 6.28214 5.34348 5.3435C6.28009 4.40687 7.41684 4.11467 8.69768 4.05263C9.66581 3.99816 10.6278 3.99923 11.5957 4.0003C11.7303 4.00045 11.865 4.0006 11.9999 4.0006C12.1344 4.0006 12.2689 4.00045 12.4032 4.0003C13.3699 3.99923 14.3321 3.99816 15.302 4.05263C16.5829 4.11467 17.7176 4.40487 18.6562 5.3435C19.5928 6.28014 19.885 7.4169 19.9471 8.69777C20.0015 9.66591 20.0005 10.6279 19.9994 11.5959ZM7.89315 12C7.89315 14.2715 9.72636 16.1048 11.9978 16.1048C14.2693 16.1048 16.1025 14.2715 16.1025 12C16.1025 9.72847 14.2693 7.89523 11.9978 7.89523C9.72636 7.89523 7.89315 9.72847 7.89315 12ZM15.312 7.72711C15.312 8.25747 15.7403 8.68576 16.2707 8.68576C16.3966 8.68592 16.5213 8.66123 16.6377 8.61311C16.7541 8.56499 16.8598 8.49438 16.9489 8.40533C17.0379 8.31628 17.1085 8.21054 17.1566 8.09416C17.2048 7.97778 17.2294 7.85305 17.2293 7.72711C17.2293 7.19675 16.801 6.76847 16.2707 6.76847C15.7403 6.76847 15.312 7.19675 15.312 7.72711Z" fill="#fff"/></svg>
                        </a>
                    <?php endif; ?>
                    <?php if($facebook): ?>
                        <a href="<?= $facebook ?>" class="social">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21.2 2H2.8C2.3575 2 2 2.3575 2 2.8V21.2C2 21.6425 2.3575 22 2.8 22H21.2C21.6425 22 22 21.6425 22 21.2V2.8C22 2.3575 21.6425 2 21.2 2ZM18.89 7.8375H17.2925C16.04 7.8375 15.7975 8.4325 15.7975 9.3075V11.235H18.7875L18.3975 14.2525H15.7975V22H12.68V14.255H10.0725V11.235H12.68V9.01C12.68 6.4275 14.2575 5.02 16.5625 5.02C17.6675 5.02 18.615 5.1025 18.8925 5.14V7.8375H18.89Z" fill="#fff"/></svg>
                        </a>
                    <?php endif; ?>
                </div>

                <div class="h24px"></div>

            </div>
        </div>
    </div>

    <div class="nav">

        <div class="h32px"></div>

        <div class="container flex-box column justify-center align-center">
            <button class="btn-header-nav logo">
                <img src="/img/logo.svg">
            </button>
            <div class="h32px"></div>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#about-us"><?= Yii::t('app', 'О нас') ?></button>
            <div class="h32px"></div>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#why-usa-cars"><?= Yii::t('app', 'преимущества') ?></button>
            <div class="h32px"></div>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#stages"><?= Yii::t('app', 'этапы работы') ?></button>
            <div class="h32px"></div>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#stages"><?= Yii::t('app', 'услуги и стоимость') ?></button>
            <div class="h32px"></div>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#faq"><?= Yii::t('app', 'помощь') ?></button>
            <div class="h32px"></div>
            <button class="font-size-14 fw-600 btn-header-nav hover-color-orange scroll-to" data-attr="#contacts"><?= Yii::t('app', 'контакты') ?></button>
        </div>
    </div>
</div>

<div class="header-mobile flex-box justify-end">
    <div class="hamburger"></div>
</div>