<?php
/**
 * @var $this yii\web\View
 * @var $products array
 */
use app\modules\content\models\Content;
use app\modules\content\BaseContent;
use app\modules\media\models\Image;
use app\models\Product;
use app\models\Form;
?>

<section class="hero">

    <div class="h64px"></div>
    <div class="h64px"></div>
    <div class="h64px"></div>
    <div class="h24px"></div>

    <div class="container position-relative flex-box column align-start">

        <h1 class="h1">
            <?= Yii::t('app', 'Покупка автомобилей из США с выгодой') ?>
            <span><?= Yii::t('app', 'от 30% до 50%') ?></span>
        </h1>

        <div class="h64px hidden-xs"></div>
        <div class="h32px hidden visible-xs"></div>

        <div class="advantages flex-box align-start wrap-on-mobile">
            <div class="item flex-box column justify-center text-center">
                <img src="/img/feature_1.svg" alt="">
                <p><?= Yii::t('app', 'Помощь в выборе') ?></p>
            </div>
            <div class="item flex-box column justify-center text-center">
                <img src="/img/feature_2.svg" alt="">
                <p><?= Yii::t('app', 'Покупка') ?></p>
            </div>
            <div class="item flex-box column justify-center text-center">
                <img src="/img/feature_3.svg" alt="">
                <p><?= Yii::t('app', 'Доставка') ?></p>
            </div>
            <div class="item flex-box column justify-center text-center">
                <img src="/img/feature_4.svg" alt="">
                <p><?= Yii::t('app', 'Оформление документов') ?></p>
            </div>
            <div class="item flex-box column justify-center text-center">
                <img src="/img/feature_5.svg" alt="">
                <p><?= Yii::t('app', 'Сопровождение ремонта') ?></p>
            </div>
        </div>

        <div class="h64px"></div>
        <div class="h10px hidden-xs"></div>
        <div class="h10px hidden-xs"></div>

        <div class="delivery flex-box align-center align-self-end align-self-start-on-mobile">
            <?= Yii::t('app', 'ДОСТАВКА ОТ 60 ДНЕЙ') ?>
        </div>

        <img class="hero-img" src="/img/hero_img.png" alt="">

    </div>

    <div class="h64px"></div>
    <div class="h12px"></div>
</section>

<section class="section-form">

    <div class="h32px"></div>

    <div class="container flex-box justify-center">
        <form class="flex-box justify-space-between justify-center-on-mobile wrap" data-type="<?= Form::TYPE_DEFAULT ?>">
            <?= $this->render('forms/default-form') ?>
        </form>
    </div>

</section>

<section class="about-us" id="about-us">

    <div class="h64px"></div>
    <div class="h16px"></div>

    <div class="container flex-box column">
        <div class="content flex-box column">

            <h2 class="h2"><?= Yii::t('app', 'О нас') ?></h2>

            <div class="h64px"></div>

            <div class="font-size-18">
                <p>
                    <?= Yii::t('app', 'Компания AVTOTRUST с 2017 года занимается подбором, покупкой, логистикой и таможенным оформлением автомобилей/мотоциклов из США и предлагает своим клиентам лучший сервис по доставке Вашей мечты в Украину!') ?>
                </p>
                <p>
                    <?= Yii::t('app', 'Особенность и экслюзивность нашей компании - в индивидуальном отточенном подходе, с нами каждый из Вас сможет осуществить свою мечту и выбрать самый оптимальный вариант, исходя из своих предпочтений.') ?>
                </p>
                <p>
                    <?= Yii::t('app', 'В связи с выгодными условиями растаможки автомобилей из США (законопроект 8487), финальная стоимость автомобиля с ремонтом выгоднее на 30-50 % в сравнении с тем, что предлагают на вторичном рынке Украины.') ?>
                </p>
                <p>
                    <?= Yii::t('app', 'Для безопасности и прозрачности мы с клиентом заключаем договор, все платежи проводятся официально, путем оплаты напрямую в Америку по банковским инвойсам.') ?>
                </p>
                <p>
                    <?= Yii::t('app', 'Наша компания является авторизованным дилером самых популярных аукционов: Copart, IAAI, Manheim, что позволяет нам получать наименьшие ставки аукционных сборов и дает возможность предлагать максимально выгодные условия.') ?>
                </p>
            </div>
        </div>

        <img class="about-us-img" src="/img/about_us_img.png" alt="">

    </div>

    <div class="h64px hidden-xs"></div>
    <div class="h64px"></div>

</section>

<section class="why-usa-cars" id="why-usa-cars">

    <div class="h64px"></div>
    <div class="h16px"></div>

    <div class="container flex-box column-on-tablet justify-space-between">
        <div class="column">
            <h2 class="h2 color-white">
                <?= Yii::t('app', 'ПРЕИМУЩЕСТВА') ?>
                <p class="color-orange"><?= Yii::t('app', 'АВТО ИЗ США') ?></p>
            </h2>
        </div>

        <div class="h64px"></div>

        <div class="advantages-items flex-box column">
            <div class="item">
                <h3 class="h3 color-white with-numbering"><span class="number">01</span><?= Yii::t('app', 'БЕРЕЖНАЯ ЭКСПЛУАТАЦИЯ') ?></h3>

                <div class="h16px"></div>

                <p class="font-size-15 color-white">
                    <?= Yii::t('app', 'Львиная доля американских автомобилей покупается в кредит (лизинг), условия которого регламентируют пробег не более 12 тысяч миль в год.') ?>
                    <?= Yii::t('app', 'Автомобили ездят по хорошим дорогам и проходят обязательное сервисное обслуживание только в сертифицированных СТО с заменой расходных запчастей исключительно на оригинал') ?>
                </p>
            </div>

            <div class="h24px"></div>

            <div class="item">
                <h3 class="h3 color-white with-numbering"><span class="number">02</span><?= Yii::t('app', 'Прозрачность истории') ?></h3>

                <div class="h16px"></div>

                <p class="font-size-15 color-white">
                    <?= Yii::t('app', 'У каждого автомобиля есть зафиксированная история по владельцам и прохождению ТО, а также по ранее произошедшим ДТП, поэтому можно быть уверенным в том, что Вы будете знать об автомобиле практически всё до момента покупки') ?>
                </p>
            </div>

            <div class="h24px"></div>

            <div class="item">
                <h3 class="h3 color-white with-numbering"><span class="number">03</span><?= Yii::t('app', 'цена') ?></h3>

                <div class="h16px"></div>

                <p class="font-size-15 color-white">
                    <?= Yii::t('app', 'Покупая авто в Америке, Вы сможете позволить себе автомобиль в максимальной комплектацией и экономией 30-50% в сравнении с таким же предложением в Украине') ?>
                </p>
            </div>

            <div class="h24px"></div>

            <div class="item">
                <h3 class="h3 color-white with-numbering"><span class="number">04</span><?= Yii::t('app', 'Технические преимущества') ?></h3>

                <div class="h16px"></div>

                <p class="font-size-15 color-white">
                    <?= Yii::t('app', 'Большинство автомобилей собирается в США, Мексике и Канаде, что обеспечивает высокое качество сборки, так как стандарты производства там высочайшего уровня') ?>
                </p>
            </div>

        </div>
    </div>

    <img class="why-usa-img" src="/img/why-usa.png" alt="">

    <div class="h64px"></div>
    <div class="h64px"></div>

</section>

<section class="our-partners" id="our-partners">

    <div class="h32px"></div>
    <div class="h16px"></div>

    <div class="container flex-box column align-center justify-center">
        <h2 class="h2 text-center"><?= Yii::t('app', 'НАШИ ПАРТНЕРЫ') ?></h2>

        <div class="h32px"></div>
        <div class="h16px"></div>

        <div class="content flex-box justify-space-between column-on-mobile align-center-on-mobile">

            <img src="/img/partner_1.png">

            <div class="h32px visible-xs"></div>

            <img src="/img/partner_2.png">

            <div class="h32px visible-xs"></div>

            <img src="/img/partner_3.png">

        </div>
    </div>

    <div class="h64px"></div>

</section>

<section class="examples" id="examples">

    <div class="h64px"></div>
    <div class="h16px"></div>

    <div class="container flex-box column">
        <h2 class="h2"><?= Yii::t('app', 'ПРИМЕРЫ АВТО ИЗ США') ?></h2>
    </div>

    <div class="h64px"></div>
    <div class="h16px"></div>

    <div class="slider">
        <?php foreach ($products as $product): ?>
            <div class="slide">
                <div class="item flex-box column align-start">
                    <img src="<?= Image::getSrc(false, TYPE_PRODUCT_IMAGE, $product['id']) ?>" />

                    <div class="flex-box column text-content">

                        <div class="h24px"></div>

                        <h4 class="h4"><?= $product['name'] ?></h4>

                        <div class="h12px"></div>

                        <div class="flex-box justify-space-between price-row auction-price">
                            <div class="key font-style-primary color-deep-black font-size-14">
                                <?= Yii::t('app', 'Цена авто на аукционе') ?>:
                            </div>
                            <div class="value font-style-secondary color-orange font-size-14">
                                <?= $totalPrice = $product['auctionPrice'] ?>$
                            </div>
                        </div>

                        <div class="h16px"></div>
                        <?php foreach (Product::$productExpensive as $contentTypeID => $expensiveName): ?>
                            <?php if($cost = Content::get($contentTypeID, $product['id'], BaseContent::LANG_NOT_SET)): ?>
                            <?php $totalPrice += $cost ?>
                                <div class="flex-box justify-space-between price-row">
                                    <div class="key <?= $contentTypeID == CONTENT_PRODUCT_AUCTION_FEE ? 'font-style-primary' : 'font-style-secondary' ?>">
                                        <?= $expensiveName ?>
                                    </div>
                                    <div class="value <?= $contentTypeID == CONTENT_PRODUCT_AUCTION_FEE ? 'font-style-primary' : 'font-style-secondary' ?>">
                                        <?= $cost ?>$
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <div class="h24px"></div>

                        <div class="market-value flex-box justify-center align-center color-deep-black fw-600">
                            <?= Yii::t('app', 'Рыночная стоимость') ?>:
                            $<?= $product['marketPrice'] ?>
                        </div>

                        <div class="h24px"></div>

                        <div class="flex-box justify-space-between">
                            <button class="btn-type-2" data-id="<?= $product['id'] ?>"><?= Yii::t('app', 'ЗАКАЗАТЬ') ?></button>

                            <div class="final flex-box column align-center justify-space-between">
                                <span class="fw-500 font-size-12 font-style-secondary">
                                    <?= Yii::t('app', 'Итого под ключ') ?>:
                                </span>
                                <p class="font-style-primary fw-700 font-size-28 color-orange final-price">
                                    $<?= $totalPrice ?>
                                </p>
                            </div>
                        </div>

                        <div class="h24px"></div>

                    </div>

                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="h32px"></div>
    <div class="h10px"></div>

    <div class="container flex-box justify-center">
        <div class="slider-nav flex-box justify-center align-center">
            <div class="btn-type-arrow-left slick-prev"></div>

            <div class="current-slide num color-orange font-size-14" id="currentSlide">1</div>

            <div class="line"></div>

            <div class="total-slides num font-size-14" id="totalSlides">5</div>

            <div class="btn-type-arrow-right slick-next"></div>
        </div>
    </div>

    <div class="h64px"></div>
    <div class="h64px"></div>

</section>

<section class="stages" id="stages">

    <div class="h64px"></div>
    <div class="h16px hidden-xs"></div>

    <div class="container flex-box column">
        <h2 class="h2 color-white"><?= Yii::t('app', 'ЭТАПЫ') ?></h2>

        <div class="h64px"></div>
        <div class="h16px hidden-xs"></div>

        <div class="content flex-box column">

            <div class="line"></div>

            <div class="flex-box justify-start">
                <div class="flex-box column item left with-numbering">
                    <span class="number">01</span>
                    <span class="font-size-18 color-orange deadlines">1 - 4 день</span>
                    <div class="font-size-27 fw-500 color-white title"><?= Yii::t('app', 'Заявка, выбор авто') ?></div>

                    <div class="h16px"></div>

                    <p class="font-size-16 fw-400 color-white">
                        <?= Yii::t('app', 'Вы оставляете заявку на нашем сайте, наши специалисты максимально быстро связываются с Вами.') ?>
                        <?= Yii::t('app', 'В течении двух - четырех дней мы предварительно подбираем для Вас 5-10 вариантов авто') ?>
                    </p>
                </div>
            </div>
            <div class="flex-box justify-end justify-start-on-mobile">
                <div class="flex-box column item right with-numbering">
                    <span class="number">02</span>
                    <span class="font-size-18 color-orange deadlines">4 - 6 день</span>
                    <div class="font-size-27 fw-500 color-white title"><?= Yii::t('app', 'Подписание договора') ?></div>

                    <div class="h16px"></div>

                    <p class="font-size-16 fw-400 color-white">
                        <?= Yii::t('app', 'Мы подписываем договор, Вы вносите задаток в размере 400$ или 10% в зависимости от стоимости авто.') ?>
                        <?= Yii::t('app', 'Окончательно определяемся с моделью и выбираем 2-3 варианта для торгов, консультируем Вас по всем вопросам') ?>
                    </p>
                </div>
            </div>

            <div class="flex-box justify-start">
                <div class="flex-box column item left with-numbering">
                    <span class="number">03</span>
                    <span class="font-size-18 color-orange deadlines">7 - 10 день</span>
                    <div class="font-size-27 fw-500 color-white title"><?= Yii::t('app', 'Проверка, торги на аукционах, покупка') ?></div>

                    <div class="h16px"></div>

                    <p class="font-size-16 fw-400 color-white">
                        <?= Yii::t('app', 'После утверждения конкретных лотов мы БЕСПЛАТНО (только для наших клиентов) предоставляем отчет по истории автомобиля.') ?>
                        <?= Yii::t('app', 'После Вашего окончательного согласия - покупаем авто на аукционе.') ?>
                        <?= Yii::t('app', 'В течении 2-х суток Вы оплачиваете инвойс, который включает') ?>:
                    <p class="font-size-16 fw-400 color-white">— <?= Yii::t('app', 'стоимость авто + комиссия аукциона;') ?></p>
                    <p class="font-size-16 fw-400 color-white">— <?= Yii::t('app', 'логистика по суше (США) + доставка морем в Одессу;') ?></p>
                    <p class="font-size-16 fw-400 color-white">— <?= Yii::t('app', 'стоимость наших услуг') ?></p>
                </div>
            </div>
            <div class="flex-box justify-end justify-start-on-mobile">
                <div class="flex-box column item right with-numbering">
                    <span class="number">04</span>
                    <span class="font-size-18 color-orange deadlines">10 - 80 день</span>
                    <div class="font-size-27 fw-500 color-white title"><?= Yii::t('app', 'Доставка авто в Украину') ?></div>

                    <div class="h16px"></div>

                    <p class="font-size-16 fw-400 color-white">
                        <?= Yii::t('app', 'После осуществления всех платежей, Ваш автомобиль будет доставлен в американский порт для осуществления фрахта и подготовки экспортных документов на транспортировку из США, после погружен в контейнер на судно.') ?>
                        <?= Yii::t('app', 'В среднем срок доставки 8-12 недель в зависимости от штата и удалённости порта отгрузки') ?>
                    </p>
                </div>
            </div>

            <div class="flex-box justify-start">
                <div class="flex-box column item left with-numbering">
                    <span class="number">05</span>
                    <div class="font-size-27 fw-500 color-white title"><?= Yii::t('app', 'Растаможка в порту Одесса') ?></div>

                    <div class="h16px"></div>

                    <p class="font-size-16 fw-400 color-white">
                        <?= Yii::t('app', 'Прибытие автомобиля в порт Одессы.') ?>
                        <?= Yii::t('app', 'После выгрузки наш брокер подготовит документы для таможенного оформления и оплаты пошлины (оплата в грн по факту выгрузки из контейнера).') ?>
                        <?= Yii::t('app', 'После растаможки автомобиль будет готов к выдаче в порту.') ?>
                        <?= Yii::t('app', 'Для удобства клиента автомобиль может быть доставлен автовозом в любую точку Украины.') ?>
                        <?= Yii::t('app', 'Ваше присутствие в Одессе не нужно, все заботы мы берём на себя') ?>
                    </p>
                </div>
            </div>
            <div class="flex-box justify-end justify-start-on-mobile">
                <div class="flex-box column item right with-numbering">
                    <span class="number">06</span>
                    <div class="font-size-27 fw-500 color-white title"><?= Yii::t('app', 'Сопровождение по ремонту авто') ?></div>

                    <div class="h16px"></div>

                    <p class="font-size-16 fw-400 color-white">
                        <?= Yii::t('app', 'Мы осуществляем поиск и подбор запчастей ( при заказе пакета “Расширенный” ), а также курируем весь процесс ремонта Вашего авто на партнерском СТО.') ?>
                    </p>
                </div>
            </div>

            <div class="flex-box justify-start">
                <div class="flex-box column item left with-numbering">
                    <span class="number">07</span>
                    <div class="font-size-27 fw-500 color-white title"><?= Yii::t('app', 'Сертификация и постановка на учет') ?></div>

                    <div class="h16px"></div>

                    <p class="font-size-16 fw-400 color-white">
                        <?= Yii::t('app', 'Сертификация авто необходима для постановки автомобиля на учёт.') ?>
                        <?= Yii::t('app', 'Сертификат выдаётся аккредитованными лабораториями лишь на полностью целое авто.') ?>
                        <?= Yii::t('app', 'Далее автомобиль регистрируется в Сервисном центре МВД с выдачей тех.паспорта и государственных номерных знаков.') ?>
                        <?= Yii::t('app', 'Вы можете воспользоваться нашей помощью в осуществлении этих услуг при заказе пакета “Расширенный” или сделать это самостоятельно') ?>
                    </p>
                </div>
            </div>
            <div class="flex-box justify-end justify-start-on-mobile">
                <div class="flex-box column item right with-numbering">
                    <span class="number">08</span>
                    <div class="font-size-27 fw-500 color-white title"><?= Yii::t('app', 'Вы становитесь счастливым обладателем автомобиля своей мечты!') ?></div>
                </div>
            </div>


        </div>

    </div>

    <div class="h64px hidden-xs"></div>
    <div class="h16px hidden-xs"></div>

</section>

<section class="services" id="services">

    <div class="h64px"></div>
    <div class="h16px"></div>

    <div class="container flex-box column">
        <h2 class="h2 text-center"><?= Yii::t('app', 'Услуги и стоимость') ?></h2>

        <div class="h64px"></div>

        <div class="content flex-box align-center column-on-mobile">
            <div class="item left">

                <div class="h32px"></div>
                <div class="h8px"></div>

                <div class="font-size-40 fw-600 color-orange title"><?= Yii::t('app', 'БАЗОВЫЙ') ?></div>

                <div class="h32px"></div>
                <div class="h8px"></div>

                <div class="font-size-30 fw-500">
                    <?= Content::get(CONTENT_BASIC_SERVICE_PRICE, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET) ?>
                </div>

                <div class="h24px"></div>

                <p class="font-size-16">— <?= Yii::t('app', 'Подбор') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Проверка авто') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Покупка на аукционе') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Логистика из США в порт г. Одесса') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Таможенная очистка в порту Одессы') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Организация доставки из Одессы к Вам домой') ?></p>

                <div class="h24px"></div>
                <div class="h24px"></div>

                <button class="btn-type-3"><?= Yii::t('app', 'ПОДОБРАТЬ АВТО') ?></button>

                <div class="h64px"></div>
                <div class="h16px"></div>
            </div>

            <div class="h32px visible-xs"></div>

            <div class="item right">

                <div class="h64px"></div>

                <div class="font-size-44 fw-600 color-white title"><?= Yii::t('app', 'РАСШИРЕННЫЙ') ?></div>

                <div class="h32px"></div>
                <div class="h8px"></div>

                <div class="font-size-40 fw-700">
                    <?= Content::get(CONTENT_ADVANCED_SERVICE_PRICE, BaseContent::OBJ_NOT_SET, BaseContent::LANG_NOT_SET) ?>
                </div>

                <div class="h32px"></div>
                <div class="h8px"></div>

                <p class="font-size-16">— <?= Yii::t('app', 'Подбор') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Проверка авто') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Покупка на аукционе') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Логистика из США в порт г. Одесса') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Таможенная очистка в порту Одессы') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Организация доставки из Одессы к Вам домой') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Грамотный подбор запчастей') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Сопровождение в процессе ремонта авто') ?></p>
                <p class="font-size-16">— <?= Yii::t('app', 'Сертификация и постановка на учет') ?></p>

                <div class="h24px"></div>
                <div class="h24px"></div>

                <button class="btn-type-3"><?= Yii::t('app', 'ПОДОБРАТЬ АВТО') ?></button>

                <div class="h64px"></div>
                <div class="h16px"></div>
            </div>
        </div>
    </div>

    <div class="h64px hidden-xs"></div>
    <div class="h16px"></div>

</section>

<section class="faq" id="faq">

    <div class="h64px"></div>
    <div class="h16px"></div>

    <div class="container flex-box column">
        <h2 class="h2"><?= Yii::t('app', 'ОТВЕТЫ НА ВОПРОСЫ') ?></h2>

        <div class="h64px"></div>

        <div id="accordion">
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'Почему стоимость конкретной машины на auto.ria меньше, чем привезти из США ?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'Дело в том, что недобросовестные продавцы выставляют машины по заниженным ценам для того, чтобы привлечь внимание покупателей, фактически в 80% случаев машин нет в наличии и Вам предложат её привезти.') ?>
                    <?= Yii::t('app', 'Если машина действительно будет в наличии, с ней будет масса сюрпризов:') ?>
                    <?= Yii::t('app', 'Авто ремонтировали/собирали из дешевых аналогов + шпаклёвка кузова в проблемных местах.') ?>
                    <?= Yii::t('app', 'Зачастую в таких машина устанавливается самая дешевая китайская оптика и электроника может быть в плачевном состоянии и другие скрытые нюансы. ') ?>
                    <?= Yii::t('app', 'Всегда учитывайте это при просчёте автомобиля своей мечты! :)') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'Почему это выгодно ?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'Автомобиль из Америки со всеми расходами и ремонтом стоит в Украине дешевле своих европейских аналогов на 30-50%.') ?>
                    <?= Yii::t('app', 'Что секономит Ваши деньги и в будущем при продаже автомобиля Вы сможете еще и заработать') ?>
                    <?= Yii::t('app', '') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'Сколько нужно ждать мой автомобиль?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'Ориентируйтесь на 3-4 месяца.') ?>
                    <?= Yii::t('app', 'Все зависит от удаленности площадки аукциона до порта отгрузки, сопроводительных документов на экспорт и погодных условий в море, а также других непредвиденных ситуаций, которые могут повлиять на сроки доставки.') ?>
                    <?= Yii::t('app', 'В любом случае, мы будем сопровождать Ваше авто на всех этапах доставки и оформления, вы будете контролировать вместе с нами каждый шаг и отслеживать передвижение контейнера в реальном времени.') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'Мне всю сумму сразу надо заплатить?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'Нет. Сперва оплачивается залог от 400$ до 10% от стоимости авто на аукционе для допуска к торгам.') ?>
                    <?= Yii::t('app', 'После покупки оплачивается стоимость лота (авто), аукционный сбор и доставка в Украину + наши услуги.') ?>
                    <?= Yii::t('app', 'Остальная часть расходов (таможенная очистка, брокер, экспедитор) оплачивается по прибытию машины в Украину, а это как правило через 2-2,5 месяца.') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'С какого момента я являюсь владельцем машины?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'После оформления таможенных документов') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'Могу ли я переоформить машину пока она плывет на другого человека или продать ее?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'Да. За 2 недели до прибытия машины в порт брокеру подаются документы, на какого человека будет идти оформление.') ?>
                    <?= Yii::t('app', 'Этот человек должен иметь право льготной растаможки (то есть он не пользовался этим правом за последние 365 дней)') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'Могут ли быть в машине дополнительные повреждения по прибытию, которых не было видно на фото и после сделанного Карфакса?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'Да, могут.') ?>
                    <?= Yii::t('app', 'Покупка и доставка авто из другого континента всегда риск и Вы должны быть готовы к этому.') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'Будет повреждения?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'Если вы оплачивали страховку автомобиля, наша компания отремонтирует или компенсирует ущерб который нанёс перевозчик в процессе доставки.') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'Как долго могут идти торги нужной мне машины?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'По опыту, от трех дней до трех недель, все зависит от того, какие у Вас требования к автомобилю.') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'Что будет если я выиграл торги, но не могу или не хочу ее оплачивать?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'Вы теряете изначальный залог, который идет на оплату штрафа аукциону.') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'У вас в Америке есть человек, который проверит машину?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'Нет, такого человека нет.') ?>
                    <?= Yii::t('app', 'Так как мы участвуем в торгах на разных площадках, а они находятся в разных штатах Америки.') ?>
                    <?= Yii::t('app', 'Поэтому, вы всегда сможете заказать дополнительную экспертизу, на аукцион приедет человек и даст полную оценку автомобилю, фиксируя фото и видео отчёт.') ?>
                    <?= Yii::t('app', 'Услуга оплачивается дополнительно (150-200$)') ?>
                </p>
            </div>
            <div class="color-white font-size-16 title">
                <?= Yii::t('app', 'Какие вы даёте гарантии?') ?>
            </div>
            <div class="content">
                <p class="font-size-18">
                    <?= Yii::t('app', 'Первое, что нужно знать и помнить — все авто с аукциона в США вы покупаете без гарантии, на условиях «как есть», поэтому нужно быть очень внимательным при выборе, а лучше доверить это профессионалам.') ?>
                    <?= Yii::t('app', 'Решение о покупке принимается на основании фото и описании лота, ещё возможна проверка истории авто по системам Carfax и AutoCheck, что в среднем в интернете стоит около 20$ (для наших клиентов мы это делаем бесплатно).') ?>
                    <?= Yii::t('app', 'На основании вышеперечисленных данных и принимается окончательное решение об участии в торгах') ?>
                </p>
            </div>
        </div>

    </div>

    <div class="h64px hidden-xs"></div>
    <div class="h64px"></div>

</section>