<?php
/**
 * @var $this yii\web\View
 */
use app\models\Form;
?>
<div class="mdc-text-field" data-mdc-auto-init="MDCTextField">
    <input class="mdc-text-field__input  mdc-theme--primary" type="text" name="<?= Form::FIELD_NAME ?>">
    <label class="mdc-floating-label mdc-theme--primary"><?= Yii::t('app', 'Имя') ?></label>
    <div class="mdc-line-ripple"></div>
</div>
<div class="mdc-text-field" data-mdc-auto-init="MDCTextField">
    <input class="mdc-text-field__input  mdc-theme--primary" type="text" name="<?= Form::FIELD_PHONE ?>">
    <label class="mdc-floating-label mdc-theme--primary"><?= Yii::t('app', 'Телефон') ?></label>
    <div class="mdc-line-ripple"></div>
</div>
<div class="mdc-text-field" data-mdc-auto-init="MDCTextField">
    <input class="mdc-text-field__input  mdc-theme--primary" type="text" name="<?= Form::FIELD_BUDGET ?>">
    <label class="mdc-floating-label mdc-theme--primary"><?= Yii::t('app', 'Бюджет') ?></label>
    <div class="mdc-line-ripple"></div>
</div>
<div class="mdc-text-field" data-mdc-auto-init="MDCTextField">
    <input class="mdc-text-field__input  mdc-theme--primary" type="text" name="<?= Form::FIELD_MODEL ?>">
    <label class="mdc-floating-label mdc-theme--primary"><?= Yii::t('app', 'Марка / модель') ?></label>
    <div class="mdc-line-ripple"></div>
</div>
<div class="mdc-text-field" data-mdc-auto-init="MDCTextField">
    <input class="mdc-text-field__input  mdc-theme--primary" type="text" name="<?= Form::FIELD_YEAR ?>">
    <label class="mdc-floating-label mdc-theme--primary"><?= Yii::t('app', 'Год выпуска') ?></label>
    <div class="mdc-line-ripple"></div>
</div>

<button type="submit" class="btn-type-1"><?= Yii::t('app', 'ПОДОБРАТЬ АВТО') ?></button>